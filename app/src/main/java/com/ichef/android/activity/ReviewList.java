package com.ichef.android.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ichef.android.R;
import com.ichef.android.adapter.ReviewNewAdapter;
import com.ichef.android.responsemodel.Rating.Result;
import com.ichef.android.responsemodel.Rating.ReviewsListResponse;
import com.ichef.android.retrofit.APIInterface;
import com.ichef.android.retrofit.ApiClient;
import com.ichef.android.utils.Prefrence;
import com.ichef.android.utils.TransparentProgressDialog;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ReviewList extends AppCompatActivity {
    APIInterface apiInterface;
    String username;
    Spinner spinner;
    RecyclerView rv_MyProjectList;
    ReviewNewAdapter rv_MyProjectAdapter;
    TransparentProgressDialog dialog;
    RecyclerView.LayoutManager rv_MyProjectLayoutManager;
    List<Result> mListData = new ArrayList<>();
    String token;
    private String vendorId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_review_list);
        ImageView back = findViewById(R.id.back);
        vendorId=getIntent().getStringExtra("vendorId");
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        init();
    }

    private void init() {
        rv_MyProjectList = findViewById(R.id.rvcompletedreview);
        rv_MyProjectList.setHasFixedSize(true);
        rv_MyProjectLayoutManager = new LinearLayoutManager(ReviewList.this);
        rv_MyProjectList.setLayoutManager(rv_MyProjectLayoutManager);
        getlist();
        dialog = new TransparentProgressDialog(ReviewList.this);
        dialog.show();
    }

    private void getlist() {
        token = Prefrence.get(this, Prefrence.KEY_TOKEN);
        APIInterface apiInterface = ApiClient.getClient().create(APIInterface.class);
        Call<ReviewsListResponse> resultCall = apiInterface.CallReview("Bearer " + token, vendorId,"0", "10", "datecreated", "desc");
        resultCall.enqueue(new Callback<ReviewsListResponse>() {
            @Override
            public void onResponse(Call<ReviewsListResponse> call, Response<ReviewsListResponse> response) {
                //  Toast.makeText(MyRestaurant.this, ""+response, Toast.LENGTH_SHORT).show();
                if (response.body().getStatus().equals(true)) {
                    dialog.dismiss();
                    mListData = response.body().getResult();
                    setProduct();
                } else {
                    Toast.makeText(ReviewList.this, "" + response.body().getMessage(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ReviewsListResponse> call, Throwable t) {
                Toast.makeText(ReviewList.this, "" + t, Toast.LENGTH_LONG).show();

            }
        });

    }

    private void setProduct() {
        if (mListData != null && mListData.size() > 0) {
            rv_MyProjectAdapter = new ReviewNewAdapter(ReviewList.this, (ArrayList<Result>) mListData);
            rv_MyProjectList.setAdapter(rv_MyProjectAdapter);
        }

    }

}