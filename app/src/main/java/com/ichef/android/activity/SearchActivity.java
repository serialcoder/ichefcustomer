package com.ichef.android.activity;

import android.app.Service;
import android.content.Context;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ichef.android.R;
import com.ichef.android.adapter.ProductAdapter;
import com.ichef.android.adapter.VendorAdapter;
import com.ichef.android.responsemodel.productlist.Vendor;
import com.ichef.android.responsemodel.search.Product;
import com.ichef.android.responsemodel.search.SearchResponse;

import com.ichef.android.responsemodel.search.Vendor__1;
import com.ichef.android.retrofit.APIInterface;
import com.ichef.android.retrofit.ApiClient;
import com.ichef.android.utils.Prefrence;
import com.ichef.android.utils.TransparentProgressDialog;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class SearchActivity extends AppCompatActivity {
    RecyclerView recycleVendor,recycleProduct;
    VendorAdapter rv_MyProjectAdapter;
    ProductAdapter rv_MyProjectAdapter2;
    TransparentProgressDialog dialog;
    RecyclerView.LayoutManager rv_MyProjectLayoutManager;
    List<Vendor__1> vendorlist = new ArrayList<>();
    List<Product> productlist = new ArrayList<>();
    String token;
    EditText searchtext;
    TextView tvProduct,tvVendor,tvNoDataFound,tvClear;
    Context mContext = this;
    NestedScrollView llSearchFood;
    ProgressBar pd_loading;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        ImageView back = findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        init();

    }


    private void init() {
        pd_loading= findViewById(R.id.pd_loading);
        llSearchFood= findViewById(R.id.llSearchFood);
        tvClear= findViewById(R.id.tvClear);
        tvVendor= findViewById(R.id.tvVendor);
        tvProduct= findViewById(R.id.tvProduct);
        tvNoDataFound= findViewById(R.id.tvNoDataFound);
        searchtext= findViewById(R.id.searchtext);
        recycleVendor = findViewById(R.id.rvlist_vendors);
        recycleVendor.setHasFixedSize(true);
        RecyclerView.LayoutManager manager = new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL, false);
        recycleVendor.setLayoutManager(manager);

        recycleProduct = findViewById(R.id.rvlist_products);
        recycleProduct.setHasFixedSize(true);
        rv_MyProjectLayoutManager = new LinearLayoutManager(SearchActivity.this);
        recycleProduct.setLayoutManager(rv_MyProjectLayoutManager);

        searchtext.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    if(searchtext.getText().toString().equalsIgnoreCase("")){
                        Toast.makeText(mContext, "Please enter search text", Toast.LENGTH_SHORT).show();
                    }else{
                        hideKeyBoard(mContext,searchtext);
                        getlist();
                    }
                    return true;
                }
                return false;
            }
        });

        tvClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                searchtext.setText("");
            }
        });

    }


    private void getlist()  {
        pd_loading.setVisibility(View.VISIBLE);
        token= Prefrence.get(SearchActivity.this, Prefrence.KEY_TOKEN);
        APIInterface apiInterface = ApiClient.getClient().create(APIInterface.class);
        Call<SearchResponse> call = apiInterface.GetSearch("Bearer " + token,searchtext.getText().toString().trim(),"0","10"
               ,Prefrence.get(mContext,Prefrence.KEY_LATITUDE), Prefrence.get(mContext,Prefrence.KEY_LONGITUDE));
       // Call<SearchResponse> call = apiInterface.GetSearch("Bearer " + token);
        call.enqueue(new Callback<SearchResponse>() {
            @Override
            public void onResponse(Call<SearchResponse> call, Response<SearchResponse> response)
            {
                pd_loading.setVisibility(View.GONE);
                if(response.isSuccessful()){
                    if(response.body().getStatus()){
                        if(response.body().getResult() != null){
                            tvNoDataFound.setVisibility(View.GONE);
                            llSearchFood.setVisibility(View.VISIBLE);
                            vendorlist = response.body().getResult().getVendors();
                            if (vendorlist != null && vendorlist.size() > 0) {
                                tvVendor.setVisibility(View.VISIBLE);
                                recycleVendor.setVisibility(View.VISIBLE);
                                tvVendor.setText("Vendor ("+vendorlist.size()+")");
                                rv_MyProjectAdapter = new VendorAdapter(SearchActivity.this, (ArrayList<Vendor__1>) vendorlist);
                                recycleVendor.setAdapter(rv_MyProjectAdapter);
                            }else{
                                tvVendor.setVisibility(View.GONE);
                                recycleVendor.setVisibility(View.GONE);
                            }

                            productlist = response.body().getResult().getProducts();
                            if (productlist != null && productlist.size() > 0) {
                                tvProduct.setVisibility(View.VISIBLE);
                                recycleProduct.setVisibility(View.VISIBLE);
                                tvProduct.setText("Product ("+productlist.size()+")");
                                rv_MyProjectAdapter2 = new ProductAdapter(SearchActivity.this, (ArrayList<Product>) productlist);
                                recycleProduct.setAdapter(rv_MyProjectAdapter2);
                            }else{
                                tvProduct.setVisibility(View.GONE);
                                recycleProduct.setVisibility(View.GONE);
                            }
                        }else{
                            tvVendor.setVisibility(View.GONE);
                            recycleVendor.setVisibility(View.GONE);
                            tvProduct.setVisibility(View.GONE);
                            recycleProduct.setVisibility(View.GONE);
                            tvNoDataFound.setVisibility(View.VISIBLE);
                            llSearchFood.setVisibility(View.GONE);
                        }
                    }else{
                        tvVendor.setVisibility(View.GONE);
                        recycleVendor.setVisibility(View.GONE);
                        tvProduct.setVisibility(View.GONE);
                        recycleProduct.setVisibility(View.GONE);
                        tvNoDataFound.setVisibility(View.VISIBLE);
                        llSearchFood.setVisibility(View.GONE);
                        Toast.makeText(SearchActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
                else {
                    tvVendor.setVisibility(View.GONE);
                    recycleVendor.setVisibility(View.GONE);
                    tvProduct.setVisibility(View.GONE);
                    recycleProduct.setVisibility(View.GONE);
                    tvNoDataFound.setVisibility(View.VISIBLE);
                    llSearchFood.setVisibility(View.GONE);
                    Toast.makeText(SearchActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<SearchResponse> call, Throwable t) {
                pd_loading.setVisibility(View.GONE);
                Toast.makeText(SearchActivity.this, getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public static void hideKeyBoard(Context ct, EditText ed) {
        InputMethodManager imm = (InputMethodManager) ct
                .getSystemService(Service.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(ed.getWindowToken(), 0);
    }


}