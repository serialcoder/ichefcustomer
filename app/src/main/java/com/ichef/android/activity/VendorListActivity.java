package com.ichef.android.activity;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.airbnb.lottie.LottieAnimationView;
import com.ichef.android.R;
import com.ichef.android.adapter.HomeFoodAdapter;
import com.ichef.android.adapter.ReviewNewAdapter;
import com.ichef.android.responsemodel.productlist.Result;
import com.ichef.android.responsemodel.Rating.ReviewsListResponse;
import com.ichef.android.responsemodel.productlist.HomePageListModel;
import com.ichef.android.responsemodel.productlist.Vendor;
import com.ichef.android.retrofit.APIInterface;
import com.ichef.android.retrofit.ApiClient;
import com.ichef.android.utils.Prefrence;
import com.ichef.android.utils.TransparentProgressDialog;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class VendorListActivity extends AppCompatActivity {
    RecyclerView rv_MyProjectList;
    HomeFoodAdapter rv_MyProjectAdapter;
    TransparentProgressDialog dialog;
    RecyclerView.LayoutManager rv_MyProjectLayoutManager;
    List<Vendor> mListData = new ArrayList<>();
    String token;
    private String type="",id="",title="Reviews";
    TextView tvStatus;
    Context mContext = this;
    SwipeRefreshLayout mSwipeRefreshlayout;
    RelativeLayout lottiNoFood;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_review_list);
        ImageView back = findViewById(R.id.back);
        type=getIntent().getStringExtra("type");
        id=getIntent().getStringExtra("id");
        if(getIntent().getStringExtra("name") != null && !getIntent().getStringExtra("name").equalsIgnoreCase("")){
           title =  getIntent().getStringExtra("name");
        }
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        init();
    }

    private void init() {
        lottiNoFood = findViewById(R.id.lottiNoFood);
        mSwipeRefreshlayout = findViewById(R.id.swipeRefresh);
        tvStatus = findViewById(R.id.tvStatus);
        rv_MyProjectList = findViewById(R.id.rvcompletedreview);
        rv_MyProjectList.setHasFixedSize(true);
        rv_MyProjectLayoutManager = new LinearLayoutManager(VendorListActivity.this);
        rv_MyProjectList.setLayoutManager(rv_MyProjectLayoutManager);
        tvStatus.setText(title);
        getNearVendorByTypeIdList();
        dialog = new TransparentProgressDialog(VendorListActivity.this);
        dialog.show();
    }

    private void getNearVendorByTypeIdList() {

        String token = "Bearer " + Prefrence.get(mContext, Prefrence.KEY_TOKEN);

        APIInterface apiInterface = ApiClient.getClient().create(APIInterface.class);
        Call<HomePageListModel> resultCall = apiInterface.CallNearByVendorType(token, type,id,Prefrence.get(mContext, Prefrence.KEY_LATITUDE), Prefrence.get(mContext, Prefrence.KEY_LONGITUDE));

        resultCall.enqueue(new Callback<HomePageListModel>() {
            @Override
            public void onResponse(Call<HomePageListModel> call, Response<HomePageListModel> response) {
                if (mSwipeRefreshlayout != null) {
                    mSwipeRefreshlayout.setRefreshing(false);
                }
                if (response.body().getStatus()) {
                    dialog.dismiss();
                    mListData = response.body().getResult().getVendors();
                    if (mListData.size() == 0) {
                        lottiNoFood.setVisibility(View.VISIBLE);
                        rv_MyProjectList.setVisibility(View.GONE);
                    } else {
                        lottiNoFood.setVisibility(View.GONE);
                        rv_MyProjectList.setVisibility(View.VISIBLE);
                    }
                    setProduct();
                } else {
                    lottiNoFood.setVisibility(View.VISIBLE);
                    rv_MyProjectList.setVisibility(View.GONE);
                    dialog.dismiss();
                    Toast.makeText(mContext, "No vendor found at this location!", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<HomePageListModel> call, Throwable t) {
                if (mSwipeRefreshlayout != null) {
                    mSwipeRefreshlayout.setRefreshing(false);
                }
                lottiNoFood.setVisibility(View.VISIBLE);
                rv_MyProjectList.setVisibility(View.GONE);
                dialog.dismiss();
                call.cancel();
            }
        });
    }


    private void setProduct() {
        if (mListData != null && mListData.size() > 0) {
            rv_MyProjectAdapter = new HomeFoodAdapter(mContext, (ArrayList<Vendor>) mListData);
            rv_MyProjectList.setAdapter(rv_MyProjectAdapter);
        }
    }

}