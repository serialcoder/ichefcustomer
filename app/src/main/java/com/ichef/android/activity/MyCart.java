package com.ichef.android.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.hbb20.CountryCodePicker;
import com.ichef.android.R;
import com.ichef.android.adapter.MyCartAdapter;
import com.ichef.android.requestmodel.Cart.UpdateCartRequest;
import com.ichef.android.requestmodel.CreateOrder.Address;
import com.ichef.android.requestmodel.CreateOrder.CreateOrderRequest;
import com.ichef.android.requestmodel.user.EditProfileRequest;
import com.ichef.android.responsemodel.Address.AddAddressResponse;
import com.ichef.android.responsemodel.PromoCode.ApplyPromoCodeResponse;
import com.ichef.android.responsemodel.cartmodel.Cart;
import com.ichef.android.responsemodel.cartmodel.FetchCartResponse;
import com.ichef.android.responsemodel.cartmodel.Result;
import com.ichef.android.responsemodel.signup.SignupResponse;
import com.ichef.android.responsemodel.verify.VerifyResponseModel;
import com.ichef.android.retrofit.APIInterface;
import com.ichef.android.retrofit.ApiClient;
import com.ichef.android.utils.CommonUtility;
import com.ichef.android.utils.Prefrence;
import com.ichef.android.utils.TransparentProgressDialog;
import com.ichef.android.utils.VerifyProgressDialog;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyCart extends AppCompatActivity {

    String token;
    int LAUNCH_PAYMENT_ACTIVITY = 1000;
    RecyclerView rv_MyProjectList;
    MyCartAdapter rv_MyProjectAdapter;
    TransparentProgressDialog dialog;
    RecyclerView.LayoutManager rv_MyProjectLayoutManager;
    List<Cart> mListData = new ArrayList<>();
    TextView   tvApplyPromoCode, tvRemovePromoCode, tvPromoApplied;
    EditText etTipAmount;
    RelativeLayout rlcontinue, rlPromoApplied, rlApplyPromo;
    TextView tvCartAmount, tvDeliveryCharge, tvTaxAmount, tvTotalAmount, tvamount, tvPromoAmount, tvDeliveryAddress;
    int tipAmount = 0;
    double promoSlash = 0.0;
    double cartTotal = 0.0;
    EditText etPromocode;
    String promoCode;
    private ImageView imgEditAddress;
    private Result cartObject;
    private boolean isFromPayment=false;
    LinearLayout llEmptyCart;
    Context mContext = this;
    String vendorID = "";
    EditText etComment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_cart);
        ImageView back = findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        init();
        onlclick();
    }

    private void onlclick() {
        rlcontinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String mobileNo = "";

                if (Prefrence.get(MyCart.this, Prefrence.KEY_DELIVERY_MOBILE_NO) != null &&
                        !Prefrence.get(MyCart.this, Prefrence.KEY_DELIVERY_MOBILE_NO).equalsIgnoreCase("")) {

                    mobileNo = Prefrence.get(MyCart.this, Prefrence.KEY_DELIVERY_MOBILE_NO);

                } else if (Prefrence.get(MyCart.this, Prefrence.KEY_MOBILE_NO) != null &&
                        !Prefrence.get(MyCart.this, Prefrence.KEY_MOBILE_NO).equalsIgnoreCase("")) {

                    mobileNo = Prefrence.get(MyCart.this, Prefrence.KEY_MOBILE_NO);
                }

                if(mobileNo.equalsIgnoreCase("")){
                    showMobileDialog();
                }else{
                    Intent i = new Intent(MyCart.this, PaymentWebView.class);
                    i.putExtra("totalAmount",cartTotal);
                    i.putExtra("vendorID",vendorID);
                    isFromPayment=true;
                    startActivityForResult(i, LAUNCH_PAYMENT_ACTIVITY);
                }
            }
        });
        imgEditAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MyCart.this, SelectDeliveryAddress.class);
                startActivity(intent);
            }
        });
        tvApplyPromoCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                promoCode = etPromocode.getText().toString();
                if (promoCode != null && !promoCode.isEmpty()) {
                    applyPromoCode(promoCode);
                } else {
                    Toast.makeText(MyCart.this, "Enter a valid promo code", Toast.LENGTH_SHORT).show();
                }
            }
        });
        tvRemovePromoCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rlApplyPromo.setVisibility(View.VISIBLE);
                rlPromoApplied.setVisibility(View.GONE);
                promoSlash = 0.0;
                updateCartTotal();
            }
        });
        etTipAmount.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(editable.length()>0) {
                    String currentText = editable.toString();
                    tipAmount=Integer.parseInt(currentText);
                }else{
                    tipAmount = 0;
                }
                updateCartTotal();
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == LAUNCH_PAYMENT_ACTIVITY) {
            if(resultCode == Activity.RESULT_OK){
                String transactionId=data.getStringExtra("transactionId");
                CreateOrderApiCall(transactionId,true);
            }
            if (resultCode == Activity.RESULT_CANCELED) {

                CreateOrderApiCall("NA",false);

                AlertDialog.Builder alertDialog = new AlertDialog.Builder(MyCart.this);
                alertDialog.setMessage(R.string.payment_failed);
                alertDialog.setIcon(R.drawable.payementgrey);
                // Setting Negative "NO" Button
                alertDialog.setNegativeButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // Write your code here to invoke NO event
                        dialog.dismiss();
                        dialog.cancel();
                    }
                });
                alertDialog.setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent i = new Intent(MyCart.this, PaymentWebView.class);
                        i.putExtra("totalAmount",cartTotal);
                        i.putExtra("vendorID",vendorID);
                        isFromPayment=true;
                        startActivityForResult(i, LAUNCH_PAYMENT_ACTIVITY);
                    }
                });
                // Showing Alert Message
                alertDialog.show();
            }
        }
    }

    private void init() {
        etComment = findViewById(R.id.etComment);
        rlcontinue = findViewById(R.id.rlcontinue);
        rlPromoApplied = findViewById(R.id.rlPromoApplied);
        rlApplyPromo = findViewById(R.id.rlApplyPromo);
        etPromocode = findViewById(R.id.etPromocode);
        tvApplyPromoCode = findViewById(R.id.tvApplyPromoCode);
        tvRemovePromoCode = findViewById(R.id.tvRemovePromo);
        tvPromoAmount = findViewById(R.id.tvPromoAmount);
        tvPromoApplied = findViewById(R.id.tvPromoApplied);
        tvCartAmount = findViewById(R.id.tvCartAmount);
        etTipAmount = findViewById(R.id.etTipAmount);
        tvDeliveryCharge = findViewById(R.id.tvDeliveryCharge);
        tvTaxAmount = findViewById(R.id.tvTaxAmount);
        tvTotalAmount = findViewById(R.id.tvTotalAmount);
        tvamount = findViewById(R.id.tvamount);
        rv_MyProjectList = findViewById(R.id.rvcart);
        imgEditAddress = findViewById(R.id.imgEditAddress);
        llEmptyCart = findViewById(R.id.llEmptyCart);
        tvDeliveryAddress = findViewById(R.id.tvDeliveryAddress);
        rv_MyProjectList.setHasFixedSize(true);
        rv_MyProjectLayoutManager = new LinearLayoutManager(MyCart.this);
        rv_MyProjectList.setLayoutManager(rv_MyProjectLayoutManager);
    }

    private void applyPromoCode(String promoCode) {
        dialog = new TransparentProgressDialog(MyCart.this);
        dialog.show();
        token = Prefrence.get(MyCart.this, Prefrence.KEY_TOKEN);
        APIInterface apiInterface = ApiClient.getClient().create(APIInterface.class);
        Call<ApplyPromoCodeResponse> call = apiInterface.ApplyPromoCode("Bearer " + token, promoCode);
        call.enqueue(new Callback<ApplyPromoCodeResponse>() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<ApplyPromoCodeResponse> call, Response<ApplyPromoCodeResponse> response) {
                if (response.body().getSuccess()) {
                    if (response.body().getResult().getUsed()) {
                        Toast.makeText(MyCart.this, "Promo Code already used", Toast.LENGTH_SHORT).show();
                        promoSlash = 0.0;
                    } else {
                        promoSlash = response.body().getResult().getPriceSlash();
                        tvPromoApplied.setText("Promo code " + promoCode + " applied!");
                        rlApplyPromo.setVisibility(View.GONE);
                        rlPromoApplied.setVisibility(View.VISIBLE);
                    }

                } else {
                    promoSlash = 0.0;
                    Toast.makeText(MyCart.this, "Invalid PromoCode", Toast.LENGTH_SHORT).show();
                }
                updateCartTotal();
                dialog.dismiss();
            }

            @Override
            public void onFailure(Call<ApplyPromoCodeResponse> call, Throwable t) {
                promoSlash = 0.0;
                dialog.dismiss();
                call.cancel();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        tvDeliveryAddress.setText(Prefrence.get(this, Prefrence.KEY_DELIVERY_CITYNAME).isEmpty() ? Prefrence.get(this, Prefrence.CITYNAME) : Prefrence.get(this, Prefrence.KEY_DELIVERY_CITYNAME));
        if(!isFromPayment) {
            getlist();
            dialog = new TransparentProgressDialog(MyCart.this);
            dialog.show();
        }
        isFromPayment=false;
    }

    private void CreateOrderApiCall(String transactionId,Boolean isPaid) {
        if(isPaid){
            TransparentProgressDialog dialog = new TransparentProgressDialog(this);
            dialog.show();
        }
        String token = Prefrence.get(this, Prefrence.KEY_TOKEN);
        CreateOrderRequest request = new CreateOrderRequest();
        request.setTransactionId(transactionId);
        request.setDelivery_type("delivery");
        request.setComment(etComment.getText().toString().trim());
        request.setVendor(cartObject.getVendor().getId());
        request.setAmountCharged(Double.parseDouble(String.format("%.2f", cartTotal)));
        request.setTotalCartAmount(cartObject.getTotalCartAmount());
        request.setCartAmount(cartObject.getCartAmount());
        request.setDeliveryAmount(Double.parseDouble(cartObject.getDeliveryAmount()));
        request.setTaxableAmount(cartObject.getTaxableAmount());
        request.setCoupon(promoSlash > 0 ? promoCode : "");
        request.setDiscountSlash(0);
        request.setPromoSlash(promoSlash);
        request.setStatus("pending");
        request.setTip(tipAmount);
        request.setPaid(isPaid);
        Address address = new Address();
        address.setAddress(Prefrence.get(this, Prefrence.KEY_DELIVERY_CITYNAME).isEmpty() ? Prefrence.get(this, Prefrence.CITYNAME) : Prefrence.get(this, Prefrence.KEY_DELIVERY_CITYNAME));
        address.setLatitude(Prefrence.get(this, Prefrence.KEY_DELIVERY_LATITUDE).isEmpty() ? Prefrence.get(this, Prefrence.KEY_LATITUDE) : Prefrence.get(this, Prefrence.KEY_DELIVERY_LATITUDE));
        address.setLongitude(Prefrence.get(this, Prefrence.KEY_DELIVERY_LONGITUDE).isEmpty() ? Prefrence.get(this, Prefrence.KEY_LONGITUDE) : Prefrence.get(this, Prefrence.KEY_DELIVERY_LONGITUDE));
        if (Prefrence.get(this, Prefrence.KEY_DELIVERY_MOBILE_NO).isEmpty()) {
            address.setMobile(Prefrence.get(this, Prefrence.KEY_MOBILE_NO));
        } else {
            address.setMobile(Prefrence.get(this, Prefrence.KEY_DELIVERY_MOBILE_NO));
        }
        if (Prefrence.get(this, Prefrence.KEY_DELIVERY_NAME).isEmpty()) {
            address.setName(Prefrence.get(this, Prefrence.KEY_FIRST_NAME));
        } else {
            address.setName(Prefrence.get(this, Prefrence.KEY_DELIVERY_NAME));
        }
        request.setAddress(address);

        APIInterface apiInterface = ApiClient.getClient().create(APIInterface.class);
        Call<AddAddressResponse> resultCall = apiInterface.CallCreateOrder("Bearer " + token, request);
        resultCall.enqueue(new Callback<AddAddressResponse>() {

            @Override
            public void onResponse(Call<AddAddressResponse> call, retrofit2.Response<AddAddressResponse> response) {
                if (response.body().getStatus()) {
                    if(isPaid) {
                        Toast.makeText(MyCart.this, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        Prefrence.saveInt(MyCart.this, Prefrence.KEY_ITEM_COUNT, 0);
                        Prefrence.save(MyCart.this, Prefrence.KEY_TOTAL, "0");
                        Prefrence.save(MyCart.this, Prefrence.KEY_DELIVERY_LATITUDE, "");
                        Prefrence.save(MyCart.this, Prefrence.KEY_DELIVERY_LONGITUDE, "");
                        Prefrence.save(MyCart.this, Prefrence.KEY_DELIVERY_CITYNAME, "");
                        Prefrence.save(MyCart.this, Prefrence.KEY_DELIVERY_NAME, "");
                        Prefrence.save(MyCart.this, Prefrence.KEY_DELIVERY_MOBILE_NO, "");
                        Intent intent = new Intent(MyCart.this, ThankyouActvitiy.class);
                        intent.putExtra("totalAmount", tvTotalAmount.getText().toString());
                        intent.putExtra("deliveryAddress", tvDeliveryAddress.getText().toString());
                        startActivity(intent);
                        finish();
                    }
                } else {
                    if(isPaid) {
                        Toast.makeText(MyCart.this, "Something went wrong, Please try again later", Toast.LENGTH_SHORT).show();
                    }
                }
                if(isPaid) {
                    dialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<AddAddressResponse> call, Throwable t) {
                if(isPaid) {
                    dialog.dismiss();
                    Toast.makeText(MyCart.this, "Please check your Internet Connection", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void getlist() {
        token = Prefrence.get(MyCart.this, Prefrence.KEY_TOKEN);
        APIInterface apiInterface = ApiClient.getClient().create(APIInterface.class);
        UpdateCartRequest request = new UpdateCartRequest();
        request.setLatitude(Double.valueOf(Prefrence.get(this, Prefrence.KEY_LATITUDE)));
        request.setLongitude(Double.valueOf(Prefrence.get(this, Prefrence.KEY_LONGITUDE)));
        Call<FetchCartResponse> call = apiInterface.GetUpdatedCart("Bearer " + token, request);
        call.enqueue(new Callback<FetchCartResponse>() {
            @Override
            public void onResponse(Call<FetchCartResponse> call, Response<FetchCartResponse> response) {
                dialog.dismiss();
                if (response.body().getStatus()) {
                    //  dialog.dismiss();
                    try {
                        llEmptyCart.setVisibility(View.GONE);
                        cartObject = response.body().getResult();
                        mListData = cartObject.getCart();
                        vendorID = cartObject.getVendor().getId();
                        setProduct(cartObject.getVendor().getId());
                        CommonUtility.saveCartDetails(MyCart.this, cartObject);
                        tvCartAmount.setText(String.valueOf(cartObject.getCartAmount()));
                        tvDeliveryCharge.setText(String.valueOf(cartObject.getDeliveryAmount()));
                        tvTaxAmount.setText(String.valueOf(cartObject.getTaxableAmount()));
                        tvTotalAmount.setText(String.valueOf(cartObject.getTotalCartAmount()));
                        cartTotal = cartObject.getTotalCartAmount();
                        updateCartTotal();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    // dialog.dismiss();
                    Toast.makeText(MyCart.this, "Cart is empty, Please add item first", Toast.LENGTH_LONG).show();
                    Prefrence.saveInt(MyCart.this, Prefrence.KEY_ITEM_COUNT, 0);
                    Prefrence.save(MyCart.this, Prefrence.KEY_TOTAL, "0");
                    llEmptyCart.setVisibility(View.VISIBLE);
                    //finish();
                }
            }

            @Override
            public void onFailure(Call<FetchCartResponse> call, Throwable t) {
                llEmptyCart.setVisibility(View.VISIBLE);
                dialog.dismiss();
                call.cancel();
            }
        });
    }

    private void updateCartTotal() {
        if(cartObject != null){
            cartTotal = cartObject.getTotalCartAmount() + tipAmount - promoSlash;
            tvamount.setText(getString(R.string.nigiriacurrency) + " " + String.format("%.2f", cartTotal));
            tvTotalAmount.setText(String.format("%.2f", cartTotal));
            tvPromoAmount.setText(String.format("%.2f", promoSlash));
        }
    }



    private void setProduct(String vendorId) {
        if (mListData != null && mListData.size() > 0) {
            rv_MyProjectAdapter = new MyCartAdapter(MyCart.this, (ArrayList<Cart>) mListData, vendorId);
            rv_MyProjectList.setAdapter(rv_MyProjectAdapter);
        }

    }


    public void showMobileDialog() {
        final Dialog dialogBuilder = new Dialog(mContext,R.style.Theme_Dialog);
        dialogBuilder.requestWindowFeature(Window.FEATURE_NO_TITLE); //before

        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_enter_mobile, null);
        dialogBuilder.setContentView(dialogView);

        EditText et_mobile = dialogView.findViewById(R.id.et_mobile);
        CountryCodePicker dialogccp = dialogView.findViewById(R.id.ccp);
        Button btnSubmit = dialogView.findViewById(R.id.btnSUbmit);
        TextView btnCancel = dialogView.findViewById(R.id.btnCancel);
        ProgressBar pd_loading = dialogView.findViewById(R.id.pd_loading);

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(et_mobile.getText().toString().trim().equalsIgnoreCase("")){
                    Toast.makeText(mContext, mContext.getResources().getString(R.string.enter_mobile), Toast.LENGTH_SHORT).show();
                }else{
                    String myMobile = dialogccp.getSelectedCountryCodeWithPlus()+""+et_mobile.getText().toString().trim().replace(" ", "");;
                    EditProfileRequest request = new EditProfileRequest();
                    request.setId(Prefrence.get(mContext, Prefrence.KEY_USER_ID));
                    request.setMobile_number(myMobile);
                    callUpdateMobileNumber(request,pd_loading,btnSubmit,dialogBuilder);
                }
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogBuilder.dismiss();
            }
        });


        dialogBuilder.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
            }
        });
        dialogBuilder.show();
    }


    private void callUpdateMobileNumber(EditProfileRequest request, ProgressBar pd_loading, Button btnSubmit, Dialog dialogBuilder) {
        pd_loading.setVisibility(View.VISIBLE);
        btnSubmit.setVisibility(View.GONE);
        APIInterface apiInterface = ApiClient.getClient().create(APIInterface.class);
        Call<SignupResponse> resultCall = apiInterface.CallUpdateUser("Bearer " + Prefrence.get(mContext,Prefrence.KEY_TOKEN),request);
        resultCall.enqueue(new Callback<SignupResponse>() {

            @Override
            public void onResponse(Call<SignupResponse> call, Response<SignupResponse> response) {
                pd_loading.setVisibility(View.GONE);
                btnSubmit.setVisibility(View.VISIBLE);
                if (response.body().getStatus().equals(true)){
                    dialogBuilder.dismiss();
                    Prefrence.save(mContext,Prefrence.KEY_MOBILE_NO,request.getMobile_number());
                    Toast.makeText(mContext, "Your mobile number has been updated", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(mContext, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<SignupResponse> call, Throwable t) {
                Toast.makeText(mContext, getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
            }
        });

    }


    public void updateCartItem(Result mCartObject,Boolean status){
        if(status){
            llEmptyCart.setVisibility(View.GONE);
            cartObject = mCartObject;
            mListData = cartObject.getCart();
            vendorID = cartObject.getVendor().getId();
            setProduct(cartObject.getVendor().getId());
            tvCartAmount.setText(String.valueOf(cartObject.getCartAmount()));
            tvDeliveryCharge.setText(String.valueOf(cartObject.getDeliveryAmount()));
            tvTaxAmount.setText(String.valueOf(cartObject.getTaxableAmount()));
            tvTotalAmount.setText(String.valueOf(cartObject.getTotalCartAmount()));
            cartTotal = cartObject.getTotalCartAmount();
            updateCartTotal();
        }else{
            Toast.makeText(MyCart.this, "Cart is empty, Please add item first", Toast.LENGTH_LONG).show();
            Prefrence.saveInt(MyCart.this, Prefrence.KEY_ITEM_COUNT, 0);
            Prefrence.save(MyCart.this, Prefrence.KEY_TOTAL, "0");
            llEmptyCart.setVisibility(View.VISIBLE);
        }

    }

    private void verifyPayment(String tid) {
        VerifyProgressDialog dialog = new VerifyProgressDialog(this);
         String token = "Bearer " + Prefrence.get(this, Prefrence.KEY_TOKEN);
        APIInterface apiInterface = ApiClient.getClient().create(APIInterface.class);
        Call<VerifyResponseModel> call = apiInterface.VarifyPayment(token, tid);
        call.enqueue(new Callback<VerifyResponseModel>() {
            @Override
            public void onResponse(Call<VerifyResponseModel> call, Response<VerifyResponseModel> response) {
            }

            @Override
            public void onFailure(Call<VerifyResponseModel> call, Throwable t) {
            }
        });
    }

}