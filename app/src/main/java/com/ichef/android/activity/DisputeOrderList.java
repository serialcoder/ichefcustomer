package com.ichef.android.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.ichef.android.R;
import com.ichef.android.adapter.DisputeOrderAdapter;
import com.ichef.android.adapter.RunningOrderAdapter;
import com.ichef.android.responsemodel.dispute.DisputedModel;
import com.ichef.android.responsemodel.orders.GetOrderResponse;
import com.ichef.android.responsemodel.orders.Result;
import com.ichef.android.retrofit.APIInterface;
import com.ichef.android.retrofit.ApiClient;
import com.ichef.android.utils.Prefrence;
import com.ichef.android.utils.TransparentProgressDialog;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DisputeOrderList extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {
    APIInterface apiInterface;
    String username;
    Spinner spinner;
    RecyclerView rv_MyProjectList;
    DisputeOrderAdapter rv_MyProjectAdapter;
    TransparentProgressDialog dialog;
    RecyclerView.LayoutManager rv_MyProjectLayoutManager;
    int page = 0, limit = 10;
    List<com.ichef.android.responsemodel.dispute.Result> mListData = new ArrayList<>();
    SwipeRefreshLayout mSwipeRefreshlayout;
    LinearLayout llNoResultound;
    TextView txtMessage,letsStore,tvTitle;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_running_order);
        ImageView back = findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        init();
    }

    private void init() {
        tvTitle = findViewById(R.id.tvTitle);
        tvTitle.setText("Disputed Orders");
        letsStore = findViewById(R.id.letsStore);
        txtMessage = findViewById(R.id.txtMessage);
        txtMessage.setText("No Disputes Found!");
        llNoResultound = findViewById(R.id.llNoResultound);
        mSwipeRefreshlayout = findViewById(R.id.swipeRefresh);
        mSwipeRefreshlayout.setOnRefreshListener(this);
        letsStore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(DisputeOrderList.this,HomePageActivity.class);
                startActivity(i);
            }
        });

        rv_MyProjectList = findViewById(R.id.rvrunningorder);
        rv_MyProjectList.setHasFixedSize(true);
        rv_MyProjectLayoutManager = new LinearLayoutManager(DisputeOrderList.this);
        rv_MyProjectList.setLayoutManager(rv_MyProjectLayoutManager);
        getlist();
        dialog = new TransparentProgressDialog(DisputeOrderList.this);
        dialog.show();
    }

    private void getlist() {
        username = Prefrence.get(DisputeOrderList.this, Prefrence.KEY_MANAGER_ID);
        String token = "Bearer " + Prefrence.get(this, Prefrence.KEY_TOKEN);
        apiInterface = ApiClient.getClient().create(APIInterface.class);
        Call<DisputedModel> call = apiInterface.GetDisputes(token);
        call.enqueue(new Callback<DisputedModel>() {
            @Override
            public void onResponse(Call<DisputedModel> call, Response<DisputedModel> response) {
                if (mSwipeRefreshlayout != null) {
                    mSwipeRefreshlayout.setRefreshing(false);
                }
                if (response.body().getStatus()) {
                    dialog.dismiss();
                    mListData = response.body().getResult();
                    if(mListData != null && mListData.size()>0){
                        llNoResultound.setVisibility(View.GONE);
                        setProduct();
                    }else{
                        llNoResultound.setVisibility(View.VISIBLE);
                    }
                } else {
                    llNoResultound.setVisibility(View.VISIBLE);
                    dialog.dismiss();
                    Toast.makeText(DisputeOrderList.this, "No Data Found!", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<DisputedModel> call, Throwable t) {
                if (mSwipeRefreshlayout != null) {
                    mSwipeRefreshlayout.setRefreshing(false);
                }
                dialog.dismiss();
                call.cancel();
            }
        });
    }

    private void setProduct() {
        if (mListData != null && mListData.size() > 0) {
            rv_MyProjectAdapter = new DisputeOrderAdapter(DisputeOrderList.this, (ArrayList<com.ichef.android.responsemodel.dispute.Result>) mListData);
            rv_MyProjectList.setAdapter(rv_MyProjectAdapter);
        }
    }

    @Override
    public void onRefresh() {
        getlist();
    }


}