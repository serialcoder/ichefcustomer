package com.ichef.android.activity;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.AutocompleteSupportFragment;
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener;
import com.ichef.android.R;
import com.ichef.android.utils.LocationTrack;
import com.ichef.android.utils.Prefrence;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;
public class LocationManually extends AppCompatActivity
{

    LinearLayout locationtxt;
    TextView locationtx;
    String city;
    protected LocationManager locationManager;
    protected Context context;
    EditText etPlace;
    String s_source = "";
    double srcLatitude, srcLlongitude;
    private ArrayList permissionsToRequest;
    private ArrayList permissionsRejected = new ArrayList();
    private ArrayList permissions = new ArrayList();

    private final static int ALL_PERMISSIONS_RESULT = 101;
    LocationTrack locationTrack;
    double slatitude,slongitude;
    boolean isFromHome=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location_manually);
        ImageView back = findViewById(R.id.back);
        isFromHome=getIntent().getBooleanExtra("isFromHome",false);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        permissions.add(ACCESS_FINE_LOCATION);
        permissions.add(ACCESS_COARSE_LOCATION);
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        permissionsToRequest = findUnAskedPermissions(permissions);
        //get the permissions we have asked for before but are not granted..
        //we will store this in a global list to access later.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (permissionsToRequest.size() > 0)
                requestPermissions((String[]) permissionsToRequest.toArray(new String[permissionsToRequest.size()]), ALL_PERMISSIONS_RESULT);
        }
        init();
        onclick();
    }

    private void onclick() {
        locationtxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(LocationManually.this, "Getting Current Location...", Toast.LENGTH_LONG).show();
                fetchCurrentLatLong();
            }
        });
    }

    private void displayLocationSettingsRequest(Context context) {
        GoogleApiClient googleApiClient = new GoogleApiClient.Builder(context)
                .addApi(LocationServices.API).build();
        googleApiClient.connect();
        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(10000 / 2);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);
        builder.setAlwaysShow(true);
        PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        Log.i("SHUBHAM", "All location settings are satisfied.");
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        Log.i("SHUBHAM", "Location settings are not satisfied. Show the user a dialog to upgrade location settings ");
                        try {
                            // Show the dialog by calling startResolutionForResult(), and check the result
                            // in onActivityResult().
                            status.startResolutionForResult(LocationManually.this, 101);
                        } catch (IntentSender.SendIntentException e) {
                            Log.i("SHUBHAM", "PendingIntent unable to execute request.");
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        Log.i("SHUBHAM", "Location settings are inadequate, and cannot be fixed here. Dialog not created.");
                        break;
                }
            }
        });
    }

    public void fetchCurrentLatLong() {
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            displayLocationSettingsRequest(this);
        }

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            // execute when network_provider is enable
            if (locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
                // set requestLocationUpdates with adding time to refresh your location status and distance
                handleLocation(locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER));
               /* locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 5000, 0, new LocationListener() {
                    @Override
                    public void onLocationChanged(@NonNull Location location) {
                        handleLocation(location);
                    }
                });*/

            }
            // execute when Gps_provider is enable
            else if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                // set requestLocationUpdates with adding time to refresh your location status and distance
                handleLocation(locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER));
               /* locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 0, new LocationListener() {
                    @Override
                    public void onLocationChanged(@NonNull Location location) {
                        handleLocation(location);
                    }
                });*/

            } else {
                Toast.makeText(this, "Please enable GPS for better result", Toast.LENGTH_SHORT).show();

            }
        }
    }

    private void handleLocation(Location location){
        if (location != null) {
            Prefrence.save(this, Prefrence.KEY_LATITUDE, String.valueOf(location.getLatitude()));
            Prefrence.save(this, Prefrence.KEY_LONGITUDE, String.valueOf(location.getLongitude()));
            List<Address> addresses = null;
            try {
                Geocoder geocoder = new Geocoder(this, Locale.getDefault());
                // addresses = geocoder.getFromLocation(23.2601154, 77.4203411, 1);
                addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (addresses != null && addresses.size() > 0) {
                Address address = addresses.get(0);
                StringBuilder sb = new StringBuilder();
                String address1 = address.getAddressLine(0);
                String address2 = address.getAddressLine(1);
                String city = address.getLocality();
                String state = address.getAdminArea();
                String country = address.getCountryName();
                String postalCode = address.getPostalCode();
                String fullAddress = sb.append(address1).append(" ").toString();
                     /*   .append(address2).append(", ")
                        .append(city).append(", ")
                        .append(state).append(", ")
                        .append(country).append(", ")
                        .append(postalCode).append(", ")
                        .toString();*/
                Prefrence.save(LocationManually.this, Prefrence.CITYNAME, fullAddress);
                if(!isFromHome) {
                    Intent intent = new Intent(LocationManually.this, HomePageActivity.class);
                    startActivity(intent);
                }
                finish();
            } else {
                Toast.makeText(this, "Unable to fetch your location", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private ArrayList findUnAskedPermissions(ArrayList wanted) {
        ArrayList result = new ArrayList();

        for (Object perm : wanted) {
            if (!hasPermission((String) perm)) {
                result.add(perm);
            }
        }

        return result;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {

        } else {
            Toast.makeText(LocationManually.this, "Please enable gps location for better result", Toast.LENGTH_SHORT).show();
        }
    }


    private boolean hasPermission(String permission) {
        if (canMakeSmores()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                return (checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED);
            }
        }
        return true;
    }

    private boolean canMakeSmores() {
        return (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1);
    }


    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {

            case ALL_PERMISSIONS_RESULT:
                for (Object perms : permissionsToRequest) {
                    if (!hasPermission((String) perms)) {
                        permissionsRejected.add(perms);
                    }
                }

                if (permissionsRejected.size() > 0) {


                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (shouldShowRequestPermissionRationale((String) permissionsRejected.get(0))) {
                            showMessageOKCancel("These permissions are mandatory for the application. Please allow access.",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                requestPermissions((String[]) permissionsRejected.toArray(new String[permissionsRejected.size()]), ALL_PERMISSIONS_RESULT);
                                            }
                                        }
                                    });
                            return;
                        }
                    }

                }

                break;
        }

    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(LocationManually.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

/*
    @Override
    protected void onDestroy() {
        super.onDestroy();
        locationTrack.stopListener();
    }
*/

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(locationTrack != null)
            locationTrack.stopListener();
    }

    private void init() {
        locationtxt = findViewById(R.id.currentlocation);
        locationtx = findViewById(R.id.locationtx);
        Places.initialize(getApplicationContext(), getString(R.string.google_maps_key), Locale.US);
        // Initialize the AutocompleteSupportFragment.
        AutocompleteSupportFragment autocompleteFragment = (AutocompleteSupportFragment)
                getSupportFragmentManager().findFragmentById(R.id.autocomplete_fragment);
        etPlace = (EditText) autocompleteFragment.getView().findViewById(R.id.places_autocomplete_search_input);
        etPlace.setText("Search Location");
        etPlace.setTextSize(16);
        etPlace.setTextColor(Color.parseColor("#FF000000"));

        //autocompleteFragment.setCountry("UK");
        ImageView searchIcon = (ImageView)((LinearLayout)autocompleteFragment.getView()).getChildAt(0);
        searchIcon.setImageDrawable(getResources().getDrawable(R.drawable.search));
        // Specify the types of place data to return.
        autocompleteFragment.setPlaceFields(Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG));

        // Set up a PlaceSelectionListener to handle the response.
        autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(@NotNull Place place) {
                // TODO: Get info about the selected place.
                s_source = place.getName();
                Log.i("SHUBHAM", "Place: " + place.getName() + ", " + place.getId());
                srcLatitude = place.getLatLng().latitude;
                srcLlongitude = place.getLatLng().longitude;
                Prefrence.save(LocationManually.this, Prefrence.KEY_LATITUDE, String.valueOf(srcLatitude));
                Prefrence.save(LocationManually.this, Prefrence.KEY_LONGITUDE, String.valueOf(srcLlongitude));
                Prefrence.save(LocationManually.this, Prefrence.CITYNAME, s_source);
                if(!isFromHome) {
                    Intent intent = new Intent(LocationManually.this, HomePageActivity.class);
                    startActivity(intent);
                }
                finish();
            }
            @Override
            public void onError(@NotNull Status status) {
                // TODO: Handle the error.
                Log.i("SHUBHAM", "An error occurred: " + status);
                s_source = "";
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
       
    }
}