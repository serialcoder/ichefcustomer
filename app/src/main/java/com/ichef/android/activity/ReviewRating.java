package com.ichef.android.activity;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.ichef.android.R;
import com.ichef.android.requestmodel.rate.RateOrderRequest;
import com.ichef.android.responsemodel.Address.AddAddressResponse;
import com.ichef.android.retrofit.APIInterface;
import com.ichef.android.retrofit.ApiClient;
import com.ichef.android.utils.Prefrence;
import com.ichef.android.utils.TransparentProgressDialog;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ReviewRating extends AppCompatActivity {

    EditText et_review;
    TextView txt_count,txtName;
    RatingBar ratingBar;
    TextView post, cancel;
    private String vendorName,orderId,vendorId;
    ImageView back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_review_rating);
        init();
        onclick();
    }

    private void init() {
        et_review = findViewById(R.id.et_review);
        txt_count = findViewById(R.id.textcount);
        ratingBar = findViewById(R.id.rate);
        post = findViewById(R.id.post);
        cancel = findViewById(R.id.cancel);
        back = findViewById(R.id.back);
        txtName = findViewById(R.id.txtName);
        orderId= getIntent().getStringExtra("orderId");
        vendorName= getIntent().getStringExtra("vendorName");
        vendorId= getIntent().getStringExtra("vendorId");
        txtName.setText(vendorName);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void onclick() {
        et_review.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String currentText = editable.toString();
                int currentLength = currentText.length();
                txt_count.setText(currentLength + "/" + 500);
            }
        });
        post.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                //Getting the rating and displaying it in the toast
                Float rating = ratingBar.getRating();
                //Toast.makeText(getApplicationContext(), "You have rated :   " + rating, Toast.LENGTH_SHORT).show();
                rateOrder();
            }

        });

    }
    private void rateOrder() {
        TransparentProgressDialog dialog = new TransparentProgressDialog(this);
        dialog.show();
        String token = Prefrence.get(this, Prefrence.KEY_TOKEN);
        RateOrderRequest rateOrderRequest=new RateOrderRequest();
        rateOrderRequest.setOrderID(orderId);
        rateOrderRequest.setRating(ratingBar.getRating());
        rateOrderRequest.setComment(et_review.getText().toString());
        rateOrderRequest.setVendor(vendorId);

        APIInterface apiInterface = ApiClient.getClient().create(APIInterface.class);
        Call<AddAddressResponse> call = apiInterface.CallRateOrder("Bearer " + token,rateOrderRequest);
        call.enqueue(new Callback<AddAddressResponse>() {
            @Override
            public void onResponse(Call<AddAddressResponse> call, Response<AddAddressResponse> response) {
                if (response.body().getStatus()) {
                    Toast.makeText(ReviewRating.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    finish();
                } else {
                    Toast.makeText(ReviewRating.this, " " + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }
                dialog.dismiss();
            }

            @Override
            public void onFailure(Call<AddAddressResponse> call, Throwable t) {

                dialog.dismiss();
                call.cancel();
            }
        });
    }

}