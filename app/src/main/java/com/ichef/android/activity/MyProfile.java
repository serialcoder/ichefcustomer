package com.ichef.android.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import com.ichef.android.R;
import com.ichef.android.adapter.SlidingImage_Adapter;
import com.ichef.android.responsemodel.banner.BannerListModel;
import com.ichef.android.responsemodel.banner.Result;
import com.ichef.android.responsemodel.profile.ProfileModel;
import com.ichef.android.retrofit.APIInterface;
import com.ichef.android.retrofit.ApiClient;
import com.ichef.android.utils.Prefrence;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyProfile extends AppCompatActivity {

    LinearLayout logout,llAbout,serviceprovider,tvRefer,llDispute;
    LinearLayout yourorder, myaddress, llorders;
    LinearLayout bookmark, notification, settings, payment, mycart;
    TextView runningorder, completedorder, cancelledorder, myreward ;
    TextView txtbookmark, txtnotification, txtsetting, txtpayment;
    TextView txname,txtEmail;
    Context mContext = this;
    private ViewPager mPager;
    List<Result> mBannerListData = new ArrayList<>();
    private  int currentPage = 0;
    private  int NUM_PAGES = 0;
    CircleImageView imgProfile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_profile);
        ImageView back = findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        init();
        onclick();
        getBannerList();

    }

    private void init() {
        llAbout = findViewById(R.id.llAbout);
        llDispute = findViewById(R.id.llDispute);
        imgProfile = findViewById(R.id.imgProfile);
        mPager = findViewById(R.id.pager);
        bookmark = findViewById(R.id.bookmark);
        payment = findViewById(R.id.payment);
        notification = findViewById(R.id.notification);
        settings = findViewById(R.id.setting);
        llorders = findViewById(R.id.llorders);
        yourorder = findViewById(R.id.yourorder);
        myaddress = findViewById(R.id.myaddress);
        runningorder = findViewById(R.id.runningorder);
        completedorder = findViewById(R.id.completedorder);
        cancelledorder = findViewById(R.id.cancelledorder);
        myreward = findViewById(R.id.myreward);
        mycart = findViewById(R.id.mycart);
        serviceprovider = findViewById(R.id.serviceprovider);
        logout = findViewById(R.id.lllogout);
        txname = findViewById(R.id.txname);
        txtEmail = findViewById(R.id.txtEmail);
        tvRefer = findViewById(R.id.tvRefer);
        txtnotification = findViewById(R.id.txtnotification);
        txtbookmark = findViewById(R.id.txtbookmark);
        txtsetting = findViewById(R.id.txtsetting);

    }

    @Override
    protected void onResume() {
        super.onResume();
        getProfile();

    }

    private void onclick() {
        llAbout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(mContext,ShowWebView.class);
                startActivity(i);
            }
        });
        imgProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(mContext, UpdateProfilePicActivity.class).putExtra("isFromUpdateScreen",true));
            }
        });

        tvRefer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT,
                        "Download iChef app now:\n\nAndroid: https://play.google.com/store/apps/details?id=com.ichef.android \n\nYou can now order your favourite food from any restaurant on demand with the iChef App.");
                sendIntent.setType("text/plain");
                startActivity(sendIntent);
            }
        });
        settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(AnimationUtils.loadAnimation(MyProfile.this, R.anim.image_click));
                /*settings.setBackgroundResource(R.color.themered);
                payment.setBackgroundResource(R.color.lightgrey);
                bookmark.setBackgroundResource(R.color.lightgrey);
                notification.setBackgroundResource(R.color.lightgrey);
                txtnotification.setTextColor(getColor(R.color.black));
                txtbookmark.setTextColor(getColor(R.color.black));
                txtsetting.setTextColor(getColor(R.color.white));*/
                Intent intent = new Intent(MyProfile.this, Settings.class);
                startActivity(intent);
            }
        });
        payment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(AnimationUtils.loadAnimation(MyProfile.this, R.anim.image_click));
                /*settings.setBackgroundResource(R.color.lightgrey);
                payment.setBackgroundResource(R.color.themered);
                bookmark.setBackgroundResource(R.color.lightgrey);
                notification.setBackgroundResource(R.color.lightgrey);*/
               /* Intent intent = new Intent(MyProfile.this, Payment.class);
                startActivity(intent);*/
            }
        });
        bookmark.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(AnimationUtils.loadAnimation(MyProfile.this, R.anim.image_click));
                /*settings.setBackgroundResource(R.color.lightgrey);
                payment.setBackgroundResource(R.color.lightgrey);
                bookmark.setBackgroundResource(R.color.themered);
                notification.setBackgroundResource(R.color.lightgrey);
                txtnotification.setTextColor(getColor(R.color.black));
                txtbookmark.setTextColor(getColor(R.color.white));
                txtsetting.setTextColor(getColor(R.color.black));*/

                Intent intent = new Intent(MyProfile.this, Bookmark.class);
                startActivity(intent);
            }
        });
        notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(AnimationUtils.loadAnimation(MyProfile.this, R.anim.image_click));
                /*settings.setBackgroundResource(R.color.lightgrey);
                payment.setBackgroundResource(R.color.lightgrey);
                bookmark.setBackgroundResource(R.color.lightgrey);
                notification.setBackgroundResource(R.color.themered);
                txtnotification.setTextColor(getColor(R.color.white));
                txtbookmark.setTextColor(getColor(R.color.black));
                txtsetting.setTextColor(getColor(R.color.black));*/
                Intent intent = new Intent(MyProfile.this, Notification.class);
                startActivity(intent);
            }
        });


        yourorder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(AnimationUtils.loadAnimation(MyProfile.this, R.anim.image_click));
                Intent intent = new Intent(MyProfile.this, RunningOrder.class);
                startActivity(intent);
                //llorders.setVisibility(View.VISIBLE);

            }
        });


        myaddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(AnimationUtils.loadAnimation(MyProfile.this, R.anim.image_click));
                llorders.setVisibility(View.GONE);
                Intent intent = new Intent(MyProfile.this, MyAddress.class);
                startActivity(intent);
            }
        });

        runningorder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(AnimationUtils.loadAnimation(MyProfile.this, R.anim.image_click));
                runningorder.setBackgroundResource(R.color.themered);
                completedorder.setBackgroundResource(R.color.white);
                cancelledorder.setBackgroundResource(R.color.white);
                runningorder.setTextColor(Color.parseColor("#FFFFFFFF"));
                completedorder.setTextColor(Color.parseColor("#FF000000"));
                cancelledorder.setTextColor(Color.parseColor("#FF000000"));
                Intent intent = new Intent(MyProfile.this, RunningOrder.class);
                startActivity(intent);
            }
        });
        completedorder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(AnimationUtils.loadAnimation(MyProfile.this, R.anim.image_click));
                runningorder.setBackgroundResource(R.color.white);
                completedorder.setBackgroundResource(R.color.themered);
                cancelledorder.setBackgroundResource(R.color.white);
                runningorder.setTextColor(Color.parseColor("#FF000000"));
                completedorder.setTextColor(Color.parseColor("#FFFFFFFF"));
                cancelledorder.setTextColor(Color.parseColor("#FF000000"));
                Intent intent = new Intent(MyProfile.this, CompletedOrder.class);
                startActivity(intent);
            }
        });
        cancelledorder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(AnimationUtils.loadAnimation(MyProfile.this, R.anim.image_click));
                runningorder.setBackgroundResource(R.color.white);
                completedorder.setBackgroundResource(R.color.white);
                cancelledorder.setBackgroundResource(R.color.themered);
                runningorder.setTextColor(Color.parseColor("#FF000000"));
                completedorder.setTextColor(Color.parseColor("#FF000000"));
                cancelledorder.setTextColor(Color.parseColor("#FFFFFFFF"));
                Intent intent = new Intent(MyProfile.this, CompletedOrder.class);
                startActivity(intent);
            }
        });

        myreward.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(AnimationUtils.loadAnimation(MyProfile.this, R.anim.image_click));
                llorders.setVisibility(View.GONE);
                Intent intent = new Intent(MyProfile.this, MyReward.class);
                startActivity(intent);
            }
        });
        mycart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(AnimationUtils.loadAnimation(MyProfile.this, R.anim.image_click));
                llorders.setVisibility(View.GONE);
                Intent intent = new Intent(MyProfile.this, MyCart.class);
                startActivity(intent);
            }
        });

        llDispute.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(AnimationUtils.loadAnimation(MyProfile.this, R.anim.image_click));
                llorders.setVisibility(View.GONE);
                Intent intent = new Intent(MyProfile.this, DisputeOrderList.class);
                startActivity(intent);
            }
        });

        serviceprovider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String appPackageName = "com.ichefbusiness.android"; // getPackageName() from Context or Activity object
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                }
            }
        });
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                {

                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(MyProfile.this);
                    alertDialog.setMessage("Are you sure you want to Logout?");
                    alertDialog.setIcon(R.drawable.logout);
                    alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            Prefrence.save(getApplication(), Prefrence.KEY_USER_ID, "");
                            Prefrence.save(getApplication(), Prefrence.KEY_FIRST_NAME, "");
                            Prefrence.save(getApplication(), Prefrence.KEY_EMAIL_ID, "");
                            Prefrence.save(getApplication(), Prefrence.KEY_MOBILE_NO, "");
                            Prefrence.save(getApplication(), Prefrence.KEY_USERTYPE, "");
                            Prefrence.save(getApplication(), Prefrence.KEY_USER_ID, "");
                            Prefrence.clearPreference(MyProfile.this);
                            Intent in = new Intent(MyProfile.this, MobileLogin.class);
                            startActivity(in);
                            finishAffinity();
                            Toast.makeText(getApplicationContext(), "Logout", Toast.LENGTH_SHORT).show();
                        }
                    });

                    // Setting Negative "NO" Button
                    alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // Write your code here to invoke NO event
                            dialog.dismiss();
                            dialog.cancel();
                        }
                    });
                    // Showing Alert Message
                    alertDialog.show();
                }
            }
        });


    }

    private void getBannerList() {

        APIInterface apiInterface = ApiClient.getClient().create(APIInterface.class);
        Call<BannerListModel> resultCall = apiInterface.CallBannerList();
        //Call<HomePageListModel> resultCall = apiInterface.CallProductList(token,"7.448166400000001","9.0570752");

        resultCall.enqueue(new Callback<BannerListModel>() {
            @Override
            public void onResponse(Call<BannerListModel> call, Response<BannerListModel> response) {
                if (response.body().getStatus()) {
                    mBannerListData = response.body().getResult();
                    if (mBannerListData.size() == 0) {
                        mPager.setVisibility(View.GONE);
                    } else {
                        mPager.setVisibility(View.VISIBLE);
                        ArrayList<String> image_list = new ArrayList<>();
                        if(mBannerListData != null && mBannerListData.size()>0){
                            for (int i=0;i<mBannerListData.size();i++){
                                image_list.add(mBannerListData.get(i).getImage());
                            }
                            mPager.setAdapter(new SlidingImage_Adapter(mContext, image_list));
                            final float density = getResources().getDisplayMetrics().density;
                            NUM_PAGES = image_list.size();

                            // Auto start of viewpager
                            if (image_list.size() > 0) {
                                final Handler handler = new Handler();
                                final Runnable Update = new Runnable() {
                                    public void run() {
                                        if (currentPage == NUM_PAGES) {
                                            currentPage = 0;
                                        }
                                        mPager.setCurrentItem(currentPage++, true);
                                    }
                                };

                                Timer swipeTimer = new Timer();
                                swipeTimer.schedule(new TimerTask() {
                                    @Override
                                    public void run() {
                                        handler.post(Update);
                                    }
                                }, 5000, 4000);
                            }
                        }
                    }
                } else {
                    mPager.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<BannerListModel> call, Throwable t) {
                mPager.setVisibility(View.GONE);
                call.cancel();
            }
        });
    }

    private void getProfile() {
        APIInterface apiInterface = ApiClient.getClient().create(APIInterface.class);
        Call<ProfileModel> call = apiInterface.GetProfile("Bearer " + Prefrence.get(mContext, Prefrence.KEY_TOKEN));
        call.enqueue(new Callback<ProfileModel>() {
            @Override
            public void onResponse(Call<ProfileModel> call, Response<ProfileModel> response) {
                if (response.body().getStatus()) {
                    String lname = response.body().getParam().getLastname() != null ? response.body().getParam().getLastname().toString() : "";
                    txname.setText(response.body().getParam().getFirstname()+" "+lname);
                    txtEmail.setText(response.body().getParam().getEmail());
                    if(response.body().getParam().getDisplayPicture() != null){
                        Picasso.get().load(response.body().getParam().getDisplayPicture())
                                .placeholder(R.drawable.userprofilered)
                                .into(imgProfile);
                    }
                }
            }

            @Override
            public void onFailure(Call<ProfileModel> call, Throwable t) {
                call.cancel();
            }
        });
    }

}