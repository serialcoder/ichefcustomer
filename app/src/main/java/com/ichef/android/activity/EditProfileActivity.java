package com.ichef.android.activity;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.ichef.android.R;
import com.ichef.android.requestmodel.user.EditProfileRequest;
import com.ichef.android.responsemodel.Address.AddAddressResponse;
import com.ichef.android.retrofit.APIInterface;
import com.ichef.android.retrofit.ApiClient;
import com.ichef.android.utils.Prefrence;
import com.ichef.android.utils.TransparentProgressDialog;

import retrofit2.Call;
import retrofit2.Callback;

public class EditProfileActivity extends AppCompatActivity {

    LinearLayout llUpdate;
    EditText name, mail, phone, password, confirmpassword;
    private static String VALIDATION_MSG;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);

        ImageView back = findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        init();
        onclick();
    }

    private void onclick() {
        llUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validation()) {
                    v.startAnimation(AnimationUtils.loadAnimation(EditProfileActivity.this, R.anim.image_click));
                    updateProfile();
                }
            }
        });
    }

    private void updateProfile() {
        TransparentProgressDialog dialog = new TransparentProgressDialog(EditProfileActivity.this);
        dialog.show();
        String token = Prefrence.get(this, Prefrence.KEY_TOKEN);
        EditProfileRequest request = new EditProfileRequest();
        request.setEmail(mail.getText().toString());
        request.setFirstname(name.getText().toString());
        request.setId(Prefrence.get(this, Prefrence.KEY_USER_ID));

        APIInterface apiInterface = ApiClient.getClient().create(APIInterface.class);
        Call<AddAddressResponse> resultCall = apiInterface.CallUpdateProfile("Bearer " + token, request);
        resultCall.enqueue(new Callback<AddAddressResponse>() {

            @Override
            public void onResponse(Call<AddAddressResponse> call, retrofit2.Response<AddAddressResponse> response) {

                if (response.body().getStatus().equals(true)) {
                    Toast.makeText(EditProfileActivity.this, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    Prefrence.save(EditProfileActivity.this, Prefrence.KEY_EMAIL_ID, mail.getText().toString());
                    Prefrence.save(EditProfileActivity.this, Prefrence.KEY_FIRST_NAME, name.getText().toString());
                    finish();
                } else {
                    Toast.makeText(EditProfileActivity.this, "Please try again", Toast.LENGTH_SHORT).show();
                }
                dialog.dismiss();
            }

            @Override
            public void onFailure(Call<AddAddressResponse> call, Throwable t) {
                Toast.makeText(EditProfileActivity.this, "Please check your Internet Connection", Toast.LENGTH_SHORT).show();
                dialog.dismiss();
            }
        });

    }


    private boolean validation() {

        boolean flag = true;

        if (name.getText().toString().trim().length() == 0) {
            flag = false;
            VALIDATION_MSG = "Name is required!";
            name.setError(VALIDATION_MSG);
        } else if (!isValidEmail(mail.getText().toString().trim())) {
            flag = false;
            VALIDATION_MSG = "Please enter valid email address!";
            mail.setError(VALIDATION_MSG);
        }
        return flag;
    }

    public static boolean isValidEmail(CharSequence target) {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }


    private void init() {
        llUpdate = findViewById(R.id.llUpdate);
        name = findViewById(R.id.etname);
        mail = findViewById(R.id.etemail);
        phone = findViewById(R.id.etphone);
        password = findViewById(R.id.etpassword);
        confirmpassword = findViewById(R.id.etconfrmpass);
        mail.setText(Prefrence.get(this, Prefrence.KEY_EMAIL_ID));
        name.setText(Prefrence.get(this, Prefrence.KEY_FIRST_NAME));
        phone.setText(Prefrence.get(this, Prefrence.KEY_MOBILE_NO));
    }
}