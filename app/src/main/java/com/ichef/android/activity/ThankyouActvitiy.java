package com.ichef.android.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import com.ichef.android.R;


public class ThankyouActvitiy extends AppCompatActivity {

    TextView txtOrderAmount,txtAddressValue;
    String amount = "";;
    String deliveryAddress = "";
    Button imgOrderView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_thankyou);

        imgOrderView =  findViewById(R.id.imgOrderView);
        txtOrderAmount =  findViewById(R.id.txtOrderAmount);
        txtAddressValue =  findViewById(R.id.txtAddressValue);
        if(getIntent().getStringExtra("totalAmount") != null){
            amount = getIntent().getStringExtra("totalAmount");
            txtOrderAmount.setText("Your Order has been successfully placed We have recevied the amount of "+getString(R.string.nigiriacurrency)+" "+amount);
        }
        if(getIntent().getStringExtra("deliveryAddress") != null){
            deliveryAddress = getIntent().getStringExtra("deliveryAddress");
            txtAddressValue.setText(deliveryAddress);
        }

        Button imgGoToHome = (Button) findViewById(R.id.imgGoToHome);
        imgGoToHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent newIntent=new Intent(ThankyouActvitiy.this,HomePageActivity.class);
                startActivity(newIntent);
                finishAffinity();
            }
        });

        imgOrderView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent newIntent=new Intent(ThankyouActvitiy.this,RunningOrder.class);
                startActivity(newIntent);
                finish();
            }
        });
    }

    @SuppressLint("WrongConstant")
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(this, HomePageActivity.class);
        //intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finishAffinity();
    }



}
