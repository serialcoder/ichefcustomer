package com.ichef.android.activity;

import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.maps.model.LatLng;
import com.ichef.android.R;
import com.ichef.android.requestmodel.Address.AddAddressRequest;
import com.ichef.android.responsemodel.Address.AddAddressResponse;
import com.ichef.android.responsemodel.Address.City.CityResponse;
import com.ichef.android.responsemodel.Address.GetAddress.City;
import com.ichef.android.retrofit.APIInterface;
import com.ichef.android.retrofit.ApiClient;
import com.ichef.android.utils.Prefrence;
import com.ichef.android.utils.TransparentProgressDialog;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddNewAddress extends AppCompatActivity {

    ImageView back;
    EditText etLocationName, etAddressLine1, etAddressLine2, etAddressLine3, etMobile, etPersonName, etZipCode;
    //  etCountry,etCity,,etState;
    Button btnSave;
    String locationName, addressLine1, addressLine2, addressLine3, city, personName, mobile;
    Spinner  spnCity;
    String finalAddress;
    private List<com.ichef.android.responsemodel.Address.City.Result> cityList;
    ArrayList<String> mCityList = new ArrayList<>();
    String selectedCityID = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_address);
        init();
    }

    private void init() {
        back = findViewById(R.id.back);
        etLocationName = findViewById(R.id.etLocationName);
        etAddressLine1 = findViewById(R.id.etAddressLine1);
        etAddressLine2 = findViewById(R.id.etAddressLine2);
        etAddressLine3 = findViewById(R.id.etAddressLine3);
        etZipCode = findViewById(R.id.etZipCode);
        spnCity = findViewById(R.id.spnCity);
        etPersonName = findViewById(R.id.etPersonName);
        etMobile = findViewById(R.id.etMobile);
        btnSave = findViewById(R.id.btnSave);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validateData()) {
                    finalAddress = addressLine1 + " " + addressLine2 + " " + spnCity.getSelectedItem();
                    goToLocationFromAddress(finalAddress);
                }
            }
        });

        spnCity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                // Get the value selected by the user
                // e.g. to store it as a field or immediately call a method
                selectedCityID = getSelectedCityID(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        getCity();
    }

    public void goToLocationFromAddress(String strAddress) {
        //Create coder with Activity context - this
        TransparentProgressDialog dialog = new TransparentProgressDialog(AddNewAddress.this);
        dialog.show();
        Geocoder coder = new Geocoder(this);
        List<Address> address;

        try {
            //Get latLng from String
            address = coder.getFromLocationName(strAddress, 5);

            //check for null
            if (address != null) {

                //Lets take first possibility from the all possibilities.
                try {
                    Address location = address.get(0);
                    LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
                    AddAddressApiCall(latLng);
                    //Animate and Zoon on that map location

                } catch (IndexOutOfBoundsException er) {
                    Double latt = Double.parseDouble(Prefrence.get(AddNewAddress.this,Prefrence.KEY_LATITUDE)) ;
                    Double longg = Double.parseDouble(Prefrence.get(AddNewAddress.this,Prefrence.KEY_LONGITUDE)) ;
                    LatLng latLng = new LatLng(latt, longg);
                    AddAddressApiCall(latLng);
                }

            }
            dialog.dismiss();

        } catch (IOException e) {
            e.printStackTrace();
            dialog.dismiss();
        }
    }


    private void getCity() {
        TransparentProgressDialog dialog = new TransparentProgressDialog(AddNewAddress.this);
        dialog.show();
        String token = Prefrence.get(AddNewAddress.this, Prefrence.KEY_TOKEN);
        APIInterface apiInterface = ApiClient.getClient().create(APIInterface.class);
        Call<CityResponse> call = apiInterface.GetCity("Bearer " + token);
        call.enqueue(new Callback<CityResponse>() {
            @Override
            public void onResponse(Call<CityResponse> call, Response<CityResponse> response) {
                dialog.dismiss();
                if (response.body().getStatus()) {
                    //  dialog.dismiss();
                    cityList = response.body().getResult();
                    if(cityList != null && cityList.size()>0){
                        for (int i=0;i<cityList.size();i++){
                            mCityList.add(cityList.get(i).getCityname());
                        }
                    }
                    ArrayAdapter countryAdapter = new ArrayAdapter(AddNewAddress.this, R.layout.support_simple_spinner_dropdown_item, mCityList);
                    spnCity.setAdapter(countryAdapter);

                } else {
                    // dialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<CityResponse> call, Throwable t) {

                dialog.dismiss();
                call.cancel();
            }
        });
    }

    private void AddAddressApiCall(LatLng latLng) {
        TransparentProgressDialog dialog = new TransparentProgressDialog(this);
        dialog.show();
        String token = Prefrence.get(this, Prefrence.KEY_TOKEN);
        AddAddressRequest request = new AddAddressRequest();
        request.setStreetNumber(addressLine1);
        request.setStreetName(addressLine2);
        request.setCity(city);
        request.setName(personName);
        request.setMobile(mobile);
        request.setLatitude(latLng.latitude);
        request.setLongitude(latLng.longitude);
        APIInterface apiInterface = ApiClient.getClient().create(APIInterface.class);
        Call<AddAddressResponse> resultCall = apiInterface.CallAddAddress("Bearer " + token, request);
        resultCall.enqueue(new Callback<AddAddressResponse>() {

            @Override
            public void onResponse(Call<AddAddressResponse> call, retrofit2.Response<AddAddressResponse> response) {
                if (response.body().getStatus()) {
                    Toast.makeText(AddNewAddress.this, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    finish();
                } else {
                    Toast.makeText(AddNewAddress.this, "Please enter valid address", Toast.LENGTH_SHORT).show();
                }
                dialog.dismiss();
            }

            @Override
            public void onFailure(Call<AddAddressResponse> call, Throwable t) {
                dialog.dismiss();
                Toast.makeText(AddNewAddress.this, "Please check your Internet Connection", Toast.LENGTH_SHORT).show();
            }
        });
    }


    private boolean validateData() {
        locationName = etLocationName.getText().toString();
        addressLine1 = etAddressLine1.getText().toString();
        addressLine2 = etAddressLine2.getText().toString();
        addressLine3 = etAddressLine3.getText().toString();
        city = selectedCityID;
        personName = etPersonName.getText().toString();
        mobile = etMobile.getText().toString();
        Boolean isValid = true;
        if (addressLine1.equalsIgnoreCase("")) {
            etAddressLine1.setError(getString(R.string.enter_street_number));
            isValid = false;
        }
        if (addressLine2.equalsIgnoreCase("")) {
            etAddressLine2.setError(getString(R.string.enter_street_name));
            isValid = false;
        }

        if (city.equalsIgnoreCase("")) {
            Toast.makeText(this, getString(R.string.enter_city), Toast.LENGTH_SHORT).show();
            isValid = false;
        }

        if (personName.equalsIgnoreCase("")) {
            etPersonName.setError(getString(R.string.enter_name));
            isValid = false;
        }
        if (mobile.equalsIgnoreCase("")) {
            etMobile.setError(getString(R.string.enter_mobile));
            isValid = false;
        } else if (!isValidMobile(mobile)) {
            isValid = false;
            etMobile.setError(getString(R.string.valid_phone));
        }
        return isValid;
    }

    private boolean isValidMobile(String phone) {
        if (!Pattern.matches("[a-zA-Z]+", phone)) {
            return phone.length() > 8 && phone.length() <= 30;
        }
        return false;
    }

    public String getSelectedCityID(int pos){
        if(cityList != null && cityList.size()>0){
            for (int i=0;i<cityList.size();i++){
                if(cityList.get(i).getCityname().equalsIgnoreCase(mCityList.get(pos))){
                    return cityList.get(i).getId();
                }
            }
        }
        return "0";
    }
}