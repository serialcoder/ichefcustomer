package com.ichef.android.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.preference.Preference;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.facebook.internal.Utility;
import com.google.gson.Gson;
import com.ichef.android.R;
import com.ichef.android.adapter.ChatAdaptor;
import com.ichef.android.adapter.DisputeOrderAdapter;
import com.ichef.android.requestmodel.dispute.SendMessageModel;
import com.ichef.android.responsemodel.dispute.DisputedModel;
import com.ichef.android.responsemodel.dispute.Result;
import com.ichef.android.responsemodel.disputeMessage.DisputedMessageModel;
import com.ichef.android.responsemodel.login.DeviceTokenResponse;
import com.ichef.android.retrofit.APIInterface;
import com.ichef.android.retrofit.ApiClient;
import com.ichef.android.utils.Prefrence;
import com.ichef.android.utils.TransparentProgressDialog;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DisputeMessageActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {

    APIInterface apiInterface;
    private RecyclerView mChatRecyclerView;
    private EditText mEtMessage;
    SwipeRefreshLayout mSwipeRefreshlayout;
    private List<com.ichef.android.responsemodel.disputeMessage.Result> chatModel;
    TransparentProgressDialog dialog;
    ChatAdaptor rv_MyProjectAdapter;
    RecyclerView.LayoutManager rv_MyProjectLayoutManager;
    String disputeID = "";
    LinearLayout rlFooter;
    TextView tvStatus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        rlFooter = findViewById(R.id.rlFooter);
        mSwipeRefreshlayout = findViewById(R.id.swipeRefresh);
        mChatRecyclerView = findViewById(R.id.chatRecyclerView);
        tvStatus = findViewById(R.id.tvStatus);
        mSwipeRefreshlayout.setOnRefreshListener(this);

        disputeID = getIntent().getStringExtra("disputeID");

        if(getIntent().getStringExtra("status")!= null && getIntent().getStringExtra("status").equalsIgnoreCase("Opened")){
            tvStatus.setText("Dispute is Open");
            tvStatus.setBackgroundColor(getColor(R.color.greenTint));
            rlFooter.setVisibility(View.VISIBLE);
        }else {
            tvStatus.setText("Dispute has been Closed");
            tvStatus.setBackgroundColor(getColor(R.color.red));
            rlFooter.setVisibility(View.GONE);
        }

        mChatRecyclerView.setHasFixedSize(true);
        rv_MyProjectLayoutManager = new LinearLayoutManager(DisputeMessageActivity.this);
        mChatRecyclerView.setLayoutManager(rv_MyProjectLayoutManager);

        mEtMessage = findViewById(R.id.etMessage);
        //mTxtUserName.setText(chatUserModel.getName());
        GetChatUserApiCall();
        dialog = new TransparentProgressDialog(DisputeMessageActivity.this);
        dialog.show();
    }


    private void GetChatUserApiCall() {
        String token = "Bearer " + Prefrence.get(this, Prefrence.KEY_TOKEN);
        apiInterface = ApiClient.getClient().create(APIInterface.class);
        Call<DisputedMessageModel> call = apiInterface.GetDisputesMessage(token,disputeID);
        call.enqueue(new Callback<DisputedMessageModel>() {
            @Override
            public void onResponse(Call<DisputedMessageModel> call, Response<DisputedMessageModel> response) {
                if (mSwipeRefreshlayout != null) {
                    mSwipeRefreshlayout.setRefreshing(false);
                }
                if (response.body().getStatus()) {
                    dialog.dismiss();
                    chatModel = response.body().getResult();
                    if(chatModel != null && chatModel.size()>0){
                        setProduct();
                    }else{
                    }
                } else {
                    dialog.dismiss();
                    Toast.makeText(DisputeMessageActivity.this, "No Data Found!", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<DisputedMessageModel> call, Throwable t) {
                if (mSwipeRefreshlayout != null) {
                    mSwipeRefreshlayout.setRefreshing(false);
                }
                dialog.dismiss();
                call.cancel();
            }
        });
    }

    private void setProduct() {
        if (chatModel != null && chatModel.size() > 0) {
            rv_MyProjectAdapter = new ChatAdaptor(DisputeMessageActivity.this,
                    (ArrayList<com.ichef.android.responsemodel.disputeMessage.Result>) chatModel,disputeID);
            mChatRecyclerView.setAdapter(rv_MyProjectAdapter);
        }
    }



    public void backClicked(View view) {
        super.onBackPressed();
    }

    public void sendMessageClicked(View view) {
        String message = mEtMessage.getText().toString().trim();
        if(!message.isEmpty()){
            SendMessageModel messageModel = new SendMessageModel();
            messageModel.setDisputeID(disputeID);
            messageModel.setMessage(message);
            messageModel.setFile("");
            sendMessage(messageModel);
        }
    }


    private void sendMessage(SendMessageModel messageModel) {
        dialog.show();
        String token = "Bearer " + Prefrence.get(this, Prefrence.KEY_TOKEN);
        apiInterface = ApiClient.getClient().create(APIInterface.class);
        Call<DeviceTokenResponse> call = apiInterface.sendMessage(token,messageModel);
        call.enqueue(new Callback<DeviceTokenResponse>() {
            @Override
            public void onResponse(Call<DeviceTokenResponse> call, Response<DeviceTokenResponse> response) {
                if (response.body().getStatus()) {
                    mEtMessage.setText("");
                    dialog.dismiss();
                    GetChatUserApiCall();
                } else {
                    dialog.dismiss();
                    Toast.makeText(DisputeMessageActivity.this, "No Data Found!", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<DeviceTokenResponse> call, Throwable t) {
                dialog.dismiss();
                call.cancel();
            }
        });
    }


    @Override
    public void onRefresh() {
        GetChatUserApiCall();
        dialog = new TransparentProgressDialog(DisputeMessageActivity.this);
        dialog.show();
    }
}