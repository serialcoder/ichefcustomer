package com.ichef.android.activity;

import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;

import androidx.appcompat.app.AppCompatActivity;

import com.ichef.android.R;
import com.ichef.android.utils.Prefrence;
import com.splunk.mint.Mint;

public class SplashScreen extends AppCompatActivity {


    Handler handler;
    Context mContext = this;
    LocationManager locationManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        Mint.initAndStartSession(SplashScreen.this, "0d3969b2");
        Prefrence.save(this, Prefrence.KEY_LATITUDE, "");
        Prefrence.save(this, Prefrence.KEY_LONGITUDE, "");
        Prefrence.save(this, Prefrence.CITYNAME, "");
        Prefrence.save(this, Prefrence.KEY_DELIVERY_LATITUDE, "");
        Prefrence.save(this, Prefrence.KEY_DELIVERY_LONGITUDE, "");
        Prefrence.save(this, Prefrence.KEY_DELIVERY_CITYNAME, "");
        Prefrence.save(this, Prefrence.KEY_DELIVERY_NAME, "");
        Prefrence.save(this, Prefrence.KEY_DELIVERY_MOBILE_NO, "");
        letsStart();
    }

    public void letsStart() {
        handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                String userID = Prefrence.get(SplashScreen.this, Prefrence.KEY_USER_ID);
                if (userID.equalsIgnoreCase(null) || userID.equalsIgnoreCase("") ||
                        userID.equalsIgnoreCase("0")) {

                    Intent intent = new Intent(SplashScreen.this, MobileLogin.class);
                    startActivity(intent);
                    finish();
                } else {

                    if (Prefrence.get(mContext, Prefrence.KEY_LATITUDE).equalsIgnoreCase("") ||
                            Prefrence.get(mContext, Prefrence.KEY_LATITUDE).equalsIgnoreCase("0.0")) {
                        Intent intent = new Intent(SplashScreen.this, NiceToMeetYou.class);
                        startActivity(intent);
                        finish();
                    } else {
                        Intent intent = new Intent(SplashScreen.this, HomePageActivity.class);
                        startActivity(intent);
                        finish();
                    }
                }
            }
        }, 1000);
    }


}