package com.ichef.android.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.ichef.android.R;
import com.ichef.android.requestmodel.payment.PaymentItem;
import com.ichef.android.responsemodel.payment.PaymentResponseModel;
import com.ichef.android.responsemodel.verify.VerifyResponseModel;
import com.ichef.android.retrofit.APIInterface;
import com.ichef.android.retrofit.ApiClient;
import com.ichef.android.utils.Prefrence;
import com.ichef.android.utils.VerifyProgressDialog;

import java.util.Set;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ShowWebView extends AppCompatActivity {


    private WebView webView;
    Context mContext= this;
    ImageView imgBack;
    ProgressBar pd_loading;
    public static final String USER_AGENT = "Mozilla/5.0 (Linux; Android 4.1.1; Galaxy Nexus Build/JRO03C) AppleWebKit/535.19 (KHTML, like Gecko) Chrome/18.0.1025.166 Mobile Safari/535.19";
    Double mTotalAmount = 0.0;
    String vendorID = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_webview);
        TextView tvTitle = findViewById(R.id.tvTitle);
        tvTitle.setText("About Us");
        ImageView back = findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        mTotalAmount = getIntent().getDoubleExtra("totalAmount", 0.0);
        vendorID = getIntent().getStringExtra("vendorID");

        pd_loading =  findViewById(R.id.pd_loading);
        webView = (WebView) findViewById(R.id.webView1);

        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        webView.getSettings().setUserAgentString(USER_AGENT);
        startWebView("https://ichefmobile.com/#/about");
    }



    private void startWebView(String url) {

        webView.setWebViewClient(new WebViewClient() {
            //If you will not use this method url links are opeen in new brower not in webview
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                pd_loading.setVisibility(View.VISIBLE);
            }

            public void onPageFinished(WebView view, String url) {
                pd_loading.setVisibility(View.GONE);
            }
        });


        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.loadUrl(url);
    }


}