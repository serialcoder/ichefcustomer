package com.ichef.android.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.hbb20.CountryCodePicker;
import com.ichef.android.R;
import com.ichef.android.requestmodel.user.ChangePasswordRequest;
import com.ichef.android.requestmodel.user.LoginRequest;
import com.ichef.android.responsemodel.otprequest.ForgotResponse;
import com.ichef.android.responsemodel.signup.SignupResponse;
import com.ichef.android.retrofit.APIInterface;
import com.ichef.android.retrofit.ApiClient;
import com.ichef.android.utils.TransparentProgressDialog;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForgotPasswordActivity extends AppCompatActivity {

    TransparentProgressDialog dialog;
    CountryCodePicker ccp;
    LinearLayout llCHeckOTP;
    EditText et_mobile,et_OTP,et_password;
    Context mContext = this;
    TextView tvSendOTP;
    String userID = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        init();
        onclick();

    }

    private void init() {
        ccp = findViewById(R.id.ccp);
        llCHeckOTP= findViewById(R.id.llCHeckOTP);
        et_mobile= findViewById(R.id.et_mobile);
        et_OTP= findViewById(R.id.et_OTP);
        tvSendOTP= findViewById(R.id.tvSendOTP);
        et_password= findViewById(R.id.et_password);
    }

    private void onclick() {
        tvSendOTP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(AnimationUtils.loadAnimation(mContext, R.anim.image_click));
                if(et_mobile.getText().toString().equalsIgnoreCase("")){
                    et_mobile.setError(getString(R.string.enter_mobile));
                }else if (!isValidMobile(et_mobile.getText().toString())) {
                    et_mobile.setError(getString(R.string.valid_phone));
                }else{
                    callForgotApi();
                }
            }
        });

        llCHeckOTP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(et_OTP.getText().toString().equalsIgnoreCase("")){
                    et_OTP.setError(getString(R.string.enter_otp));
                }else if(et_password.getText().toString().equalsIgnoreCase("")){
                    et_password.setError(getString(R.string.enter_password));
                }else if(!validate(et_password.getText().toString())){
                    et_password.setError(getString(R.string.validate_password));
                    et_password.requestFocus();
                }else{
                    callUpdatePasswordApi();
                }
            }
        });

    }

    public boolean validate(final String password){
        Pattern pattern;
        Matcher matcher;

        final String PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=\\S+$).{4,}$";

        pattern = Pattern.compile(PASSWORD_PATTERN);
        matcher = pattern.matcher(password);

        return matcher.matches();
    }

    private boolean isValidMobile(String phone) {
        if(!Pattern.matches("[a-zA-Z]+", phone)) {
            return phone.length() > 8 && phone.length() <= 30;
        }
        return false;
    }

    private void callForgotApi() {
        dialog=new TransparentProgressDialog(mContext);
        dialog.show();

        String myMobile = ccp.getSelectedCountryCodeWithPlus()+""+et_mobile.getText().toString().trim().replace(" ", "");;
        LoginRequest loginRequest = new LoginRequest();
        loginRequest.setMobileNumber(myMobile);
        APIInterface apiInterface = ApiClient.getClient().create(APIInterface.class);
        Call<ForgotResponse> resultCall = apiInterface.CallForgotPassword(loginRequest);
        resultCall.enqueue(new Callback<ForgotResponse>() {


            @Override
            public void onResponse(Call<ForgotResponse> call, Response<ForgotResponse> response) {
                dialog.dismiss();
                if (response.body().getStatus().equals(true)) {
                    userID = response.body().getParam().getId();
                } else {
                    Toast.makeText(mContext, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ForgotResponse> call, Throwable t) {
                dialog.dismiss();
                Toast.makeText(mContext, getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
            }
        });

    }


    private void callUpdatePasswordApi() {
        dialog=new TransparentProgressDialog(mContext);
        dialog.show();


        ChangePasswordRequest request = new ChangePasswordRequest();
        request.setNew_password(et_password.getText().toString());
        request.setOtp(et_OTP.getText().toString().trim());
        request.setUser_id(userID);

        APIInterface apiInterface = ApiClient.getClient().create(APIInterface.class);
        Call<SignupResponse> resultCall = apiInterface.CallUpdatePassword(request);
        resultCall.enqueue(new Callback<SignupResponse>() {


            @Override
            public void onResponse(Call<SignupResponse> call, Response<SignupResponse> response) {
                dialog.dismiss();
                if (response.body().getStatus().equals(true)) {
                    Toast.makeText(mContext,  response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(mContext, MobileLogin.class);
                    startActivity(intent);
                    finish();
                } else {
                    Toast.makeText(mContext, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<SignupResponse> call, Throwable t) {
                dialog.dismiss();
                Toast.makeText(mContext, getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
            }
        });

    }


}