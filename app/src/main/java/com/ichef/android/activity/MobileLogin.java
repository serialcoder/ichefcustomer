package com.ichef.android.activity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.hbb20.CountryCodePicker;
import com.ichef.android.R;
import com.ichef.android.requestmodel.user.EditProfileRequest;
import com.ichef.android.requestmodel.user.LoginRequest;
import com.ichef.android.requestmodel.user.SignupRequest;
import com.ichef.android.requestmodel.user.SocialLoginRequest;
import com.ichef.android.responsemodel.login.LoginResponse;
import com.ichef.android.responsemodel.signup.SignupResponse;
import com.ichef.android.retrofit.APIInterface;
import com.ichef.android.retrofit.ApiClient;
import com.ichef.android.utils.Prefrence;
import com.ichef.android.utils.TransparentProgressDialog;

import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import okhttp3.Headers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MobileLogin extends AppCompatActivity {

    TransparentProgressDialog dialog;
    private static final int RC_SIGN_IN = 007;
    private static final String TAG = "Hello";
    TextView blank,login,signup,otp,signupnext,tvForgotPassword;
    LinearLayout lllogin,llsignup,llnext;
    RelativeLayout llfb,google;
    EditText et_mobile,et_password;
    private static String VALIDATION_MSG;
    LinearLayout sendotpbtn;
    String text;
    View view;
    CountryCodePicker ccp;
    GoogleSignInClient mGoogleSignInClient;
    Context mContext = this;
    ProgressBar pd_loading;
    CountryCodePicker signup_ccp;
    EditText et_signup_mobile,et_name,et_email,et_signup_password,et_signup_cpassword;
    LinearLayout btnsignup;
    String hsahkey = "";
    LoginButton facebookLogin;
    CallbackManager mcallbackManager;
    FirebaseAuth mAuth;
    ImageView imgHidePassword,imgShowPassword;
    ImageView imgHideSignupPassword,imgShowSignupPassword;
    ImageView imgHideSignupCPassword,imgShowSignupCPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        init();
        onclick();
        printHashKey(this);

    }

    private void init() {
        imgShowPassword = findViewById(R.id.imgShowPassword);
        imgHidePassword = findViewById(R.id.imgHidePassword);
        imgShowSignupPassword = findViewById(R.id.imgShowSignupPassword);
        imgHideSignupPassword = findViewById(R.id.imgHideSignupPassword);
        imgHideSignupCPassword = findViewById(R.id.imgHideSignupCPassword);
        imgShowSignupCPassword = findViewById(R.id.imgShowSignupCPassword);
        ccp = findViewById(R.id.ccp);
        blank= findViewById(R.id.blank);
        login= findViewById(R.id.login);
        signupnext= findViewById(R.id.signupnext);
        llfb= findViewById(R.id.llfb);
        google= findViewById(R.id.google);
        signup = findViewById(R.id.signup);
        otp= findViewById(R.id.otp);
        lllogin = findViewById(R.id.lllogin);
        llsignup = findViewById(R.id.llsignup);
        pd_loading = findViewById(R.id.pd_loading);
        et_mobile= findViewById(R.id.et_mobile);
        et_password= findViewById(R.id.et_password);
        sendotpbtn=findViewById(R.id.cardotp);
        view= findViewById(R.id.viewgreen);
        llnext= findViewById(R.id.llnext);
        signup_ccp= findViewById(R.id.signup_ccp);
        et_signup_mobile= findViewById(R.id.et_signup_mobile);
        et_name= findViewById(R.id.et_name);
        et_email= findViewById(R.id.et_email);
        et_signup_password= findViewById(R.id.et_signup_password);
        et_signup_cpassword= findViewById(R.id.et_signup_cpassword);
        btnsignup= findViewById(R.id.btnsignup);
        tvForgotPassword= findViewById(R.id.tvForgotPassword);

        imgShowPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imgShowPassword.setVisibility(View.GONE);
                imgHidePassword.setVisibility(View.VISIBLE);
                et_password.setTransformationMethod(PasswordTransformationMethod.getInstance());
                et_password.setSelection(et_password.length());
            }
        });

        imgHidePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imgShowPassword.setVisibility(View.VISIBLE);
                imgHidePassword.setVisibility(View.GONE);
                et_password.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                et_password.setSelection(et_password.length());

            }
        });


        imgShowSignupPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imgShowSignupPassword.setVisibility(View.GONE);
                imgHideSignupPassword.setVisibility(View.VISIBLE);
                et_signup_password.setTransformationMethod(PasswordTransformationMethod.getInstance());
                et_signup_password.setSelection(et_signup_password.length());
            }
        });

        imgHideSignupPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imgShowSignupPassword.setVisibility(View.VISIBLE);
                imgHideSignupPassword.setVisibility(View.GONE);
                et_signup_password.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                et_signup_password.setSelection(et_signup_password.length());

            }
        });

        imgShowSignupCPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imgShowSignupCPassword.setVisibility(View.GONE);
                imgHideSignupCPassword.setVisibility(View.VISIBLE);
                et_signup_cpassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
            }
        });

        imgHideSignupCPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imgShowSignupCPassword.setVisibility(View.VISIBLE);
                imgHideSignupCPassword.setVisibility(View.GONE);
                et_signup_cpassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());

            }
        });

        // Google Login
        GoogleSignInOptions gso=new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        mGoogleSignInClient= com.google.android.gms.auth.api.signin.GoogleSignIn.getClient(this,gso);

        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(this);

        // Facebook Login
        mAuth=FirebaseAuth.getInstance();
        FacebookSdk.sdkInitialize(getApplicationContext());
        mcallbackManager=CallbackManager.Factory.create();
        facebookLogin=findViewById(R.id.facebookLogin);
        facebookLogin.setReadPermissions("email", "public_profile");
        facebookLogin.registerCallback(mcallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                handleFacebookToken(loginResult.getAccessToken());
            }

            @Override
            public void onCancel() {

                System.out.println("Error occured cancel");
            }

            @Override
            public void onError(FacebookException e) {
                Toast.makeText(MobileLogin.this, "Error occured", Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }
        });

        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        boolean isLoggedInFacebook = accessToken != null && !accessToken.isExpired();


    }



    public boolean validate(final String password){
        Pattern pattern;
        Matcher matcher;

        final String PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=\\S+$).{4,}$";

        pattern = Pattern.compile(PASSWORD_PATTERN);
        matcher = pattern.matcher(password);

        return matcher.matches();
    }

    private void onclick() {
        sendotpbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(AnimationUtils.loadAnimation(mContext, R.anim.image_click));
                if(et_mobile.getText().toString().equalsIgnoreCase("")){
                    et_mobile.setError(getString(R.string.enter_mobile));
                    et_mobile.requestFocus();
                }else if (!isValidMobile(et_mobile.getText().toString())) {
                    et_mobile.setError(getString(R.string.valid_phone));
                    et_mobile.requestFocus();
                }else if(et_password.getText().toString().equalsIgnoreCase("")){
                    et_password.setError(getString(R.string.enter_password));
                    et_password.requestFocus();
                }else if(!validate(et_password.getText().toString())){
                    et_password.setError(getString(R.string.validate_password));
                    et_password.requestFocus();
                }else{
                    et_password.setError(null);
                    String mobilenumber = et_mobile.getText().toString();
                    String checkFistLetter = mobilenumber.substring(0,1);
                    if(checkFistLetter.equalsIgnoreCase("0")){
                        et_mobile.setError(getString(R.string.valid_phone_withoout_zero));
                        et_mobile.requestFocus();
                    }else{
                        et_mobile.setError(null);
                        callLoginApi();
                    }
                }
            }
        });


        btnsignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                view.startAnimation(AnimationUtils.loadAnimation(mContext, R.anim.image_click));
                if(et_signup_mobile.getText().toString().equalsIgnoreCase("")){
                    et_signup_mobile.setError(getString(R.string.enter_mobile));
                }else if (!isValidMobile(et_signup_mobile.getText().toString())) {
                    et_signup_mobile.setError(getString(R.string.valid_phone));
                }else if(et_name.getText().toString().equalsIgnoreCase("")){
                    et_name.setError(getString(R.string.enter_name));
                }else if(et_email.getText().toString().equalsIgnoreCase("")){
                    et_name.setError(getString(R.string.enter_email));
                }else if (!isValidMail(et_email.getText().toString())) {
                    et_email.setError(getString(R.string.valid_email));
                }else if(et_signup_password.getText().toString().equalsIgnoreCase("")){
                    et_signup_password.setError(getString(R.string.enter_password));
                }else if(et_signup_cpassword.getText().toString().equalsIgnoreCase("")){
                    et_signup_cpassword.setError(getString(R.string.enter_cpassword));
                }else if(!et_signup_cpassword.getText().toString().equalsIgnoreCase(et_signup_password.getText().toString())){
                    et_signup_cpassword.setError(getString(R.string.cpassword_not_match));
                }else if(!validate(et_signup_password.getText().toString())){
                    et_signup_password.setError(getString(R.string.validate_password));
                    et_signup_password.requestFocus();
                }else{

                    et_signup_password.setError(null);
                    String myMobile = et_signup_mobile.getText().toString();
                    String checkFistLetter = myMobile.substring(0,1);
                    if(checkFistLetter.equalsIgnoreCase("0")){
                        et_signup_mobile.setError(getString(R.string.valid_phone_withoout_zero));
                        et_signup_mobile.requestFocus();
                    }else{
                        et_signup_mobile.setError(null);
                        callSignUpApi();
                    }
                }
            }
        });

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(AnimationUtils.loadAnimation(mContext, R.anim.image_click));
                login.setTextColor(Color.parseColor("#FFFFFFFF"));
                signup.setTextColor(Color.parseColor("#D8455E"));
                blank.setText("");
                signup.setText("SIGNUP");
                login.setText("LOGIN");
                lllogin.setVisibility(View.VISIBLE);
                llsignup.setVisibility(View.GONE);


            }
        });
        blank.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(AnimationUtils.loadAnimation(mContext, R.anim.image_click));
                login.setTextColor(Color.parseColor("#FFFFFFFF"));
                signup.setTextColor(Color.parseColor("#D8455E"));
                blank.setText("");
                signup.setText("SIGNUP");
                login.setText("LOGIN");
                lllogin.setVisibility(View.VISIBLE);
                llsignup.setVisibility(View.GONE);

            }
        });
        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                login.setText("SIGNUP");
                signup.setText("");
                blank.setText("LOGIN");
                v.startAnimation(AnimationUtils.loadAnimation(mContext, R.anim.image_click));
                login.setTextColor(Color.parseColor("#FFFFFFFF"));
                signup.setTextColor(Color.parseColor("#D8455E"));
                blank.setTextColor(Color.parseColor("#D8455E"));
                lllogin.setVisibility(View.GONE);
                llsignup.setVisibility(View.VISIBLE);
            }
        });


        llfb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                facebookLogin.performClick();
            }
        });

        tvForgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(mContext,ForgotPasswordActivity.class);
                startActivity(i);
            }
        });


        google.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(AnimationUtils.loadAnimation(mContext, R.anim.image_click));
                Intent signInIntent=mGoogleSignInClient.getSignInIntent();
                startActivityForResult(signInIntent,1);
            }
        });

    }

    private boolean isValidMail(String email) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    private boolean isValidMobile(String phone) {
        if(!Pattern.matches("[a-zA-Z]+", phone)) {
            return phone.length() > 8 && phone.length() <= 30;
        }
        return false;
    }


    public  void printHashKey(Context pContext) {
        try {
            PackageInfo info = pContext.getPackageManager().getPackageInfo(pContext.getPackageName(), PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                hsahkey = new String(Base64.encode(md.digest(), 0));
                Log.i("SHUBH", "printHashKey() Hash Key: " + hsahkey);

            }
        } catch (NoSuchAlgorithmException e) {
            Log.e("SHUBH", "printHashKey()", e);
        } catch (Exception e) {
            Log.e("SHUBH", "printHashKey()", e);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        mcallbackManager.onActivityResult(requestCode,resultCode,data);
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==1) {
            Task<GoogleSignInAccount> task= GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);

            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = GoogleSignIn.getLastSignedInAccount(getApplicationContext());
            if (acct != null) {
                String personName = acct.getDisplayName();
                String personGivenName = acct.getGivenName();
                String personFamilyName = acct.getFamilyName();
                String personEmail = acct.getEmail();
                String personId = acct.getId();
                Uri personPhoto = acct.getPhotoUrl();

                socialLoginAPi(personEmail,personName,personId,"google");
            }
        } catch (ApiException e) {
            e.printStackTrace();
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Toast.makeText(this, "Error occured", Toast.LENGTH_SHORT).show();
        }
    }

    private void handleFacebookToken(AccessToken token) {
        Log.d("tag", "handleFacebookAccessToken:" + token);

        AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());

        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, UI will update with the signed-in user's information
                            Log.d("tag", "signInWithCredential:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            String email=user.getEmail();
                            String name=user.getDisplayName();
                            String fbid=user.getUid();
                            socialLoginAPi(email,name,fbid,"facebook");
                        } else {
                            // If sign-in fails, a message will display to the user.
                            Log.w("tag", "signInWithCredential:failure", task.getException());
                            Toast.makeText(MobileLogin.this, "Authentication failed.", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }


    private void callLoginApi() {
        dialog=new TransparentProgressDialog(mContext);
        dialog.show();

        String myMobile = ccp.getSelectedCountryCodeWithPlus()+""+et_mobile.getText().toString().trim().replace(" ", "");
        LoginRequest loginRequest = new LoginRequest();
        loginRequest.setMobileNumber(myMobile);
        loginRequest.setPassword(et_password.getText().toString().trim());

        APIInterface apiInterface = ApiClient.getClient().create(APIInterface.class);
        Call<LoginResponse> resultCall = apiInterface.CallLogin(loginRequest);
        resultCall.enqueue(new Callback<LoginResponse>() {


            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                dialog.dismiss();
                if (response.body().getStatus().equals(true)) {

                    Headers headers = response.headers();
                   // Toast.makeText(mContext, ""+headers.get("Auth-Token"), Toast.LENGTH_SHORT).show();

                    Prefrence.save(mContext, Prefrence.KEY_TOKEN, headers.get("Auth-Token"));
                    Prefrence.save(mContext, Prefrence.KEY_USER_ID, response.body().getParam().getId());
                    Prefrence.save(mContext, Prefrence.KEY_MOBILE_NO, response.body().getParam().getMobileNumber());
                    Prefrence.save(mContext, Prefrence.KEY_EMAIL_ID, response.body().getParam().getEmail());
                    Prefrence.save(mContext, Prefrence.KEY_FIRST_NAME, response.body().getParam().getFirstname());
                    Prefrence.save(mContext, Prefrence.KEY_USERTYPE, response.body().getParam().getUserType());

                    if(Prefrence.get(mContext,Prefrence.KEY_LATITUDE).equalsIgnoreCase("") ||
                            Prefrence.get(mContext,Prefrence.KEY_LATITUDE).equalsIgnoreCase("0.0")){
                        Intent intent = new Intent(mContext, NiceToMeetYou.class);
                        startActivity(intent);
                        finish();
                    }else{
                        Intent intent = new Intent(mContext, HomePageActivity.class);
                        startActivity(intent);
                        finish();
                    }

                } else {
                    Toast.makeText(mContext, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                dialog.dismiss();
                Toast.makeText(mContext, getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
            }
        });

    }


    private void callSignUpApi() {
        dialog=new TransparentProgressDialog(mContext);
        dialog.show();

        String myMobile = signup_ccp.getSelectedCountryCodeWithPlus()+""+et_signup_mobile.getText().toString().trim().replace(" ", "");

        SignupRequest request = new SignupRequest();
        request.setEmail(et_email.getText().toString());
        request.setMobileNumber(myMobile);
        request.setPassword(et_signup_password.getText().toString());
        request.setName(et_name.getText().toString());

        APIInterface apiInterface = ApiClient.getClient().create(APIInterface.class);
        Call<SignupResponse> resultCall = apiInterface.CallSignup(request);
        resultCall.enqueue(new Callback<SignupResponse>() {


            @Override
            public void onResponse(Call<SignupResponse> call, Response<SignupResponse> response) {
                dialog.dismiss();
                if (response.body().getStatus().equals(true)) {
                    Toast.makeText(mContext,  "Your OTP - "+response.body().getParam().getOtp(), Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(mContext, OtpActivity.class);
                    intent.putExtra("otp",response.body().getParam().getOtp());
                    intent.putExtra("id",response.body().getParam().getId());
                    intent.putExtra("mymobile",myMobile);
                    startActivity(intent);
                    finish();
                } else {
                    Toast.makeText(mContext, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<SignupResponse> call, Throwable t) {
                dialog.dismiss();
                Toast.makeText(mContext, getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
            }
        });

    }

    public void socialLoginAPi(String email,String name,String fbid,String type) {
        pd_loading.setVisibility(View.VISIBLE);

        String android_id = android.provider.Settings.Secure.getString(getContentResolver(),
                Settings.Secure.ANDROID_ID);

        SocialLoginRequest mLoginRequest = new SocialLoginRequest();
        mLoginRequest.setEmail(email);
        mLoginRequest.setName(name);
        mLoginRequest.setSocial_id(fbid);
        mLoginRequest.setUser_type("personal");


        APIInterface apiInterface = ApiClient.getClient().create(APIInterface.class);
        Call<LoginResponse> resultCall = apiInterface.CallSocialLogin(mLoginRequest);

        resultCall.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                pd_loading.setVisibility(View.GONE);
                if (response != null && response.isSuccessful()) {
                    if (response.body().getStatus().equals(true)) {

                        Headers headers = response.headers();
                        Prefrence.save(mContext, Prefrence.KEY_TOKEN, headers.get("Auth-Token"));
                        Prefrence.save(mContext, Prefrence.KEY_USER_ID, response.body().getParam().getId());
                        Prefrence.save(mContext, Prefrence.KEY_MOBILE_NO, response.body().getParam().getMobileNumber());
                        Prefrence.save(mContext, Prefrence.KEY_EMAIL_ID, response.body().getParam().getEmail());
                        Prefrence.save(mContext, Prefrence.KEY_FIRST_NAME, response.body().getParam().getFirstname());
                        Prefrence.save(mContext, Prefrence.KEY_USERTYPE, response.body().getParam().getUserType());
                        Prefrence.saveBool(mContext, Prefrence.KEY_NOTIFICATION, response.body().getParam().getPushNotification());

                        if(response.body().getParam().getMobileNumber() != null && !response.body().getParam().getMobileNumber().equalsIgnoreCase("")){
                            if(Prefrence.get(mContext,Prefrence.KEY_LATITUDE).equalsIgnoreCase("") ||
                                    Prefrence.get(mContext,Prefrence.KEY_LATITUDE).equalsIgnoreCase("0.0")){
                                Intent intent = new Intent(mContext, NiceToMeetYou.class);
                                startActivity(intent);
                                finish();
                            }else{
                                Intent intent = new Intent(mContext, HomePageActivity.class);
                                startActivity(intent);
                                finish();
                            }
                        }else{
                            showMobileDialog();
                       }
                    } else {
                        Toast.makeText(mContext, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }else{
                    if (response != null  && response.errorBody() != null) {
                        try {
                            JSONObject jObjError = new JSONObject(response.errorBody().string());
                            Toast.makeText(mContext, jObjError.getString("message"), Toast.LENGTH_LONG).show();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        return;
                    }
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                Toast.makeText(mContext, getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
                pd_loading.setVisibility(View.GONE);
            }
        });
    }

    public void showMobileDialog() {
        final Dialog dialogBuilder = new Dialog(mContext,R.style.Theme_Dialog);
        dialogBuilder.requestWindowFeature(Window.FEATURE_NO_TITLE); //before

        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_enter_mobile, null);
        dialogBuilder.setContentView(dialogView);

        EditText et_mobile = dialogView.findViewById(R.id.et_mobile);
        CountryCodePicker dialogccp = dialogView.findViewById(R.id.ccp);
        Button btnSubmit = dialogView.findViewById(R.id.btnSUbmit);
        TextView btnCancel = dialogView.findViewById(R.id.btnCancel);
        ProgressBar pd_loading = dialogView.findViewById(R.id.pd_loading);

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(et_mobile.getText().toString().trim().equalsIgnoreCase("")){
                    Toast.makeText(mContext, mContext.getResources().getString(R.string.enter_mobile), Toast.LENGTH_SHORT).show();
                }else{
                    String myMobile = dialogccp.getSelectedCountryCodeWithPlus()+""+et_mobile.getText().toString().trim();
                    EditProfileRequest request = new EditProfileRequest();
                    request.setId(Prefrence.get(mContext, Prefrence.KEY_USER_ID));
                    request.setMobile_number(myMobile);
                    callUpdateMobileNumber(request,pd_loading,btnSubmit,dialogBuilder);
                }
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogBuilder.dismiss();
            }
        });


        dialogBuilder.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
            }
        });
        dialogBuilder.show();
    }


    private void callUpdateMobileNumber(EditProfileRequest request,ProgressBar pd_loading,Button btnSubmit,Dialog dialogBuilder) {
        pd_loading.setVisibility(View.VISIBLE);
        btnSubmit.setVisibility(View.GONE);
        APIInterface apiInterface = ApiClient.getClient().create(APIInterface.class);
        Call<SignupResponse> resultCall = apiInterface.CallUpdateUser("Bearer " + Prefrence.get(mContext,Prefrence.KEY_TOKEN),request);
        resultCall.enqueue(new Callback<SignupResponse>() {

            @Override
            public void onResponse(Call<SignupResponse> call, Response<SignupResponse> response) {
                pd_loading.setVisibility(View.GONE);
                btnSubmit.setVisibility(View.VISIBLE);
                if (response.body().getStatus().equals(true)){
                    dialogBuilder.dismiss();
                    Prefrence.save(mContext,Prefrence.KEY_MOBILE_NO,request.getMobile_number());
                    Toast.makeText(mContext, "Your mobile number has been updated", Toast.LENGTH_SHORT).show();

                    if(Prefrence.get(mContext,Prefrence.KEY_LATITUDE).equalsIgnoreCase("") ||
                            Prefrence.get(mContext,Prefrence.KEY_LATITUDE).equalsIgnoreCase("0.0")){
                        Intent intent = new Intent(mContext, NiceToMeetYou.class);
                        startActivity(intent);
                        finish();
                    }else{
                        Intent intent = new Intent(mContext, HomePageActivity.class);
                        startActivity(intent);
                        finish();
                    }

                } else {
                    Toast.makeText(mContext, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<SignupResponse> call, Throwable t) {
                Toast.makeText(mContext, getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
            }
        });

    }

}