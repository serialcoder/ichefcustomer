package com.ichef.android.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebStorage;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.ichef.android.MainActivity;
import com.ichef.android.R;
import com.ichef.android.adapter.SlidingImage_Adapter;
import com.ichef.android.requestmodel.payment.PaymentItem;
import com.ichef.android.responsemodel.Address.GetAddress.GetAddressesResponse;
import com.ichef.android.responsemodel.banner.BannerListModel;
import com.ichef.android.responsemodel.payment.PaymentResponseModel;
import com.ichef.android.responsemodel.verify.VerifyResponseModel;
import com.ichef.android.retrofit.APIInterface;
import com.ichef.android.retrofit.ApiClient;
import com.ichef.android.utils.Prefrence;
import com.ichef.android.utils.TransparentProgressDialog;
import com.ichef.android.utils.VerifyProgressDialog;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.ButterKnife;
import butterknife.OnTextChanged;
import co.paystack.android.Paystack;
import co.paystack.android.PaystackSdk;
import co.paystack.android.Transaction;
import co.paystack.android.exceptions.ExpiredAccessCodeException;
import co.paystack.android.model.Card;
import co.paystack.android.model.Charge;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PaymentWebView extends AppCompatActivity {


    private WebView webView;
    String myURL = "";
    Context mContext= this;
    ImageView imgBack;
    ProgressBar pd_loading;
    public static final String USER_AGENT = "Mozilla/5.0 (Linux; Android 4.1.1; Galaxy Nexus Build/JRO03C) AppleWebKit/535.19 (KHTML, like Gecko) Chrome/18.0.1025.166 Mobile Safari/535.19";
    String myEmail = "";
    Double mTotalAmount = 0.0;
    String vendorID = "";
    Boolean isWebviewLoaded =false;
    String tid = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_webview);
        ImageView back = findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        mTotalAmount = getIntent().getDoubleExtra("totalAmount", 0.0);
        vendorID = getIntent().getStringExtra("vendorID");

        pd_loading =  findViewById(R.id.pd_loading);
        webView = (WebView) findViewById(R.id.webView1);

        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        webView.getSettings().setUserAgentString(USER_AGENT);

        CookieManager.getInstance().setAcceptCookie(true);
        getAuthURL();
        startWebView(myURL);
    }


    private void getAuthURL() {
        String token = "Bearer " + Prefrence.get(this, Prefrence.KEY_TOKEN);
        APIInterface apiInterface = ApiClient.getClient().create(APIInterface.class);
        PaymentItem model = new PaymentItem();
        model.setAmount(String.valueOf(mTotalAmount));
        model.setBusiness_id(vendorID);
        model.setUser_id(Prefrence.get(mContext,Prefrence.KEY_USER_ID));
        Call<PaymentResponseModel> resultCall = apiInterface.CallPaymentInitiate(token,model);
        //Call<HomePageListModel> resultCall = apiInterface.CallProductList(token,"7.448166400000001","9.0570752");

        resultCall.enqueue(new Callback<PaymentResponseModel>() {
            @Override
            public void onResponse(Call<PaymentResponseModel> call, Response<PaymentResponseModel> response) {
                if (response.body().getStatus()) {
                    tid = response.body().getParam().getResponse().getId();
                    startWebView(response.body().getParam().getResponse().getAuthorizationUrl());
                } else {
                    Toast.makeText(mContext, "Payment not initiate!", Toast.LENGTH_SHORT).show();
                    finish();
                }
            }

            @Override
            public void onFailure(Call<PaymentResponseModel> call, Throwable t) {
                Toast.makeText(mContext, "Payment not initiate!", Toast.LENGTH_SHORT).show();
                finish();
                call.cancel();
            }
        });
    }


    private void startWebView(String url) {

        webView.setWebViewClient(new WebViewClient() {
            //If you will not use this method url links are opeen in new brower not in webview
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                if(url.contains("trxref=")){
                    Uri mUri = Uri.parse(webView.getUrl());
                    Set<String> paramNames = mUri.getQueryParameterNames();
                    int sno = 0;
                    String trxref = "";
                    for (String key: paramNames) {
                        trxref = mUri.getQueryParameter(key);
                        //Toast.makeText(PaymentWebView.this, ""+trxref, Toast.LENGTH_SHORT).show();
                        verifyPayment(trxref);
                        break;
                    }
                }
                return true;
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                pd_loading.setVisibility(View.VISIBLE);
            }

            public void onPageFinished(WebView view, String url) {
                if(url.contains("about:blank") && isWebviewLoaded){
                    isWebviewLoaded = false;
                    Toast.makeText(PaymentWebView.this, "You have cancelled the payment!", Toast.LENGTH_SHORT).show();
                    verifyPayment("");
                }else if(url.contains("checkout")){
                    isWebviewLoaded = true;
                }
                pd_loading.setVisibility(View.GONE);
            }
        });


        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.loadUrl(url);
    }

    // Open previous opened link from history on webview when back button pressed

    @Override
    // Detect when the back button is pressed
    public void onBackPressed() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(PaymentWebView.this);
        alertDialog.setTitle(R.string.cancel_trans);
        alertDialog.setMessage(R.string.cancel_transaction_content);
        alertDialog.setIcon(R.drawable.payementgrey);
        alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });

        // Setting Negative "NO" Button
        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to invoke NO event
                dialog.dismiss();
                dialog.cancel();
            }
        });
        // Showing Alert Message
        alertDialog.show();
    }

    private void verifyPayment(String reference) {
        VerifyProgressDialog dialog = new VerifyProgressDialog(this);
        dialog.show();

        String token = "Bearer " + Prefrence.get(this, Prefrence.KEY_TOKEN);
        APIInterface apiInterface = ApiClient.getClient().create(APIInterface.class);
        Call<VerifyResponseModel> call = apiInterface.VarifyPayment(token, tid);
        call.enqueue(new Callback<VerifyResponseModel>() {
            @Override
            public void onResponse(Call<VerifyResponseModel> call, Response<VerifyResponseModel> response) {
                dialog.dismiss();
                if (!reference.equalsIgnoreCase("") && response != null &&  response.body() != null && response.body().getStatus() != null &&  response.body().getStatus()) {
                    Intent returnIntent = new Intent();
                    returnIntent.putExtra("transactionId", reference);
                    setResult(Activity.RESULT_OK, returnIntent);
                    finish();
                } else {
                    Intent returnIntent = new Intent();
                    returnIntent.putExtra("message", response.body().getParam().getGatewayResponse());
                    setResult(Activity.RESULT_OK, returnIntent);
                    setResult(Activity.RESULT_CANCELED, returnIntent);
                    finish();
                }
            }

            @Override
            public void onFailure(Call<VerifyResponseModel> call, Throwable t) {
                dialog.dismiss();
                Intent returnIntent = new Intent();
                returnIntent.putExtra("message", "cancel");
                setResult(Activity.RESULT_CANCELED, returnIntent);
                finish();
                call.cancel();
            }
        });
    }


}