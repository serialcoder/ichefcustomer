package com.ichef.android.activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.ichef.android.R;
import com.ichef.android.utils.Prefrence;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import pub.devrel.easypermissions.EasyPermissions;

public class NiceToMeetYou extends AppCompatActivity implements EasyPermissions.PermissionCallbacks {
    TextView llcontinue;
    TextView locationtxt;
    protected LocationManager locationManager;
    protected Context mContext = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nice_to_meet_you);
        ImageView back = findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        init();
        onclick();


    }

    private void onclick() {
        llcontinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(AnimationUtils.loadAnimation(NiceToMeetYou.this, R.anim.image_click));
                Intent intent = new Intent(NiceToMeetYou.this, LocationManually.class);
                intent.putExtra("isFromHome", false);
                startActivity(intent);

            }
        });
        locationtxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(AnimationUtils.loadAnimation(NiceToMeetYou.this, R.anim.image_click));
                if (Prefrence.get(mContext, Prefrence.KEY_LATITUDE).equalsIgnoreCase("") ||
                        Prefrence.get(mContext, Prefrence.KEY_LATITUDE).equalsIgnoreCase("0.0")) {
                    fetchCurrentLatLong();
                } else {
                    Intent intent = new Intent(mContext, HomePageActivity.class);
                    startActivity(intent);
                }
            }
        });
    }

    private void init() {
        llcontinue = findViewById(R.id.llmanuallocation);
        locationtxt = findViewById(R.id.currentlocation);
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        checkPermissions();
    }

    private void checkPermissions() {
        String perm[] = {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE};
        if (EasyPermissions.hasPermissions(mContext, perm)) {
            fetchCurrentLatLong();
        } else {
            EasyPermissions.requestPermissions(this, "You need to allow these permission to use this app",
                    102, perm);
        }
    }


    public void fetchCurrentLatLong() {
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            displayLocationSettingsRequest(mContext);
        }

        if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            // execute when Gps_provider is enable
            if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {

                //handleLocation(locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER));
                // set requestLocationUpdates with adding time to refresh your location status and distance

                /*locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, new LocationListener() {
                    @Override
                    public void onLocationChanged(@NonNull Location location) {
                        handleLocation(location);
                    }
                });*/

                locationManager.requestSingleUpdate(LocationManager.GPS_PROVIDER,new LocationListener() {
                    public void onLocationChanged(Location location) {
                        Log.e("location",location.getLatitude()+"");
                        handleLocation(location);
                    }

                    public void onStatusChanged(String provider, int status, Bundle extras) {}

                    public void onProviderEnabled(String provider) {}

                    public void onProviderDisabled(String provider) {}
                }
                ,null);
            }
            if (locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {  // execute when network_provider is enable
                    // set requestLocationUpdates with adding time to refresh your location status and distance
                    // handleLocation(locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER));
                    /*locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, new LocationListener() {
                        @Override
                        public void onLocationChanged(@NonNull Location location) {
                            handleLocation(location);
                        }
                    });*/

                locationManager.requestSingleUpdate(LocationManager.NETWORK_PROVIDER,new LocationListener() {
                            public void onLocationChanged(Location location) {
                                Log.e("location",location.getLatitude()+"");
                                handleLocation(location);
                            }

                            public void onStatusChanged(String provider, int status, Bundle extras) {}

                            public void onProviderEnabled(String provider) {}

                            public void onProviderDisabled(String provider) {}
                        }
                        ,null);
            }
            else {
                Toast.makeText(mContext, "Please enable GPS for better result", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void handleLocation(Location location) {
        if (location != null) {
            Prefrence.save(mContext, Prefrence.KEY_LATITUDE, String.valueOf(location.getLatitude()));
            Prefrence.save(mContext, Prefrence.KEY_LONGITUDE, String.valueOf(location.getLongitude()));
            List<Address> addresses = null;
            try {
                Geocoder geocoder = new Geocoder(NiceToMeetYou.this, Locale.getDefault());
                // addresses = geocoder.getFromLocation(23.2601154, 77.4203411, 1);
                addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (addresses != null && addresses.size() > 0) {
                Address address = addresses.get(0);
                StringBuilder sb = new StringBuilder();
                String address1 = address.getAddressLine(0);
                String address2 = address.getAddressLine(1);
                String city = address.getLocality();
                String state = address.getAdminArea();
                String country = address.getCountryName();
                String postalCode = address.getPostalCode();
                String fullAddress = sb.append(address1).append(" ").toString();
                     /*   .append(address2).append(", ")
                        .append(city).append(", ")
                        .append(state).append(", ")
                        .append(country).append(", ")
                        .append(postalCode).append(", ")
                        .toString();*/
                Prefrence.save(NiceToMeetYou.this, Prefrence.CITYNAME, fullAddress);
                Intent intent = new Intent(NiceToMeetYou.this, HomePageActivity.class);
                startActivity(intent);
                finish();
            } else {
                Toast.makeText(mContext, "Unable to fetch your location", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void displayLocationSettingsRequest(Context context) {
        GoogleApiClient googleApiClient = new GoogleApiClient.Builder(context)
                .addApi(LocationServices.API).build();
        googleApiClient.connect();

        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(10000 / 2);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);
        builder.setAlwaysShow(true);

        PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        Log.i("SHUBHAM", "All location settings are satisfied.");
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        Log.i("SHUBHAM", "Location settings are not satisfied. Show the user a dialog to upgrade location settings ");
                        try {
                            // Show the dialog by calling startResolutionForResult(), and check the result
                            // in onActivityResult().
                            status.startResolutionForResult(NiceToMeetYou.this, 101);
                        } catch (IntentSender.SendIntentException e) {
                            Log.i("SHUBHAM", "PendingIntent unable to execute request.");
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        Log.i("SHUBHAM", "Location settings are inadequate, and cannot be fixed here. Dialog not created.");
                        break;
                }
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            fetchCurrentLatLong();
        } else {
            Toast.makeText(mContext, "Please enable gps location for better result", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {
        // fetchCurrentLatLong();
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {

    }


}
