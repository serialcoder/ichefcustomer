package com.ichef.android.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;
import androidx.viewpager.widget.ViewPager;

import com.ichef.android.R;
import com.ichef.android.adapter.SlidingImage_Adapter;
import com.ichef.android.requestmodel.user.DeleteUserRequest;
import com.ichef.android.requestmodel.user.UpdateNotificationRequest;
import com.ichef.android.responsemodel.Address.AddAddressResponse;
import com.ichef.android.responsemodel.banner.BannerListModel;
import com.ichef.android.responsemodel.banner.Result;
import com.ichef.android.retrofit.APIInterface;
import com.ichef.android.retrofit.ApiClient;
import com.ichef.android.utils.Prefrence;
import com.ichef.android.utils.TransparentProgressDialog;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Settings extends AppCompatActivity {

    Context mContext = this;
    private ViewPager mPager;
    List<Result> mBannerListData = new ArrayList<>();
    private  int currentPage = 0;
    private  int NUM_PAGES = 0;
    TextView tvVersion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        mPager = findViewById(R.id.pager);
        SwitchCompat switchNotification = findViewById(R.id.switchNotification);
        switchNotification.setChecked(Prefrence.getBool(this, Prefrence.KEY_NOTIFICATION));
        switchNotification.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                updateNotificationSetting(isChecked);
            }
        });
        tvVersion = findViewById(R.id.tvVersion);
        try{
            String versionName = getPackageManager()
                    .getPackageInfo(getPackageName(), 0).versionName;
            tvVersion.setText(versionName);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }


        ImageView back = findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        getBannerList();
    }

    private void updateNotificationSetting(boolean isChecked) {
        TransparentProgressDialog dialog = new TransparentProgressDialog(this);
        dialog.show();
        String token = Prefrence.get(this, Prefrence.KEY_TOKEN);
        UpdateNotificationRequest request = new UpdateNotificationRequest();
        request.setPushNotification(isChecked);
        APIInterface apiInterface = ApiClient.getClient().create(APIInterface.class);
        Call<AddAddressResponse> resultCall = apiInterface.CallUpdateNotification("Bearer " + token, request);
        resultCall.enqueue(new Callback<AddAddressResponse>() {

            @Override
            public void onResponse(Call<AddAddressResponse> call, retrofit2.Response<AddAddressResponse> response) {
                if (response.body().getStatus()) {
                    Prefrence.saveBool(Settings.this, Prefrence.KEY_NOTIFICATION, isChecked);
                    Toast.makeText(Settings.this, "Notification Setting Updated", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(Settings.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }
                dialog.dismiss();
            }

            @Override
            public void onFailure(Call<AddAddressResponse> call, Throwable t) {
                dialog.dismiss();
                Toast.makeText(Settings.this, "Please check your Internet Connection", Toast.LENGTH_SHORT).show();
            }
        });

    }

    public void EditProfile(View view) {
        view.startAnimation(AnimationUtils.loadAnimation(this, R.anim.image_click));
        startActivity(new Intent(this, EditProfileActivity.class));
    }

    public void UpdateProfilePic(View view) {
        view.startAnimation(AnimationUtils.loadAnimation(this, R.anim.image_click));
        startActivity(new Intent(this, UpdateProfilePicActivity.class));
    }

    public void DeleteUser(View view) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(Settings.this);
        alertDialog.setMessage("Are you sure, you want to delete account?");
        alertDialog.setIcon(android.R.drawable.ic_menu_close_clear_cancel);
        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                deleteUserApiCall();
            }
        });

        // Setting Negative "NO" Button
        alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to invoke NO event
                dialog.dismiss();
                dialog.cancel();
            }
        });
        // Showing Alert Message
        alertDialog.show();
    }

    private void deleteUserApiCall() {
        TransparentProgressDialog dialog = new TransparentProgressDialog(this);
        dialog.show();
        String token = Prefrence.get(this, Prefrence.KEY_TOKEN);
        DeleteUserRequest request = new DeleteUserRequest();
        request.setEmail(Prefrence.get(this, Prefrence.KEY_EMAIL_ID));
        request.setId(Prefrence.get(this, Prefrence.KEY_USER_ID));
        request.setMobileNumber(Prefrence.get(this, Prefrence.KEY_MOBILE_NO));

        APIInterface apiInterface = ApiClient.getClient().create(APIInterface.class);
        Call<AddAddressResponse> resultCall = apiInterface.CallDeleteUSer("Bearer " + token, request);
        resultCall.enqueue(new Callback<AddAddressResponse>() {

            @Override
            public void onResponse(Call<AddAddressResponse> call, retrofit2.Response<AddAddressResponse> response) {
                if (response.body().getStatus()) {
                    Toast.makeText(Settings.this, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    Prefrence.save(getApplication(), Prefrence.KEY_USER_ID, "");
                    Prefrence.save(getApplication(), Prefrence.KEY_FIRST_NAME, "");
                    Prefrence.save(getApplication(), Prefrence.KEY_EMAIL_ID, "");
                    Prefrence.save(getApplication(), Prefrence.KEY_MOBILE_NO, "");
                    Prefrence.save(getApplication(), Prefrence.KEY_USERTYPE, "");
                    Prefrence.save(getApplication(), Prefrence.KEY_USER_ID, "");
                    Prefrence.clearPreference(Settings.this);
                    Intent in = new Intent(Settings.this, MobileLogin.class);
                    startActivity(in);
                    finishAffinity();
                } else {
                    Toast.makeText(Settings.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }
                dialog.dismiss();
            }

            @Override
            public void onFailure(Call<AddAddressResponse> call, Throwable t) {
                dialog.dismiss();
                Toast.makeText(Settings.this, "Please check your Internet Connection", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getBannerList() {

        APIInterface apiInterface = ApiClient.getClient().create(APIInterface.class);
        Call<BannerListModel> resultCall = apiInterface.CallBannerList();
        //Call<HomePageListModel> resultCall = apiInterface.CallProductList(token,"7.448166400000001","9.0570752");

        resultCall.enqueue(new Callback<BannerListModel>() {
            @Override
            public void onResponse(Call<BannerListModel> call, Response<BannerListModel> response) {
                if (response.body().getStatus()) {
                    mBannerListData = response.body().getResult();
                    if (mBannerListData.size() == 0) {
                        mPager.setVisibility(View.GONE);
                    } else {
                        mPager.setVisibility(View.VISIBLE);
                        ArrayList<String> image_list = new ArrayList<>();
                        if(mBannerListData != null && mBannerListData.size()>0){
                            for (int i=0;i<mBannerListData.size();i++){
                                image_list.add(mBannerListData.get(i).getImage());
                            }
                            mPager.setAdapter(new SlidingImage_Adapter(mContext, image_list));
                            final float density = getResources().getDisplayMetrics().density;
                            NUM_PAGES = image_list.size();

                            // Auto start of viewpager
                            if (image_list.size() > 0) {
                                final Handler handler = new Handler();
                                final Runnable Update = new Runnable() {
                                    public void run() {
                                        if (currentPage == NUM_PAGES) {
                                            currentPage = 0;
                                        }
                                        mPager.setCurrentItem(currentPage++, true);
                                    }
                                };

                                Timer swipeTimer = new Timer();
                                swipeTimer.schedule(new TimerTask() {
                                    @Override
                                    public void run() {
                                        handler.post(Update);
                                    }
                                }, 5000, 4000);
                            }
                        }
                    }
                } else {
                    mPager.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<BannerListModel> call, Throwable t) {
                mPager.setVisibility(View.GONE);
                call.cancel();
            }
        });
    }


}