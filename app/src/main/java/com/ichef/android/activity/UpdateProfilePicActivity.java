package com.ichef.android.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;

import com.cloudinary.android.MediaManager;
import com.cloudinary.android.callback.ErrorInfo;
import com.cloudinary.android.callback.UploadCallback;
import com.ichef.android.R;
import com.ichef.android.requestmodel.user.EditProfileRequest;
import com.ichef.android.requestmodel.user.SignupRequest;
import com.ichef.android.responsemodel.signup.SignupResponse;
import com.ichef.android.retrofit.APIInterface;
import com.ichef.android.retrofit.ApiClient;
import com.ichef.android.utils.CommonUtility;
import com.ichef.android.utils.FileCompressor;
import com.ichef.android.utils.Prefrence;


import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UpdateProfilePicActivity extends AppCompatActivity {
    Context context = this;

    ImageView upload_file_pic;
    private int REQUEST_CAMERA = 0, SELECT_FILE = 1;
    private static final int PICK_IMAGE_REQUEST = 1;
    private Bitmap bitmap14;
    private String cloudImage;
    LinearLayout btnGoLive;
    ProgressBar pd_loading;
    String userChoosenTask = "";
    public  File mPhotoFile;
    Context mContext = this;
    public String mCameraImgPath = null;
    FileCompressor mCompressor;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_profile_pic);
        ImageView back = findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        init();
        onlclick();

    }

    private void init() {
        pd_loading = findViewById(R.id.pd_loading);
        btnGoLive = findViewById(R.id.btnGoLive);
        upload_file_pic = findViewById(R.id.uploadProfilePic);
        mCompressor = new FileCompressor(this);

    }

    private void onlclick() {
        upload_file_pic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage();
            }
        });

        btnGoLive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                view.startAnimation(AnimationUtils.loadAnimation(UpdateProfilePicActivity.this, R.anim.image_click));
                if (bitmap14 != null && !cloudImage.equalsIgnoreCase("")) {
                    EditProfileRequest request = new EditProfileRequest();
                    request.setId(Prefrence.get(UpdateProfilePicActivity.this, Prefrence.KEY_USER_ID));
                    request.setDisplay(cloudImage);
                    updateProfilePic(request);
                } else {
                    Toast.makeText(context, getString(R.string.please_add_image), Toast.LENGTH_SHORT).show();
                }
            }
        });

    }


    private void updateProfilePic(EditProfileRequest request) {
        pd_loading.setVisibility(View.VISIBLE);
        btnGoLive.setVisibility(View.GONE);

        APIInterface apiInterface = ApiClient.getClient().create(APIInterface.class);
        Call<SignupResponse> resultCall = apiInterface.CallUpdateUser("Bearer " + Prefrence.get(context, Prefrence.KEY_TOKEN), request);
        resultCall.enqueue(new Callback<SignupResponse>() {

            @Override
            public void onResponse(Call<SignupResponse> call, Response<SignupResponse> response) {
                pd_loading.setVisibility(View.GONE);
                btnGoLive.setVisibility(View.VISIBLE);
                if (response.body().getStatus().equals(true)) {
                    Prefrence.save(context, Prefrence.PROFILE_PIC, cloudImage);
                    Toast.makeText(UpdateProfilePicActivity.this, "Your profile picture has been updated", Toast.LENGTH_SHORT).show();
                    finish();
                } else {
                    Toast.makeText(UpdateProfilePicActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<SignupResponse> call, Throwable t) {
                pd_loading.setVisibility(View.GONE);
                btnGoLive.setVisibility(View.VISIBLE);
                Toast.makeText(UpdateProfilePicActivity.this, getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
            }
        });

    }


    private void selectImage() {
        final CharSequence[] items = {"Take Photo", "Choose from Library",
                "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, (dialog, item) -> {
            boolean b = CommonUtility.checkAndRequestPermissions(this);
            if (items[item].equals("Take Photo")) {
                userChoosenTask = "Take Photo";
                if (b)
                    cameraIntent();
                else
                    CommonUtility.checkAndRequestPermissions(this);

            } else if (items[item].equals("Choose from Library")) {
                userChoosenTask = "Choose from Library";
                if (b)
                    showFileChooser();

            } else if (items[item].equals("Cancel")) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    private void showFileChooser() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            Uri picUri = data.getData();
            try {
                Uri uri = data.getData();
                if (uri != null) {
                    AlertDialog builder;
                    ProgressBar mDownloadProgressBar;
                    TextView mDownloadProgressTxt;
                    builder = new AlertDialog.Builder(UpdateProfilePicActivity.this).create();
                    builder.setCancelable(false);
                    LayoutInflater inflater = getLayoutInflater();
                    View dialogLayoutInflater = inflater.inflate(R.layout.activity_custom_progressbar, null);
                    mDownloadProgressBar = dialogLayoutInflater.findViewById(R.id.download_progressBar);
                    mDownloadProgressTxt = dialogLayoutInflater.findViewById(R.id.txtProgress);
                    builder.setView(dialogLayoutInflater);

                    String requestId = MediaManager.get().upload(uri).callback(new UploadCallback() {
                        @Override
                        public void onStart(String requestId) {
                            // your code here
                            builder.show();
                        }

                        @Override
                        public void onProgress(String requestId, long bytes, long totalBytes) {
                            // example code starts here
                            Double progress = (double) bytes * 100 / totalBytes;
                            // post progress to app UI (e.g. progress bar, notification)
                            // example code ends here
                            mDownloadProgressBar.setProgress((int) Math.round(progress));
                            mDownloadProgressTxt.setText(progress + "%");
                        }

                        @Override
                        public void onSuccess(String requestId, Map resultData) {
                            // your code here
                            if (builder != null && builder.isShowing()) {
                                builder.dismiss();
                            }
                            cloudImage = resultData.get("url").toString();

                        }

                        @Override
                        public void onError(String requestId, ErrorInfo error) {
                            // your code here
                            Toast.makeText(UpdateProfilePicActivity.this, "Something went wrong!", Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void onReschedule(String requestId, ErrorInfo error) {
                            // your code here
                        }
                    })
                            .dispatch();
                }
                bitmap14 = MediaStore.Images.Media.getBitmap(getContentResolver(), picUri);
                upload_file_pic.setImageBitmap(bitmap14);
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (requestCode == REQUEST_CAMERA) {

                if (resultCode == Activity.RESULT_OK) {
                    try {
                        if(mCameraImgPath != null){
                            try {
                                mPhotoFile = mCompressor.compressToFile(mPhotoFile);

                                String filePath = mPhotoFile.getPath();
                                bitmap14 = BitmapFactory.decodeFile(filePath);
                                upload_file_pic.setImageBitmap(bitmap14);
                                try {
                                    Uri uri = Uri.fromFile(mPhotoFile);
                                    if (uri != null) {
                                        AlertDialog builder;
                                        ProgressBar mDownloadProgressBar;
                                        TextView mDownloadProgressTxt;
                                        builder = new AlertDialog.Builder(UpdateProfilePicActivity.this).create();
                                        builder.setCancelable(false);
                                        LayoutInflater inflater = getLayoutInflater();
                                        View dialogLayoutInflater = inflater.inflate(R.layout.activity_custom_progressbar, null);
                                        mDownloadProgressBar = dialogLayoutInflater.findViewById(R.id.download_progressBar);
                                        mDownloadProgressTxt = dialogLayoutInflater.findViewById(R.id.txtProgress);
                                        builder.setView(dialogLayoutInflater);

                                        String requestId = MediaManager.get().upload(uri).callback(new UploadCallback() {
                                            @Override
                                            public void onStart(String requestId) {
                                                // your code here
                                                builder.show();
                                            }

                                            @Override
                                            public void onProgress(String requestId, long bytes, long totalBytes) {
                                                // example code starts here
                                                Double progress = (double) bytes * 100 / totalBytes;
                                                // post progress to app UI (e.g. progress bar, notification)
                                                // example code ends here
                                                mDownloadProgressBar.setProgress((int) Math.round(progress));
                                                mDownloadProgressTxt.setText(progress + "%");
                                            }

                                            @Override
                                            public void onSuccess(String requestId, Map resultData) {
                                                // your code here
                                                if (builder != null && builder.isShowing()) {
                                                    builder.dismiss();
                                                }
                                                cloudImage = resultData.get("url").toString();

                                            }

                                            @Override
                                            public void onError(String requestId, ErrorInfo error) {
                                                // your code here
                                                Toast.makeText(UpdateProfilePicActivity.this, "Something went wrong!", Toast.LENGTH_SHORT).show();
                                            }

                                            @Override
                                            public void onReschedule(String requestId, ErrorInfo error) {
                                                // your code here
                                            }
                                        })
                                                .dispatch();
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }


                            } catch (IOException e) {
                                e.printStackTrace();
                                Toast.makeText(this, R.string.something_went_wrong, Toast.LENGTH_SHORT).show();
                            }
                        }else{
                            Toast.makeText(this, R.string.something_went_wrong, Toast.LENGTH_SHORT).show();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        Toast.makeText(this, R.string.something_went_wrong, Toast.LENGTH_SHORT).show();
                    }
                }
        }
    }

    private void cameraIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(mContext.getPackageManager()) != null) {
            // Create the File where the photo should go
            mPhotoFile = null;
            try {
                mPhotoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
                ex.printStackTrace();
            }
            // Continue only if the File was successfully created
            if (mPhotoFile != null) {
                mCameraImgPath = mPhotoFile.getAbsolutePath();
                Uri photoURI = FileProvider.getUriForFile(mContext,
                        "com.ichef.android.fileprovider",
                        mPhotoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_CAMERA);
            }
        }


    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = mContext.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        //currentPhotoPath = image.getAbsolutePath();
        return image;
    }

}