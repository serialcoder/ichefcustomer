package com.ichef.android.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ichef.android.R;
import com.ichef.android.adapter.NotificationProfileAdapter;
import com.ichef.android.responsemodel.notifications.GetNotificationResponse;
import com.ichef.android.responsemodel.notifications.Result;
import com.ichef.android.retrofit.APIInterface;
import com.ichef.android.retrofit.ApiClient;
import com.ichef.android.utils.Prefrence;
import com.ichef.android.utils.TransparentProgressDialog;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Notification extends AppCompatActivity {
    APIInterface apiInterface;
    String username;
    RecyclerView rv_MyProjectList;
    NotificationProfileAdapter rv_MyProjectAdapter;
    RecyclerView.LayoutManager rv_MyProjectLayoutManager;
    List<Result> mListData = new ArrayList<>();
    LinearLayout llNoNotification;
    TransparentProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        ImageView back = findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        init();
    }

    private void init() {
        rv_MyProjectList = findViewById(R.id.rvnotification);
        llNoNotification = findViewById(R.id.llNoNotification);
        rv_MyProjectList.setHasFixedSize(true);
        rv_MyProjectLayoutManager = new LinearLayoutManager(Notification.this);
        rv_MyProjectList.setLayoutManager(rv_MyProjectLayoutManager);
        getnotificationlist();
        dialog = new TransparentProgressDialog(Notification.this);
        dialog.show();
    }

    private void getnotificationlist() {
        String token = "Bearer " + Prefrence.get(this, Prefrence.KEY_TOKEN);
        apiInterface = ApiClient.getClient().create(APIInterface.class);
        Call<GetNotificationResponse> call = apiInterface.GetNotifications(token);
        call.enqueue(new Callback<GetNotificationResponse>() {
            @Override
            public void onResponse(Call<GetNotificationResponse> call, Response<GetNotificationResponse> response) {
                if (response.body().getStatus()) {
                    dialog.dismiss();
                    mListData = response.body().getResult();
                    if(mListData.size()==0){
                        llNoNotification.setVisibility(View.VISIBLE);
                    }else{
                        llNoNotification.setVisibility(View.GONE);
                        setProduct();
                    }
                } else {
                    dialog.dismiss();
                    llNoNotification.setVisibility(View.VISIBLE);
                    //Toast.makeText(Notification.this, "No record found", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<GetNotificationResponse> call, Throwable t) {
                llNoNotification.setVisibility(View.VISIBLE);
                dialog.dismiss();
                call.cancel();
            }
        });
    }

    private void setProduct() {
        if (mListData != null && mListData.size() > 0) {
            rv_MyProjectAdapter = new NotificationProfileAdapter(Notification.this, (ArrayList<Result>) mListData);
            rv_MyProjectList.setAdapter(rv_MyProjectAdapter);
        }
    }
}