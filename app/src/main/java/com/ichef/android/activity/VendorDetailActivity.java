package com.ichef.android.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.ichef.android.R;
import com.ichef.android.adapter.VendorDetailFragmentAdapter;
import com.ichef.android.responsemodel.vendordetail.GetDynamicVendorResponse;
import com.ichef.android.responsemodel.vendordetail.Menu;
import com.ichef.android.responsemodel.vendordetail.Vendor;
import com.ichef.android.retrofit.APIInterface;
import com.ichef.android.retrofit.ApiClient;
import com.ichef.android.utils.Prefrence;
import com.ichef.android.utils.TransparentProgressDialog;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VendorDetailActivity extends AppCompatActivity {

    private ViewPager viewPager;
    private TabLayout mTabLayout;
    String token, vendorId,VendorName;
    List<Menu> menuList = new ArrayList<>();
    List<Vendor> vendorList = new ArrayList<>();
    VendorDetailFragmentAdapter adapter;
    TransparentProgressDialog dialog;
    TextView vendorname, rating, minorder, deliveryfee;
    String s_vendorname, s_rating, s_minorder, s_deliveryfee;
    ImageView imgFood,back;
    LinearLayout llMenu,llNoMenu;
    ImageView img_star;
    Context mContext = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vendor_detail);
        back = findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        img_star = findViewById(R.id.img_star);
        imgFood = findViewById(R.id.imgFood);
        viewPager = findViewById(R.id.viewpagerdynamic);
        llMenu = findViewById(R.id.llMenu);
        llNoMenu = findViewById(R.id.llNoMenu);
        mTabLayout = findViewById(R.id.tabs);
        vendorId = getIntent().getStringExtra("VendorId");
        init();
        getTabName();
        dialog = new TransparentProgressDialog(VendorDetailActivity.this);
        dialog.show();
    }

    private void init() {
        vendorname = findViewById(R.id.tv_vendorname);
        rating = findViewById(R.id.tv_rating);
        minorder = findViewById(R.id.tv_minorder);
        deliveryfee = findViewById(R.id.tv_deliveryfee);

    }

    private void getTabName() {
        token = Prefrence.get(VendorDetailActivity.this, Prefrence.KEY_TOKEN);
        APIInterface apiInterface = ApiClient.getClient().create(APIInterface.class);
        Call<GetDynamicVendorResponse> call = apiInterface.GetVendorDetails("Bearer " + token, vendorId,Prefrence.get(mContext, Prefrence.KEY_LATITUDE), Prefrence.get(mContext, Prefrence.KEY_LONGITUDE));
        call.enqueue(new Callback<GetDynamicVendorResponse>() {
            @Override
            public void onResponse(Call<GetDynamicVendorResponse> call, Response<GetDynamicVendorResponse> response) {
                dialog.dismiss();
                if (!response.equals(null)) {
                    vendorList = response.body().getResult().getVendor();
                    for (int i = 0; i < vendorList.size(); i++) {
                        s_vendorname = vendorList.get(i).getBusinessName();

                        if(vendorList.get(i).getDisplayPicture() != null && ! vendorList.get(i).getDisplayPicture().equalsIgnoreCase("")){
                            Picasso.get().load(vendorList.get(i).getDisplayPicture())
                                    .placeholder(R.drawable.ic__placeholder_new)
                                    .into(imgFood);
                        }

                        vendorname.setText(s_vendorname);
                        if (vendorList.get(i).getAverageRating() != null && vendorList.get(i).getAverageRating() != 0.0) {
                            s_rating = String.format("%.2f", vendorList.get(i).getAverageRating());
                            rating.setText(s_rating);
                            img_star.setVisibility(View.VISIBLE);
                        }else{
                            rating.setText("NA");
                            img_star.setVisibility(View.GONE);
                        }
                    }
                    menuList = response.body().getResult().getMenu();
                    if (menuList != null && menuList.size()>0) {
                        llMenu.setVisibility(View.VISIBLE);
                        llNoMenu.setVisibility(View.GONE);
                        initViews();
                    } else {
                        llMenu.setVisibility(View.GONE);
                        llNoMenu.setVisibility(View.VISIBLE);
                        Toast.makeText(VendorDetailActivity.this, "No Menu Found", Toast.LENGTH_SHORT).show();
                    }
                    deliveryfee.setText(getString(R.string.nigiriacurrency)+""+response.body().getResult().getDeliveryAmount());
                } else {
                    Toast.makeText(VendorDetailActivity.this, "response " + response, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<GetDynamicVendorResponse> call, Throwable t) {
                dialog.dismiss();
                call.cancel();
            }
        });
    }

    private void setProduct() {
        if (menuList != null && menuList.size() > 0) {
            adapter = new VendorDetailFragmentAdapter(getSupportFragmentManager(), menuList.size(), menuList, this, vendorId);
            viewPager.setAdapter(adapter);
            viewPager.setCurrentItem(0);
        }

    }

    private void initViews() {

        // setOffscreenPageLimit means number
        // of tabs to be shown in one page
        viewPager.setOffscreenPageLimit(menuList.size());
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(mTabLayout));
        mTabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                // setCurrentItem as the tab position
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        setDynamicFragmentToTabLayout();
    }

    // show all the tab using DynamicFragmentAdapter
    private void setDynamicFragmentToTabLayout() {
        //  mTabLayout.addTab(mTabLayout.newTab().setText("null"));
        // you can give any number here
        for (int i = 0; i < menuList.size(); i++) {
            // set the tab name as "Page: " + i
            //  mTabLayout.addTab(mTabLayout.newTab().setText("TAB: " + i));
            String name = menuList.get(i).getCategoryname();
            if (!name.equals(null)) {
                mTabLayout.addTab(mTabLayout.newTab().setText(name));
            } else {
                mTabLayout.addTab(mTabLayout.newTab().setText("Tab"));
            }

        }
        setProduct();
    }

    public void showRatingDetail(View view) {
        startActivity(new Intent(this, ReviewList.class).putExtra("vendorId", vendorId));
    }
}
