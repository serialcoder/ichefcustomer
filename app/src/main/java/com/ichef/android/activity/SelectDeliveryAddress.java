package com.ichef.android.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ichef.android.R;
import com.ichef.android.adapter.SelectAddressAdapter;
import com.ichef.android.responsemodel.Address.GetAddress.Address;
import com.ichef.android.responsemodel.Address.GetAddress.GetAddressesResponse;
import com.ichef.android.retrofit.APIInterface;
import com.ichef.android.retrofit.ApiClient;
import com.ichef.android.utils.Prefrence;
import com.ichef.android.utils.TransparentProgressDialog;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SelectDeliveryAddress extends AppCompatActivity {
    APIInterface apiInterface;
    String username;
    RecyclerView rv_MyProjectList;
    SelectAddressAdapter rv_MyProjectAdapter;
    TransparentProgressDialog dialog;
    RecyclerView.LayoutManager rv_MyProjectLayoutManager;
    List<Address> mListData = new ArrayList<>();
    TextView addnew;
    TextView locationaddress,phoneno;
    RelativeLayout relCurrentLocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_address);
        ImageView back = findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        init();
        onclick();
    }

    private void onclick() {
        addnew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SelectDeliveryAddress.this, AddNewAddress.class);
                startActivity(intent);
            }
        });

        relCurrentLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Prefrence.save(SelectDeliveryAddress.this, Prefrence.KEY_DELIVERY_CITYNAME,Prefrence.get(SelectDeliveryAddress.this, Prefrence.CITYNAME));
                Prefrence.save(SelectDeliveryAddress.this, Prefrence.KEY_DELIVERY_MOBILE_NO,Prefrence.get(SelectDeliveryAddress.this, Prefrence.KEY_MOBILE_NO));
                Prefrence.save(SelectDeliveryAddress.this, Prefrence.KEY_DELIVERY_NAME,Prefrence.get(SelectDeliveryAddress.this, Prefrence.KEY_FIRST_NAME));
                finish();
            }
        });


    }

    private void init() {
        relCurrentLocation = findViewById(R.id.relCurrentLocation);
        locationaddress = findViewById(R.id.locationaddress);
        phoneno = findViewById(R.id.phoneno);
        addnew = findViewById(R.id.addnew);
        rv_MyProjectList = findViewById(R.id.rvaddress);
        rv_MyProjectList.setHasFixedSize(true);
        rv_MyProjectLayoutManager = new LinearLayoutManager(SelectDeliveryAddress.this);
        rv_MyProjectList.setLayoutManager(rv_MyProjectLayoutManager);
        locationaddress.setText(Prefrence.get(SelectDeliveryAddress.this, Prefrence.CITYNAME));
        phoneno.setText(Prefrence.get(SelectDeliveryAddress.this, Prefrence.KEY_MOBILE_NO));
    }

    @Override
    protected void onResume() {
        super.onResume();
        getlist();
        dialog = new TransparentProgressDialog(SelectDeliveryAddress.this);
        dialog.show();
    }

    private void getlist() {
        String token = "Bearer " + Prefrence.get(this, Prefrence.KEY_TOKEN);
        apiInterface = ApiClient.getClient().create(APIInterface.class);
        Call<GetAddressesResponse> call = apiInterface.GetAddresses(token);
        call.enqueue(new Callback<GetAddressesResponse>() {
            @Override
            public void onResponse(Call<GetAddressesResponse> call, Response<GetAddressesResponse> response) {
                if (response.body().getStatus()) {
                    dialog.dismiss();
                    mListData = response.body().getParam().getAddresses();
                    setProduct();
                } else {
                    dialog.dismiss();
                    Toast.makeText(SelectDeliveryAddress.this, "No record found", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<GetAddressesResponse> call, Throwable t) {

                dialog.dismiss();
                call.cancel();
            }
        });
    }

    private void setProduct() {
        if (mListData != null && mListData.size() > 0) {
            rv_MyProjectAdapter = new SelectAddressAdapter(SelectDeliveryAddress.this, (ArrayList<Address>) mListData);
            rv_MyProjectList.setAdapter(rv_MyProjectAdapter);
        }

    }
}