package com.ichef.android.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.snackbar.Snackbar;
import com.ichef.android.R;
import com.ichef.android.adapter.SlidingImage_Adapter;
import com.ichef.android.fragment.HomeFragment;
import com.ichef.android.responsemodel.banner.BannerListModel;
import com.ichef.android.responsemodel.cartmodel.FetchCartResponse;
import com.ichef.android.responsemodel.profile.ProfileModel;
import com.ichef.android.retrofit.APIInterface;
import com.ichef.android.retrofit.ApiClient;
import com.ichef.android.utils.CommonUtility;
import com.ichef.android.utils.Prefrence;
import com.squareup.picasso.Picasso;
import com.suddenh4x.ratingdialog.AppRating;
import com.suddenh4x.ratingdialog.preferences.RatingThreshold;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import eu.dkaratzas.android.inapp.update.Constants;
import eu.dkaratzas.android.inapp.update.InAppUpdateManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomePageActivity extends AppCompatActivity {
    ImageView nav_btn, profile;
    TextView locationtxt;
    protected Context context = this;
    HomeFragment fragment = null;
    private Snackbar snackbar;
    ImageView profileImage;
    RelativeLayout llCart;
    ImageView imgVendor, imgCancel;
    LinearLayout llVendroDetail;
    TextView tvVendorName, tvView;
    public static int REQ_CODE_VERSION_UPDATE = 1414;
    public static int widthDevice = 720;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_page2);

        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        widthDevice = dm.widthPixels;

        init();
        onclick();
        FetchCart();
        getProfile();
        InAppUpdateManager.Builder(this, REQ_CODE_VERSION_UPDATE)
                .resumeUpdates(true) // Resume the update, if the update was stalled. Default is true
                .mode(Constants.UpdateMode.IMMEDIATE)
                .checkForAppUpdate();

       new AppRating.Builder(this)
                .setMinimumLaunchTimes(2)
                .setMinimumDays(0)
                .setMinimumLaunchTimesToShowAgain(2)
                .setMinimumDaysToShowAgain(5)
                .setRatingThreshold(RatingThreshold.FOUR)
                .showIfMeetsConditions();

    }

    @Override
    protected void onStart() {
        super.onStart();

    }

    private void init() {
        llCart = findViewById(R.id.llCart);
        imgVendor = findViewById(R.id.imgVendor);
        imgCancel = findViewById(R.id.imgCancel);
        llVendroDetail = findViewById(R.id.llVendroDetail);
        tvVendorName = findViewById(R.id.tvVendorName);
        tvView = findViewById(R.id.tvView);

        imgCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                llCart.setVisibility(View.GONE);
            }
        });

        tvView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(HomePageActivity.this, MyCart.class));
            }
        });

        profileImage = (ImageView) findViewById(R.id.profileImage);
        nav_btn = (ImageView) findViewById(R.id.nav_btn);
        locationtxt = findViewById(R.id.locationtxt);

        fragment = new HomeFragment();

        if (fragment != null) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            //ft.addToBackStack(null);
            ft.replace(R.id.frame, fragment);
            ft.commit();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        getLocation();
        String city = Prefrence.get(HomePageActivity.this, Prefrence.CITYNAME);
        locationtxt.setText(city);
        showCartBar();
    }


    private void getLocation() {

        APIInterface apiInterface = ApiClient.getClient().create(APIInterface.class);
        Call<BannerListModel> resultCall = apiInterface.CallBannerList();
        //Call<HomePageListModel> resultCall = apiInterface.CallProductList(token,"7.448166400000001","9.0570752");

        resultCall.enqueue(new Callback<BannerListModel>() {
            @Override
            public void onResponse(Call<BannerListModel> call, Response<BannerListModel> response) {
               if (response.body().getStatus()) {

                } else {
                }
            }

            @Override
            public void onFailure(Call<BannerListModel> call, Throwable t) {
                call.cancel();
            }
        });
    }

    private void onclick() {
        profileImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(AnimationUtils.loadAnimation(HomePageActivity.this, R.anim.image_click));
                Intent in = new Intent(HomePageActivity.this, MyProfile.class);
                startActivity(in);
            }
        });
        nav_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(AnimationUtils.loadAnimation(HomePageActivity.this, R.anim.image_click));
                Intent in = new Intent(HomePageActivity.this, LocationManually.class); //LocationManually
                in.putExtra("isFromHome", true);
                startActivity(in);
                overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up);
            }
        });

    }

    public HomePageActivity getActivityObject() {
        return this;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (fragment != null) {
                // fragment.fetchCurrentLatLong();
            }
        } else {
            Toast.makeText(context, "Please enable gps location for better result", Toast.LENGTH_SHORT).show();
        }
    }

    private void FetchCart() {
        String token = Prefrence.get(this, Prefrence.KEY_TOKEN);
        APIInterface apiInterface = ApiClient.getClient().create(APIInterface.class);
        Call<FetchCartResponse> call = apiInterface.GetFetchCart("Bearer " + token);
        call.enqueue(new Callback<FetchCartResponse>() {
            @Override
            public void onResponse(Call<FetchCartResponse> call, Response<FetchCartResponse> response) {

                if (response != null && response.body() != null && response.body().getStatus()) {
                    //  dialog.dismiss();
                    CommonUtility.saveCartDetails(HomePageActivity.this, response.body().getResult());
                    showCartBar();
                } else {
                    Prefrence.saveInt(HomePageActivity.this, Prefrence.KEY_ITEM_COUNT, 0);
                    Prefrence.save(HomePageActivity.this, Prefrence.KEY_TOTAL, "0");
                    showCartBar();
                }
            }

            @Override
            public void onFailure(Call<FetchCartResponse> call, Throwable t) {
                call.cancel();
            }
        });
    }

    private void showCartBar() {
        if (Prefrence.getInt(this, Prefrence.KEY_ITEM_COUNT) > 0) {
            llCart.setVisibility(View.VISIBLE);
            tvVendorName.setText(Prefrence.get(this, Prefrence.KEY_VENDOR_NAME));
            /*snackbar = Snackbar.make(findViewById(android.R.id.content), Prefrence.getInt(HomePageActivity.this, Prefrence.KEY_ITEM_COUNT) + " item \n" + getString(R.string.nigiriacurrency) + " " + Prefrence.get(HomePageActivity.this, Prefrence.KEY_TOTAL)
                    , Snackbar.LENGTH_INDEFINITE);
            View snackbarView = snackbar.getView();
            TextView tv = (TextView) snackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
            tv.setMaxLines(3);
            tv.setTextSize(16);
            snackbar.setAction("View Cart", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(HomePageActivity.this, MyCart.class));
                }
            })
                    .setActionTextColor(Color.RED)
                    .show();*/
        }else{
            llCart.setVisibility(View.GONE);
        }
    }

    private void getProfile() {
        APIInterface apiInterface = ApiClient.getClient().create(APIInterface.class);
        Call<ProfileModel> call = apiInterface.GetProfile("Bearer " + Prefrence.get(HomePageActivity.this, Prefrence.KEY_TOKEN));
        call.enqueue(new Callback<ProfileModel>() {
            @Override
            public void onResponse(Call<ProfileModel> call, Response<ProfileModel> response) {
                if (response != null && response.body() != null && response.body().getStatus()) {
                    if(response.body().getParam().getDisplayPicture() != null){
                        Picasso.get().load(response.body().getParam().getDisplayPicture())
                                .placeholder(R.drawable.userprofilered)
                                .into(profileImage);
                    }
                }
            }

            @Override
            public void onFailure(Call<ProfileModel> call, Throwable t) {
                call.cancel();
            }
        });
    }

}
