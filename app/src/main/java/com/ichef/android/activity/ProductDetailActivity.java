package com.ichef.android.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.google.gson.Gson;
import com.ichef.android.R;
import com.ichef.android.adapter.MyCartAdapter;
import com.ichef.android.adapter.SlidingImage_Adapter;
import com.ichef.android.adapter.UnitAdapter;
import com.ichef.android.requestmodel.Cart.AddToCartRequest;
import com.ichef.android.requestmodel.Cart.ReduceCartRequest;
import com.ichef.android.responsemodel.Address.AddAddressResponse;
import com.ichef.android.responsemodel.cartmodel.Cart;
import com.ichef.android.responsemodel.cartmodel.FetchCartResponse;
import com.ichef.android.responsemodel.productdetail.Photo;
import com.ichef.android.responsemodel.cartmodel.Result;
import com.ichef.android.responsemodel.productdetail.ProductDetailResponse;
import com.ichef.android.responsemodel.productdetail.UnitPrice;
import com.ichef.android.responsemodel.productdetail.Vendor;
import com.ichef.android.retrofit.APIInterface;
import com.ichef.android.retrofit.ApiClient;
import com.ichef.android.utils.CommonUtility;
import com.ichef.android.utils.Prefrence;
import com.ichef.android.utils.TransparentProgressDialog;
import com.viewpagerindicator.CirclePageIndicator;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductDetailActivity extends AppCompatActivity {

    TextView tvproductname, tv_price, tv_discription, tv_vendorname,
             tv_CategoryDetails;
    RecyclerView recyclerUnit;
    UnitAdapter mUnitAdapter;
    LinearLayout ll_addtocart;
    String token;
    TransparentProgressDialog dialog;
    String productId;
    int quantity = 0;
    ImageView imgFood;
    String foodItem = "";
    String foodID = "";
    List<Photo> mPhotoList = new ArrayList<>();
    List<UnitPrice> mListData = new ArrayList<>();
    Vendor mVendorDetail;


    private ViewPager mPager;
    private  int currentPage = 0;
    private  int NUM_PAGES = 0;
    RelativeLayout relativeViewPager;
    CirclePageIndicator indicator;
    Context mContext = this;
    RecyclerView.LayoutManager rv_MyProjectLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_detail);
        ImageView imgBack = findViewById(R.id.imgBack);
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        productId = getIntent().getStringExtra("ProductId");
        init();
        onclick();
        getlist();
        dialog = new TransparentProgressDialog(ProductDetailActivity.this);
        dialog.show();
    }

    private void init() {
        mPager = findViewById(R.id.pager);
        relativeViewPager = findViewById(R.id.relativeViewPager);
        indicator = findViewById(R.id.indicator);

        tvproductname = findViewById(R.id.tv_productname);
        tv_price = findViewById(R.id.tv_price);
        tv_vendorname = findViewById(R.id.tv_vendorname);
        tv_discription = findViewById(R.id.tv_discription);
        tv_CategoryDetails = findViewById(R.id.tv_CategoryDetails);
        ll_addtocart = findViewById(R.id.ll_addtocart);
        imgFood = findViewById(R.id.imgFood);
        recyclerUnit = findViewById(R.id.recyclerUnit);

        recyclerUnit.setHasFixedSize(true);
        rv_MyProjectLayoutManager = new LinearLayoutManager(mContext);
        recyclerUnit.setLayoutManager(rv_MyProjectLayoutManager);
    }

    @Override
    protected void onResume() {
        super.onResume();
        FetchCart();
    }

    private void onclick() {

        tv_vendorname.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mVendorDetail != null){
                    Intent intent = new Intent(mContext, VendorDetailActivity.class);
                    intent.putExtra("VendorId", mVendorDetail.getId());
                    startActivity(intent);
                }else{
                    Toast.makeText(mContext, ""+getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
                }
            }
        });
        ll_addtocart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Prefrence.getInt(ProductDetailActivity.this, Prefrence.KEY_ITEM_COUNT) > 0) {
                    startActivity(new Intent(ProductDetailActivity.this, MyCart.class));
                } else {
                    Toast.makeText(ProductDetailActivity.this, "Cart is empty", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    public void clearCart() {
        TransparentProgressDialog dialog = new TransparentProgressDialog(ProductDetailActivity.this);
        dialog.show();
        String token = Prefrence.get(ProductDetailActivity.this, Prefrence.KEY_TOKEN);
        APIInterface apiInterface = ApiClient.getClient().create(APIInterface.class);
        Call<AddAddressResponse> call = apiInterface.ClearCart("Bearer " + token);
        call.enqueue(new Callback<AddAddressResponse>() {
            @Override
            public void onResponse(Call<AddAddressResponse> call, Response<AddAddressResponse> response) {
                dialog.dismiss();
                Prefrence.saveInt(ProductDetailActivity.this, Prefrence.KEY_ITEM_COUNT, 0);
                Prefrence.save(ProductDetailActivity.this, Prefrence.KEY_TOTAL, "0");
                updateCartUI();
            }

            @Override
            public void onFailure(Call<AddAddressResponse> call, Throwable t) {

                dialog.dismiss();
                call.cancel();
            }
        });
    }



    private void updateCartUI() {
        List<Cart> mCartLit = new ArrayList<>();
        Vendor cartVendroID = null;
        if (Prefrence.getInt(mContext, Prefrence.KEY_ITEM_COUNT) > 0) {
            Gson gson = new Gson();
            String json = Prefrence.get(mContext, Prefrence.KEY_CART);
            Result result = gson.fromJson(json, Result.class);
            mCartLit = result.getCart();
            cartVendroID = result.getVendor();
        }
        mUnitAdapter = new UnitAdapter(mContext, (ArrayList<UnitPrice>) mListData, mVendorDetail,mCartLit,foodID,foodItem,cartVendroID);
        recyclerUnit.setAdapter(mUnitAdapter);
    }

    private void getlist() {

        token = Prefrence.get(ProductDetailActivity.this, Prefrence.KEY_TOKEN);
        APIInterface apiInterface = ApiClient.getClient().create(APIInterface.class);
        Call<ProductDetailResponse> call = apiInterface.GetProductsDetails("Bearer " + token, productId);
        // Call<SearchResponse> call = apiInterface.GetSearch("Bearer " + token);
        call.enqueue(new Callback<ProductDetailResponse>() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<ProductDetailResponse> call, Response<ProductDetailResponse> response) {
                if (!response.equals(null)) {

                    dialog.dismiss();
                    String productname = response.body().getResult().getFoodItemName();
                    foodItem  = response.body().getResult().getFoodItemName();
                    foodID  = response.body().getResult().getId();
                    mListData = response.body().getResult().getUnitPrice();
                    mVendorDetail = response.body().getResult().getVendor();
                    if (mListData != null && mListData.size() > 0) {
                        tv_price.setText("₦ " + mListData.get(0).getPrice()+"/"+mListData.get(0).getUnitName());
                        updateCartUI();
                    }

                    String discription = response.body().getResult().getDescription();
                    String category = response.body().getResult().getCategory().getCatName();
                    String subCategory = response.body().getResult().getSubcategory().getSubCatName();
                    tvproductname.setText(productname);
                    tv_discription.setText(discription);
                    tv_CategoryDetails.setText(Html.fromHtml("<b>Category - </b>" + category + "<br><b>Sub Category - </b>" + subCategory, Html.FROM_HTML_SEPARATOR_LINE_BREAK_PARAGRAPH));
                    /*if(type.equalsIgnoreCase("Drink")){
                        tv_otherDetails.setText(Html.fromHtml("<b>Type - </b>" + type + "<br><b>Dietry - </b>" + diet, Html.FROM_HTML_SEPARATOR_LINE_BREAK_PARAGRAPH));
                        tv_testDetails.setText(Html.fromHtml("<b>Eat Time - </b>" + eat_time, Html.FROM_HTML_SEPARATOR_LINE_BREAK_PARAGRAPH));
                    }else{
                        tv_otherDetails.setText(Html.fromHtml("<b>Type - </b>" + type + "<br><b>Spicy - </b>" + spicy+ "<br><b>Dietry - </b>" + diet, Html.FROM_HTML_SEPARATOR_LINE_BREAK_PARAGRAPH));
                        tv_testDetails.setText(Html.fromHtml("<b>Ingredients - </b>" + ingredient + "<br><b>Eat Time - </b>" + eat_time, Html.FROM_HTML_SEPARATOR_LINE_BREAK_PARAGRAPH));
                    }*/

                    tv_vendorname.setText("By: " + response.body().getResult().getVendor().getBusinessName());
                    mPhotoList = response.body().getResult().getPhotos();
                    if(mPhotoList != null && mPhotoList.size()>0){
                        imgFood.setVisibility(View.GONE);
                        relativeViewPager.setVisibility(View.VISIBLE);
                        ArrayList<String> image_list = new ArrayList<>();
                        for (int i=0;i<mPhotoList.size();i++){
                            image_list.add(mPhotoList.get(i).getUrl());
                        }

                        mPager.setAdapter(new SlidingImage_Adapter(mContext, image_list));
                        indicator.setViewPager(mPager);
                        final float density = getResources().getDisplayMetrics().density;
                        indicator.setRadius(5 * density);

                        NUM_PAGES = image_list.size();

                        // Auto start of viewpager
                        if (image_list.size() > 1) {
                            final Handler handler = new Handler();
                            final Runnable Update = new Runnable() {
                                public void run() {
                                    if (currentPage == NUM_PAGES) {
                                        currentPage = 0;
                                    }
                                    mPager.setCurrentItem(currentPage++, true);
                                }
                            };

                            Timer swipeTimer = new Timer();
                            swipeTimer.schedule(new TimerTask() {
                                @Override
                                public void run() {
                                    handler.post(Update);
                                }
                            }, 5000, 4000);

                            indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                                @Override
                                public void onPageSelected(int position) {
                                    currentPage = position;
                                }

                                @Override
                                public void onPageScrolled(int pos, float arg1, int arg2) {
                                }

                                @Override
                                public void onPageScrollStateChanged(int pos) {
                                }
                            });
                        }
                    }else{
                        imgFood.setVisibility(View.VISIBLE);
                        relativeViewPager.setVisibility(View.GONE);
                    }

                } else {
                    Toast.makeText(ProductDetailActivity.this, getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ProductDetailResponse> call, Throwable t) {
                dialog.dismiss();
                call.cancel();
            }
        });
    }


    private void FetchCart() {
        String token = Prefrence.get(this, Prefrence.KEY_TOKEN);
        APIInterface apiInterface = ApiClient.getClient().create(APIInterface.class);
        Call<FetchCartResponse> call = apiInterface.GetFetchCart("Bearer " + token);
        call.enqueue(new Callback<FetchCartResponse>() {
            @Override
            public void onResponse(Call<FetchCartResponse> call, Response<FetchCartResponse> response) {

                if (response != null && response.body() != null && response.body().getStatus()) {
                    //  dialog.dismiss();
                    CommonUtility.saveCartDetails(mContext, response.body().getResult());

                } else {
                    Prefrence.saveInt(mContext, Prefrence.KEY_ITEM_COUNT, 0);
                    Prefrence.save(mContext, Prefrence.KEY_TOTAL, "0");
                }
                updateCartUI();
            }

            @Override
            public void onFailure(Call<FetchCartResponse> call, Throwable t) {
                call.cancel();
            }
        });
    }

}