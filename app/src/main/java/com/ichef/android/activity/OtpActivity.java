package com.ichef.android.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ichef.android.R;
import com.ichef.android.requestmodel.user.OTPRequest;
import com.ichef.android.responsemodel.otprequest.OTPResponse;
import com.ichef.android.retrofit.APIInterface;
import com.ichef.android.retrofit.ApiClient;

import retrofit2.Call;
import retrofit2.Callback;

public class OtpActivity extends AppCompatActivity {
    ImageView back;
    RelativeLayout btnCheckOTP;
    TextView time,txmobile;
    EditText etOTPbox;
    String otp="",id="",mobileNumber = "";
    TextView tvMobileNumber;
    TextView tvResend;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp);

        Intent intent = getIntent();
        otp=intent.getStringExtra("otp");
        id=intent.getStringExtra("id");
        mobileNumber=intent.getStringExtra("mymobile");

        ImageView back = findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        init();
        onclick();
        downTimer();
    }

    private void init() {
        back = findViewById(R.id.back);
        btnCheckOTP = findViewById(R.id.checkotp);
        time=findViewById(R.id.timer30);
        txmobile=findViewById(R.id.tx2);
        etOTPbox=findViewById(R.id.etOTPbox);
        tvMobileNumber=findViewById(R.id.tvMobileNumber);
        tvResend=findViewById(R.id.tvResend);
        tvMobileNumber.setText(mobileNumber);

    }

    private void onclick() {
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btnCheckOTP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(AnimationUtils.loadAnimation(OtpActivity.this, R.anim.image_click));
                if(etOTPbox.getText().toString().trim().equalsIgnoreCase("")){
                    etOTPbox.setError(getResources().getString(R.string.enter_otp));
                }else{
                    callCheckOTPApi();
                }
            }
        });

        tvResend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callResednOTPApi();
            }
        });


    }

    private void callCheckOTPApi() {
        OTPRequest request = new OTPRequest();
        request.setToken(etOTPbox.getText().toString());
        request.setId(id);

        APIInterface apiInterface = ApiClient.getClient().create(APIInterface.class);
        Call<OTPResponse> resultCall = apiInterface.CallOtp(request);
        resultCall.enqueue(new Callback<OTPResponse>() {

            @Override
            public void onResponse(Call<OTPResponse> call, retrofit2.Response<OTPResponse> response) {
                if (response.body().getStatus().equals(true)) {
                    Toast.makeText(OtpActivity.this,  ""+response.body().getMessage(), Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(OtpActivity.this, MobileLogin.class);
                    startActivity(intent);
                    finish();
                } else {
                    Toast.makeText(OtpActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<OTPResponse> call, Throwable t) {
                Toast.makeText(OtpActivity.this, getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
            }
        });

    }


    private void callResednOTPApi() {
        OTPRequest request = new OTPRequest();
        request.setId(id);

        APIInterface apiInterface = ApiClient.getClient().create(APIInterface.class);
        Call<OTPResponse> resultCall = apiInterface.CallResendOTP(request);
        resultCall.enqueue(new Callback<OTPResponse>() {

            @Override
            public void onResponse(Call<OTPResponse> call, retrofit2.Response<OTPResponse> response) {
                if (response.body().getStatus().equals(true)) {
                    Toast.makeText(OtpActivity.this,  ""+response.body().getMessage(), Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(OtpActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<OTPResponse> call, Throwable t) {
                Toast.makeText(OtpActivity.this, getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
            }
        });

    }


    private void downTimer(){
        new CountDownTimer(180*1000,1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                long second = (millisUntilFinished / 1000) % 60;
                long minutes = (millisUntilFinished/(1000*60)) % 60;
                time.setText(minutes + ":" + second);
            }

            @Override
            public void onFinish() {
                time.setText("Time Out");
            }
        }.start();
    }

}