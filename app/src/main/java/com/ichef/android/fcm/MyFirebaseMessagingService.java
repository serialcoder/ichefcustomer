package com.ichef.android.fcm;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;

import androidx.core.app.NotificationCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;


import com.facebook.appevents.ml.Utils;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.ichef.android.R;
import com.ichef.android.activity.SplashScreen;
import com.ichef.android.utils.Prefrence;

import org.json.JSONObject;

import java.util.Map;
import java.util.Random;
import java.util.prefs.Preferences;


public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = "PushNotification";
    private static Context mContext;

    @Override
    public void onNewToken(String s) {
        //super.onNewToken(s);
        Log.d(TAG, "onNewToken: "+ s);
        Prefrence.save(this, Prefrence.KEY_TOKEN,s);
    }

    public void onMessageReceived(RemoteMessage remoteMessage) {
        mContext = this;
        Log.d(TAG, "onMessageReceived: "+remoteMessage);
        if (remoteMessage == null)
            return;

        /*// Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.e(TAG, "Notification Body: " + remoteMessage.getNotification().getBody());
            handleNotification(remoteMessage.getNotification().getTitle(),remoteMessage.getNotification().getBody());
        }*/

        Log.e(TAG, "--------------Message Received--------------");
        Log.e(TAG, remoteMessage.getData().toString());


        Map<String, String> params = remoteMessage.getData();
        JSONObject object = new JSONObject(params);
        String title = object.optString("title");
        String message = object.optString("mytitle");

        createNotification(this, title, message);
    }

    private void sendMessage(String command) {
        Log.d("sender", "Broadcasting message");
        Intent intent = new Intent("fcm-notification");
        // You can also include some extra data.
        intent.putExtra("alertCommand", command);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }


    public void createNotification(Context context, String title, String body) {

        Random r = new Random();
        int notifyID = r.nextInt(45 - 28) + 28;
        int requestCode = r.nextInt(45 - 28) + 28;

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        Intent notificationIntent;
        PendingIntent intent;
        String notificationChannelId = getString(R.string.default_notification_channel_id);

        notificationIntent = new Intent(context, SplashScreen.class);
        intent = PendingIntent.getActivity(context, requestCode, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        // set intent so it does not start a new activity
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        NotificationCompat.Builder builder = getNotificationBuilder(context, intent, title, body, notificationChannelId);

        Notification notification = builder.build();
        Log.d(TAG, "Notification ID:" + notifyID);
        Log.d(TAG, "Request Code:" + requestCode);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = "MyChaneel";
            String description = "MyChaneelDEsc";;
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(notificationChannelId, name,
                    importance);
            channel.setDescription(description);
            notificationManager.createNotificationChannel(channel);
        }
        notificationManager.notify(notifyID, notification);

    }

    public NotificationCompat.Builder getNotificationBuilder(Context context,
                                                             PendingIntent mintent,
                                                             String title, String message,
                                                             String channelId) {

        NotificationCompat.Style style = new NotificationCompat.BigTextStyle().bigText(message);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, channelId)
                .setContentIntent(mintent)
                .setContentTitle(title)
                .setContentText(message)
                .setAutoCancel(true)
                .setStyle(style)
                .setSmallIcon(getNotificationIcon())
                .setColor(context.getResources().getColor(R.color.colorAccent))
                .setChannelId(channelId)
                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT);
        return builder;
    }

    public static int getNotificationIcon() {
        boolean whiteIcon = (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP);
        return whiteIcon ? R.drawable.logo : R.drawable.logo;
    }


}
