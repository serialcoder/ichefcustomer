package com.ichef.android.requestmodel.CreateOrder;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DisputeOrderRequest {
    @SerializedName("order")
    @Expose
    private String order;
    @SerializedName("disputeFor")
    @Expose
    private String disputeFor;
    @SerializedName("disputeAgainst")
    @Expose
    private String disputeAgainst;
    @SerializedName("reason")
    @Expose
    private String reason;

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public String getDisputeFor() {
        return disputeFor;
    }

    public void setDisputeFor(String disputeFor) {
        this.disputeFor = disputeFor;
    }

    public String getDisputeAgainst() {
        return disputeAgainst;
    }

    public void setDisputeAgainst(String disputeAgainst) {
        this.disputeAgainst = disputeAgainst;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}
