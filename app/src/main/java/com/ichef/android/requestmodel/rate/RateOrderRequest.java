package com.ichef.android.requestmodel.rate;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RateOrderRequest {
    @SerializedName("vendor")
    @Expose
    private String vendor;
    @SerializedName("orderID")
    @Expose
    private String orderID;
    @SerializedName("comment")
    @Expose
    private String comment;
    @SerializedName("rating")
    @Expose
    private Float rating;

    public String getVendor() {
        return vendor;
    }

    public void setVendor(String vendor) {
        this.vendor = vendor;
    }

    public String getOrderID() {
        return orderID;
    }

    public void setOrderID(String orderID) {
        this.orderID = orderID;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Float getRating() {
        return rating;
    }

    public void setRating(Float rating) {
        this.rating = rating;
    }
}
