
package com.ichef.android.requestmodel.payment;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ichef.android.requestmodel.AddNewPd.Photo;
import com.ichef.android.requestmodel.AddNewPd.UnitPrice;

import java.util.List;

import javax.annotation.Generated;

@Generated("jsonschema2pojo")
public class PaymentItem {

    @SerializedName("amount")
    @Expose
    private String amount;
    @SerializedName("user_id")
    @Expose
    private String user_id;
    @SerializedName("business_id")
    @Expose
    private String business_id;

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getBusiness_id() {
        return business_id;
    }

    public void setBusiness_id(String business_id) {
        this.business_id = business_id;
    }
}
