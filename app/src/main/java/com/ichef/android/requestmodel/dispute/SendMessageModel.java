package com.ichef.android.requestmodel.dispute;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class SendMessageModel implements Serializable
{

@SerializedName("disputeID")
@Expose
private String disputeID;
@SerializedName("message")
@Expose
private String message;
@SerializedName("file")
@Expose
private String file;
private final static long serialVersionUID = 2267305218491540387L;

public String getDisputeID() {
return disputeID;
}

public void setDisputeID(String disputeID) {
this.disputeID = disputeID;
}

public String getMessage() {
return message;
}

public void setMessage(String message) {
this.message = message;
}

public String getFile() {
return file;
}

public void setFile(String file) {
this.file = file;
}

}