
package com.ichef.android.requestmodel.markbookmark;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("jsonschema2pojo")
public class MarkBookmarkRequest {

    @SerializedName("userID")
    @Expose
    private String userID;
    @SerializedName("vendor")
    @Expose
    private String vendor;

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getVendor() {
        return vendor;
    }

    public void setVendor(String vendor) {
        this.vendor = vendor;
    }

}
