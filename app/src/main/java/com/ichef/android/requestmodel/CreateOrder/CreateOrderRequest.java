package com.ichef.android.requestmodel.CreateOrder;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("jsonschema2pojo")
public class CreateOrderRequest {

    @SerializedName("vendor")
    @Expose
    private String vendor;
    @SerializedName("status")
    @Expose
    private String status;

    public String getDelivery_type() {
        return delivery_type;
    }

    public void setDelivery_type(String delivery_type) {
        this.delivery_type = delivery_type;
    }

    @SerializedName("delivery_type")
    @Expose
    private String delivery_type;

    @SerializedName("comment")
    @Expose
    private String comment;

    @SerializedName("transaction_id")
    @Expose
    private String transactionId;
    @SerializedName("address")
    @Expose
    private Address address;
    @SerializedName("delivery_amount")
    @Expose
    private double deliveryAmount;
    @SerializedName("taxable_amount")
    @Expose
    private double taxableAmount;
    @SerializedName("total_cart_amount")
    @Expose
    private double totalCartAmount;
    @SerializedName("cart_amount")
    @Expose
    private double cartAmount;
    @SerializedName("amount_charged")
    @Expose
    private double amountCharged;
    @SerializedName("promo_slash")
    @Expose
    private double promoSlash;
    @SerializedName("discount_slash")
    @Expose
    private double discountSlash;
    @SerializedName("tip")
    @Expose
    private double tip;
    @SerializedName("coupon")
    @Expose
    private String coupon;

    @SerializedName("paid")
    @Expose
    private Boolean paid;

    public String getVendor() {
        return vendor;
    }

    public void setVendor(String vendor) {
        this.vendor = vendor;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public double getDeliveryAmount() {
        return deliveryAmount;
    }

    public void setDeliveryAmount(double deliveryAmount) {
        this.deliveryAmount = deliveryAmount;
    }

    public double getTaxableAmount() {
        return taxableAmount;
    }

    public void setTaxableAmount(double taxableAmount) {
        this.taxableAmount = taxableAmount;
    }

    public double getTotalCartAmount() {
        return totalCartAmount;
    }

    public void setTotalCartAmount(double totalCartAmount) {
        this.totalCartAmount = totalCartAmount;
    }

    public double getCartAmount() {
        return cartAmount;
    }

    public void setCartAmount(double cartAmount) {
        this.cartAmount = cartAmount;
    }

    public double getAmountCharged() {
        return amountCharged;
    }

    public void setAmountCharged(double amountCharged) {
        this.amountCharged = amountCharged;
    }

    public double getPromoSlash() {
        return promoSlash;
    }

    public void setPromoSlash(double promoSlash) {
        this.promoSlash = promoSlash;
    }

    public double getDiscountSlash() {
        return discountSlash;
    }

    public void setDiscountSlash(double discountSlash) {
        this.discountSlash = discountSlash;
    }

    public double getTip() {
        return tip;
    }

    public void setTip(double tip) {
        this.tip = tip;
    }

    public String getCoupon() {
        return coupon;
    }

    public void setCoupon(String coupon) {
        this.coupon = coupon;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Boolean getPaid() {
        return paid;
    }

    public void setPaid(Boolean paid) {
        this.paid = paid;
    }
}
