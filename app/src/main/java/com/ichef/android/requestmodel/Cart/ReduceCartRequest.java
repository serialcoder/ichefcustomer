package com.ichef.android.requestmodel.Cart;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ReduceCartRequest {
        @SerializedName("foodItem")
        @Expose
        private String foodItem;

        @SerializedName("userID")
        @Expose
        private String userID;

    @SerializedName("unit")
    @Expose
    private String unit;


        public String getFoodItem() {
        return foodItem;
    }

        public void setFoodItem(String foodItem) {
        this.foodItem = foodItem;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }



        public String getUserID() {
        return userID;
    }

        public void setUserID(String userID) {
        this.userID = userID;
    }


}
