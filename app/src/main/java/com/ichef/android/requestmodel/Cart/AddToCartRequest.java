package com.ichef.android.requestmodel.Cart;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AddToCartRequest {
    @SerializedName("foodItem")
    @Expose
    private String foodItem;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("userID")
    @Expose
    private String userID;
    @SerializedName("vendor")
    @Expose
    private String restaurantId;

    @SerializedName("foodItem_Name")
    @Expose
    private String foodItemName;

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    @SerializedName("unit")
    @Expose
    private String unit;

    public String getFoodItem() {
        return foodItem;
    }

    public void setFoodItem(String foodItem) {
        this.foodItem = foodItem;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getRestaurantId() {
        return restaurantId;
    }

    public void setRestaurantId(String restaurantId) {
        this.restaurantId = restaurantId;
    }

    public String getFoodItemName() {
        return foodItemName;
    }

    public void setFoodItemName(String foodItemName) {
        this.foodItemName = foodItemName;
    }

}
