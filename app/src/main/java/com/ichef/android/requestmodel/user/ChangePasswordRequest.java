
package com.ichef.android.requestmodel.user;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("jsonschema2pojo")
public class ChangePasswordRequest {

    @SerializedName("user_id")
    @Expose
    private String user_id;

    @SerializedName("otp")
    @Expose
    private String otp;

    @SerializedName("new_password")
    @Expose
    private String new_password;

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public String getNew_password() {
        return new_password;
    }

    public void setNew_password(String new_password) {
        this.new_password = new_password;
    }
}
