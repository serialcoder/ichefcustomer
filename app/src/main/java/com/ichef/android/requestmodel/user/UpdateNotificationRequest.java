package com.ichef.android.requestmodel.user;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UpdateNotificationRequest {
    @SerializedName("push_notification")
    @Expose
    private Boolean pushNotification;

    public Boolean getPushNotification() {
        return pushNotification;
    }

    public void setPushNotification(Boolean pushNotification) {
        this.pushNotification = pushNotification;
    }
}
