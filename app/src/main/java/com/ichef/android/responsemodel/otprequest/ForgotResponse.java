
package com.ichef.android.responsemodel.otprequest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ForgotResponse {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("param")
    @Expose
    private ParamForgot param;
    @SerializedName("message")
    @Expose
    private String message;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public ParamForgot getParam() {
        return param;
    }

    public void setParam(ParamForgot param) {
        this.param = param;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
