package com.ichef.android.responsemodel.payment;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Response implements Serializable
{

@SerializedName("id")
@Expose
private String id;
@SerializedName("authorization_url")
@Expose
private String authorizationUrl;
@SerializedName("access_code")
@Expose
private String accessCode;
private final static long serialVersionUID = -7700652359920275504L;

public String getId() {
return id;
}

public void setId(String id) {
this.id = id;
}

public String getAuthorizationUrl() {
return authorizationUrl;
}

public void setAuthorizationUrl(String authorizationUrl) {
this.authorizationUrl = authorizationUrl;
}

public String getAccessCode() {
return accessCode;
}

public void setAccessCode(String accessCode) {
this.accessCode = accessCode;
}

}