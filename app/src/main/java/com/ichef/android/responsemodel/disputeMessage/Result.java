package com.ichef.android.responsemodel.disputeMessage;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Result implements Serializable
{

@SerializedName("disputeID")
@Expose
private String disputeID;
@SerializedName("message")
@Expose
private String message;
@SerializedName("file")
@Expose
private Object file;
@SerializedName("_id")
@Expose
private String id;
@SerializedName("user")
@Expose
private User user;
@SerializedName("datecreated")
@Expose
private String datecreated;
@SerializedName("__v")
@Expose
private Integer v;
private final static long serialVersionUID = -5638744679815513213L;

public String getDisputeID() {
return disputeID;
}

public void setDisputeID(String disputeID) {
this.disputeID = disputeID;
}

public String getMessage() {
return message;
}

public void setMessage(String message) {
this.message = message;
}

public Object getFile() {
return file;
}

public void setFile(Object file) {
this.file = file;
}

public String getId() {
return id;
}

public void setId(String id) {
this.id = id;
}

public User getUser() {
return user;
}

public void setUser(User user) {
this.user = user;
}

public String getDatecreated() {
return datecreated;
}

public void setDatecreated(String datecreated) {
this.datecreated = datecreated;
}

public Integer getV() {
return v;
}

public void setV(Integer v) {
this.v = v;
}

}