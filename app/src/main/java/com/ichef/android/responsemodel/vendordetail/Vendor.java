
package com.ichef.android.responsemodel.vendordetail;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("jsonschema2pojo")
public class Vendor {

    @SerializedName("location")
    @Expose
    private Location location;
    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("mobile_number")
    @Expose
    private String mobileNumber;
    @SerializedName("averageRating")
    @Expose
    private Double averageRating;
    @SerializedName("business_name")
    @Expose
    private String businessName;
    @SerializedName("business_type")
    @Expose
    private String businessType;
    @SerializedName("display_picture")
    @Expose
    private String displayPicture;

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public Double getAverageRating() {
        return averageRating;
    }

    public void setAverageRating(Double averageRating) {
        this.averageRating = averageRating;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public String getBusinessType() {
        return businessType;
    }

    public void setBusinessType(String businessType) {
        this.businessType = businessType;
    }

    public String getDisplayPicture() {
        return displayPicture;
    }

    public void setDisplayPicture(String displayPicture) {
        this.displayPicture = displayPicture;
    }

}
