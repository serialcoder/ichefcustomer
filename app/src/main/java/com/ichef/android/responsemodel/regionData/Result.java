package com.ichef.android.responsemodel.regionData;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Result implements Serializable
{

@SerializedName("image")
@Expose
private String image;
@SerializedName("deleted")
@Expose
private Boolean deleted;
@SerializedName("_id")
@Expose
private String id;
@SerializedName("name")
@Expose
private String name;
@SerializedName("datecreated")
@Expose
private String datecreated;
@SerializedName("lastmodified")
@Expose
private String lastmodified;
@SerializedName("__v")
@Expose
private Integer v;
private final static long serialVersionUID = -9029963598212322395L;

public String getImage() {
return image;
}

public void setImage(String image) {
this.image = image;
}

public Boolean getDeleted() {
return deleted;
}

public void setDeleted(Boolean deleted) {
this.deleted = deleted;
}

public String getId() {
return id;
}

public void setId(String id) {
this.id = id;
}

public String getName() {
return name;
}

public void setName(String name) {
this.name = name;
}

public String getDatecreated() {
return datecreated;
}

public void setDatecreated(String datecreated) {
this.datecreated = datecreated;
}

public String getLastmodified() {
return lastmodified;
}

public void setLastmodified(String lastmodified) {
this.lastmodified = lastmodified;
}

public Integer getV() {
return v;
}

public void setV(Integer v) {
this.v = v;
}

}