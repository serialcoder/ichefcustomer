package com.ichef.android.responsemodel.checkMobile;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Param implements Serializable
{

@SerializedName("loginredirect")
@Expose
private Boolean loginredirect;
private final static long serialVersionUID = 4715441869488088220L;

public Boolean getLoginredirect() {
return loginredirect;
}

public void setLoginredirect(Boolean loginredirect) {
this.loginredirect = loginredirect;
}

}