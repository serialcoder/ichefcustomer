package com.ichef.android.responsemodel.search;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ichef.android.requestmodel.AddNewPd.Photo;
import com.ichef.android.requestmodel.AddNewPd.UnitPrice;

import java.io.Serializable;
import java.util.List;

public class Product implements Serializable
{

@SerializedName("photos")
@Expose
private List<Photo> photos = null;
@SerializedName("_id")
@Expose
private String id;
@SerializedName("foodItem_name")
@Expose
private String foodItemName;
@SerializedName("description")
@Expose
private String description;
@SerializedName("stock_quantity")
@Expose
private Integer stockQuantity;
@SerializedName("unit_price")
@Expose
private List<UnitPrice> unitPrice = null;
@SerializedName("vendor")
@Expose
private Vendor vendor;
private final static long serialVersionUID = 2498409426338268945L;

public List<Photo> getPhotos() {
return photos;
}

public void setPhotos(List<Photo> photos) {
this.photos = photos;
}

public String getId() {
return id;
}

public void setId(String id) {
this.id = id;
}

public String getFoodItemName() {
return foodItemName;
}

public void setFoodItemName(String foodItemName) {
this.foodItemName = foodItemName;
}

public String getDescription() {
return description;
}

public void setDescription(String description) {
this.description = description;
}

public Integer getStockQuantity() {
return stockQuantity;
}

public void setStockQuantity(Integer stockQuantity) {
this.stockQuantity = stockQuantity;
}

public List<UnitPrice> getUnitPrice() {
return unitPrice;
}

public void setUnitPrice(List<UnitPrice> unitPrice) {
this.unitPrice = unitPrice;
}

public Vendor getVendor() {
return vendor;
}

public void setVendor(Vendor vendor) {
this.vendor = vendor;
}

}