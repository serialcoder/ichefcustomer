
package com.ichef.android.responsemodel.PromoCode;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("jsonschema2pojo")
public class Result {

    @SerializedName("used")
    @Expose
    private Boolean used;
    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("priceSlash")
    @Expose
    private double priceSlash;
    @SerializedName("__v")
    @Expose
    private Integer v;

    public Boolean getUsed() {
        return used;
    }

    public void setUsed(Boolean used) {
        this.used = used;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public double getPriceSlash() {
        return priceSlash;
    }

    public void setPriceSlash(double priceSlash) {
        this.priceSlash = priceSlash;
    }

    public Integer getV() {
        return v;
    }

    public void setV(Integer v) {
        this.v = v;
    }

}
