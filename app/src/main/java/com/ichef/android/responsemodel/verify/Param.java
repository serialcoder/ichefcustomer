package com.ichef.android.responsemodel.verify;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Param implements Serializable
{

@SerializedName("gateway_response")
@Expose
private String gatewayResponse;
@SerializedName("message")
@Expose
private Object message;
@SerializedName("status")
@Expose
private String status;
private final static long serialVersionUID = 3036735063300356963L;

public String getGatewayResponse() {
return gatewayResponse;
}

public void setGatewayResponse(String gatewayResponse) {
this.gatewayResponse = gatewayResponse;
}

public Object getMessage() {
return message;
}

public void setMessage(Object message) {
this.message = message;
}

public String getStatus() {
return status;
}

public void setStatus(String status) {
this.status = status;
}

}