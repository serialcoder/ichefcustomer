
package com.ichef.android.responsemodel.otprequest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class ParamForgot {

    @SerializedName("otp")
    @Expose
    private String otp;

    @SerializedName("id")
    @Expose
    private String id;

    public String getOTP() {
        return otp;
    }

    public void setOTP(String otp) {
        this.otp = otp;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
