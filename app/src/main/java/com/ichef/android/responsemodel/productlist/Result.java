package com.ichef.android.responsemodel.productlist;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class Result implements Serializable {

    @SerializedName("vendors")
    @Expose
    private List<Vendor> vendors = null;
    @SerializedName("defaultimage")
    @Expose
    private String defaultimage;
    private final static long serialVersionUID = -934835382070469696L;

    public List<Vendor> getVendors() {
        return vendors;
    }

    public void setVendors(List<Vendor> vendors) {
        this.vendors = vendors;
    }

    public String getDefaultimage() {
        return defaultimage;
    }

    public void setDefaultimage(String defaultimage) {
        this.defaultimage = defaultimage;
    }
}