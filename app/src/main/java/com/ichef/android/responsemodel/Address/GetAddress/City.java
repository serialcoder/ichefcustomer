package com.ichef.android.responsemodel.Address.GetAddress;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import javax.annotation.Generated;

@Generated("jsonschema2pojo")
public class City implements Serializable {

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("cityname")
    @Expose
    private String cityname;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCityname() {
        return cityname;
    }

    public void setCityname(String cityname) {
        this.cityname = cityname;
    }

}
