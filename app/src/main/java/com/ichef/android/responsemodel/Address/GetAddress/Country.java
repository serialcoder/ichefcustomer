package com.ichef.android.responsemodel.Address.GetAddress;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import javax.annotation.Generated;

@Generated("jsonschema2pojo")
public class Country implements Serializable {

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("countryname")
    @Expose
    private String countryname;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCountryname() {
        return countryname;
    }

    public void setCountryname(String countryname) {
        this.countryname = countryname;
    }

}
