
package com.ichef.android.responsemodel.orders;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("jsonschema2pojo")
public class Cart {

    @SerializedName("quantity")
    @Expose
    private Integer quantity;
    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("foodItem")
    @Expose
    private String foodItem;
    @SerializedName("foodItem_Name")
    @Expose
    private String foodItemName;
    @SerializedName("price")
    @Expose
    private Float price;
    @SerializedName("unit")
    @Expose
    private String unit;

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFoodItem() {
        return foodItem;
    }

    public void setFoodItem(String foodItem) {
        this.foodItem = foodItem;
    }

    public String getFoodItemName() {
        return foodItemName;
    }

    public void setFoodItemName(String foodItemName) {
        this.foodItemName = foodItemName;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

}
