package com.ichef.android.responsemodel.orders.Reorder;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Result {
    @SerializedName("cartItemsCount")
    @Expose
    private Integer cartItemsCount;
    @SerializedName("cartAmount")
    @Expose
    private Integer cartAmount;
    @SerializedName("deliveryAmount")
    @Expose
    private Integer deliveryAmount;
    @SerializedName("taxableAmount")
    @Expose
    private Integer taxableAmount;
    @SerializedName("totalCartAmount")
    @Expose
    private Integer totalCartAmount;
    @SerializedName("isDeliverable")
    @Expose
    private Boolean isDeliverable;
    @SerializedName("cart")
    @Expose
    private List<Object> cart = null;

    public Integer getCartItemsCount() {
        return cartItemsCount;
    }

    public void setCartItemsCount(Integer cartItemsCount) {
        this.cartItemsCount = cartItemsCount;
    }

    public Integer getCartAmount() {
        return cartAmount;
    }

    public void setCartAmount(Integer cartAmount) {
        this.cartAmount = cartAmount;
    }

    public Integer getDeliveryAmount() {
        return deliveryAmount;
    }

    public void setDeliveryAmount(Integer deliveryAmount) {
        this.deliveryAmount = deliveryAmount;
    }

    public Integer getTaxableAmount() {
        return taxableAmount;
    }

    public void setTaxableAmount(Integer taxableAmount) {
        this.taxableAmount = taxableAmount;
    }

    public Integer getTotalCartAmount() {
        return totalCartAmount;
    }

    public void setTotalCartAmount(Integer totalCartAmount) {
        this.totalCartAmount = totalCartAmount;
    }

    public Boolean getIsDeliverable() {
        return isDeliverable;
    }

    public void setIsDeliverable(Boolean isDeliverable) {
        this.isDeliverable = isDeliverable;
    }

    public List<Object> getCart() {
        return cart;
    }

    public void setCart(List<Object> cart) {
        this.cart = cart;
    }
}
