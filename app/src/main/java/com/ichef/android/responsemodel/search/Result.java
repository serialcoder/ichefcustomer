package com.ichef.android.responsemodel.search;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class Result implements Serializable
{

@SerializedName("products")
@Expose
private List<Product> products = null;
@SerializedName("searchtype")
@Expose
private String searchtype;
@SerializedName("vendors")
@Expose
private List<Vendor__1> vendors = null;
@SerializedName("defaultimage")
@Expose
private String defaultimage;
private final static long serialVersionUID = 1166061924343195992L;

public List<Product> getProducts() {
return products;
}

public void setProducts(List<Product> products) {
this.products = products;
}

public String getSearchtype() {
return searchtype;
}

public void setSearchtype(String searchtype) {
this.searchtype = searchtype;
}

public List<Vendor__1> getVendors() {
return vendors;
}

public void setVendors(List<Vendor__1> vendors) {
this.vendors = vendors;
}

public String getDefaultimage() {
return defaultimage;
}

public void setDefaultimage(String defaultimage) {
this.defaultimage = defaultimage;
}

}
