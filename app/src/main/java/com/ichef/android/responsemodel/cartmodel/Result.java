package com.ichef.android.responsemodel.cartmodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ichef.android.responsemodel.productdetail.Vendor;

import java.util.List;

import javax.annotation.Generated;

@Generated("jsonschema2pojo")
public class Result {

    @SerializedName("cartItemsCount")
    @Expose
    private Integer cartItemsCount;
    @SerializedName("cartAmount")
    @Expose
    private double cartAmount;
    @SerializedName("deliveryAmount")
    @Expose
    private String deliveryAmount;
    @SerializedName("taxableAmount")
    @Expose
    private double taxableAmount;
    @SerializedName("totalCartAmount")
    @Expose
    private double totalCartAmount;
    @SerializedName("cart")
    @Expose
    private List<Cart> cart = null;
    @SerializedName("vendor")
    @Expose
    private Vendor vendor;

    public Integer getCartItemsCount() {
        return cartItemsCount;
    }

    public void setCartItemsCount(Integer cartItemsCount) {
        this.cartItemsCount = cartItemsCount;
    }

    public double getCartAmount() {
        return cartAmount;
    }

    public void setCartAmount(double cartAmount) {
        this.cartAmount = cartAmount;
    }

    public String getDeliveryAmount() {
        return deliveryAmount;
    }

    public void setDeliveryAmount(String deliveryAmount) {
        this.deliveryAmount = deliveryAmount;
    }

    public double getTaxableAmount() {
        return taxableAmount;
    }

    public void setTaxableAmount(double taxableAmount) {
        this.taxableAmount = taxableAmount;
    }

    public double getTotalCartAmount() {
        return totalCartAmount;
    }

    public void setTotalCartAmount(double totalCartAmount) {
        this.totalCartAmount = totalCartAmount;
    }

    public List<Cart> getCart() {
        return cart;
    }

    public void setCart(List<Cart> cart) {
        this.cart = cart;
    }

    public Vendor getVendor() {
        return vendor;
    }

    public void setVendor(Vendor vendor) {
        this.vendor = vendor;
    }
}
