package com.ichef.android.responsemodel.dispute;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class DisputeFor implements Serializable
{

@SerializedName("firstname")
@Expose
private String firstname;
@SerializedName("user_type")
@Expose
private String userType;
@SerializedName("business_name")
@Expose
private Object businessName;
@SerializedName("_id")
@Expose
private String id;
private final static long serialVersionUID = -7474709467085304674L;

public String getFirstname() {
return firstname;
}

public void setFirstname(String firstname) {
this.firstname = firstname;
}

public String getUserType() {
return userType;
}

public void setUserType(String userType) {
this.userType = userType;
}

public Object getBusinessName() {
return businessName;
}

public void setBusinessName(Object businessName) {
this.businessName = businessName;
}

public String getId() {
return id;
}

public void setId(String id) {
this.id = id;
}

}