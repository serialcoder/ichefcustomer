package com.ichef.android.responsemodel.search;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Vendor implements Serializable
{

@SerializedName("_id")
@Expose
private String id;
@SerializedName("averageRating")
@Expose
private Double averageRating;
@SerializedName("business_name")
@Expose
private String businessName;
@SerializedName("hours_availability")
@Expose
private String hoursAvailability;
@SerializedName("weekdays_availability")
@Expose
private String weekdaysAvailability;
private final static long serialVersionUID = -6766403613307382465L;

public String getId() {
return id;
}

public void setId(String id) {
this.id = id;
}

public Double getAverageRating() {
return averageRating;
}

public void setAverageRating(Double averageRating) {
this.averageRating = averageRating;
}

public String getBusinessName() {
return businessName;
}

public void setBusinessName(String businessName) {
this.businessName = businessName;
}

public String getHoursAvailability() {
return hoursAvailability;
}

public void setHoursAvailability(String hoursAvailability) {
this.hoursAvailability = hoursAvailability;
}

public String getWeekdaysAvailability() {
return weekdaysAvailability;
}

public void setWeekdaysAvailability(String weekdaysAvailability) {
this.weekdaysAvailability = weekdaysAvailability;
}

}