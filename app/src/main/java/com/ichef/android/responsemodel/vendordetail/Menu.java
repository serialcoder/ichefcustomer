
package com.ichef.android.responsemodel.vendordetail;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import javax.annotation.Generated;

@Generated("jsonschema2pojo")
public class Menu {

    @SerializedName("categoryid")
    @Expose
    private String categoryid;
    @SerializedName("categoryname")
    @Expose
    private String categoryname;
    @SerializedName("products")
    @Expose
    private List<com.ichef.android.responsemodel.vendordetail.Product> products = null;

    public String getCategoryid() {
        return categoryid;
    }

    public void setCategoryid(String categoryid) {
        this.categoryid = categoryid;
    }

    public String getCategoryname() {
        return categoryname;
    }

    public void setCategoryname(String categoryname) {
        this.categoryname = categoryname;
    }

    public List<com.ichef.android.responsemodel.vendordetail.Product> getProducts() {
        return products;
    }

    public void setProducts(List<com.ichef.android.responsemodel.vendordetail.Product> products) {
        this.products = products;
    }

}
