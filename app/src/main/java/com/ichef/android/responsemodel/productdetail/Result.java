
package com.ichef.android.responsemodel.productdetail;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

import javax.annotation.Generated;

@Generated("jsonschema2pojo")
public class Result implements Serializable
{

    @SerializedName("type")
    @Expose
    private Type type;
    @SerializedName("foodclass")
    @Expose
    private Foodclass foodclass;
    @SerializedName("region")
    @Expose
    private Object region;
    @SerializedName("dietry")
    @Expose
    private Dietry dietry;
    @SerializedName("spicy")
    @Expose
    private Spicy spicy;
    @SerializedName("eattime")
    @Expose
    private Eattime eattime;
    @SerializedName("photos")
    @Expose
    private List<Photo> photos = null;
    @SerializedName("packaging_time")
    @Expose
    private Integer packagingTime;
    @SerializedName("deleted")
    @Expose
    private Boolean deleted;
    @SerializedName("availability_status")
    @Expose
    private Boolean availabilityStatus;
    @SerializedName("active")
    @Expose
    private Boolean active;
    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("foodItem_name")
    @Expose
    private String foodItemName;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("ingredient")
    @Expose
    private String ingredient;
    @SerializedName("taste")
    @Expose
    private String taste;
    @SerializedName("regions")
    @Expose
    private String regions;
    @SerializedName("eat_time")
    @Expose
    private Eattime eatTime;
    @SerializedName("category")
    @Expose
    private Category category;
    @SerializedName("subcategory")
    @Expose
    private Subcategory subcategory;
    @SerializedName("stock_quantity")
    @Expose
    private Integer stockQuantity;
    @SerializedName("unit_price")
    @Expose
    private List<UnitPrice> unitPrice = null;
    @SerializedName("vendor")
    @Expose
    private Vendor vendor;
    @SerializedName("datecreated")
    @Expose
    private String datecreated;
    @SerializedName("lastmodified")
    @Expose
    private String lastmodified;
    @SerializedName("__v")
    @Expose
    private Integer v;
    private final static long serialVersionUID = 2082739439425361376L;

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public Foodclass getFoodclass() {
        return foodclass;
    }

    public void setFoodclass(Foodclass foodclass) {
        this.foodclass = foodclass;
    }

    public Object getRegion() {
        return region;
    }

    public void setRegion(Object region) {
        this.region = region;
    }

    public Dietry getDietry() {
        return dietry;
    }

    public void setDietry(Dietry dietry) {
        this.dietry = dietry;
    }

    public Spicy getSpicy() {
        return spicy;
    }

    public void setSpicy(Spicy spicy) {
        this.spicy = spicy;
    }

    public Eattime getEattime() {
        return eattime;
    }

    public void setEattime(Eattime eattime) {
        this.eattime = eattime;
    }

    public List<Photo> getPhotos() {
        return photos;
    }

    public void setPhotos(List<Photo> photos) {
        this.photos = photos;
    }

    public Integer getPackagingTime() {
        return packagingTime;
    }

    public void setPackagingTime(Integer packagingTime) {
        this.packagingTime = packagingTime;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public Boolean getAvailabilityStatus() {
        return availabilityStatus;
    }

    public void setAvailabilityStatus(Boolean availabilityStatus) {
        this.availabilityStatus = availabilityStatus;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFoodItemName() {
        return foodItemName;
    }

    public void setFoodItemName(String foodItemName) {
        this.foodItemName = foodItemName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIngredient() {
        return ingredient;
    }

    public void setIngredient(String ingredient) {
        this.ingredient = ingredient;
    }

    public String getTaste() {
        return taste;
    }

    public void setTaste(String taste) {
        this.taste = taste;
    }

    public String getRegions() {
        return regions;
    }

    public void setRegions(String regions) {
        this.regions = regions;
    }


    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Subcategory getSubcategory() {
        return subcategory;
    }

    public void setSubcategory(Subcategory subcategory) {
        this.subcategory = subcategory;
    }

    public Integer getStockQuantity() {
        return stockQuantity;
    }

    public void setStockQuantity(Integer stockQuantity) {
        this.stockQuantity = stockQuantity;
    }

    public List<UnitPrice> getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(List<UnitPrice> unitPrice) {
        this.unitPrice = unitPrice;
    }

    public Vendor getVendor() {
        return vendor;
    }

    public void setVendor(Vendor vendor) {
        this.vendor = vendor;
    }

    public String getDatecreated() {
        return datecreated;
    }

    public void setDatecreated(String datecreated) {
        this.datecreated = datecreated;
    }

    public String getLastmodified() {
        return lastmodified;
    }

    public void setLastmodified(String lastmodified) {
        this.lastmodified = lastmodified;
    }

    public Integer getV() {
        return v;
    }

    public void setV(Integer v) {
        this.v = v;
    }

}