package com.ichef.android.responsemodel.disputeMessage;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class DisputedMessageModel implements Serializable
{

@SerializedName("status")
@Expose
private Boolean status;
@SerializedName("result")
@Expose
private List<Result> result = null;
@SerializedName("message")
@Expose
private String message;
private final static long serialVersionUID = 5858639338086064270L;

public Boolean getStatus() {
return status;
}

public void setStatus(Boolean status) {
this.status = status;
}

public List<Result> getResult() {
return result;
}

public void setResult(List<Result> result) {
this.result = result;
}

public String getMessage() {
return message;
}

public void setMessage(String message) {
this.message = message;
}

}