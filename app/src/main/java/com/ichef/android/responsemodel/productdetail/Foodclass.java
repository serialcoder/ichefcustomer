package com.ichef.android.responsemodel.productdetail;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Foodclass implements Serializable
{

@SerializedName("_id")
@Expose
private String id;
@SerializedName("name")
@Expose
private String name;
private final static long serialVersionUID = 6528325951635372762L;

public String getId() {
return id;
}

public void setId(String id) {
this.id = id;
}

public String getName() {
return name;
}

public void setName(String name) {
this.name = name;
}

}