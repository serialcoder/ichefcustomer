package com.ichef.android.responsemodel.banner;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ichef.android.responsemodel.banner.Result;

import java.io.Serializable;
import java.util.List;

public class BannerListModel implements Serializable
{

@SerializedName("status")
@Expose
private Boolean status;
@SerializedName("result")
@Expose
private List<com.ichef.android.responsemodel.banner.Result> result = null;
@SerializedName("message")
@Expose
private String message;
private final static long serialVersionUID = 1800703230837856111L;

public Boolean getStatus() {
return status;
}

public void setStatus(Boolean status) {
this.status = status;
}

public List<com.ichef.android.responsemodel.banner.Result> getResult() {
return result;
}

public void setResult(List<Result> result) {
this.result = result;
}

public String getMessage() {
return message;
}

public void setMessage(String message) {
this.message = message;
}

}