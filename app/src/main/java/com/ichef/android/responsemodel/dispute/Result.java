package com.ichef.android.responsemodel.dispute;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Result implements Serializable
{

@SerializedName("status")
@Expose
private String status;
@SerializedName("_id")
@Expose
private String id;
@SerializedName("disputeAgainst")
@Expose
private DisputeAgainst disputeAgainst;
@SerializedName("disputeFor")
@Expose
private DisputeFor disputeFor;
@SerializedName("order")
@Expose
private String order;
@SerializedName("reason")
@Expose
private String reason;
private final static long serialVersionUID = -1395749957791622079L;

public String getStatus() {
return status;
}

public void setStatus(String status) {
this.status = status;
}

public String getId() {
return id;
}

public void setId(String id) {
this.id = id;
}

public DisputeAgainst getDisputeAgainst() {
return disputeAgainst;
}

public void setDisputeAgainst(DisputeAgainst disputeAgainst) {
this.disputeAgainst = disputeAgainst;
}

public DisputeFor getDisputeFor() {
return disputeFor;
}

public void setDisputeFor(DisputeFor disputeFor) {
this.disputeFor = disputeFor;
}

public String getOrder() {
return order;
}

public void setOrder(String order) {
this.order = order;
}

public String getReason() {
return reason;
}

public void setReason(String reason) {
this.reason = reason;
}

}