package com.ichef.android.responsemodel.verify;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class VerifyResponseModel implements Serializable
{

@SerializedName("status")
@Expose
private Boolean status;
@SerializedName("param")
@Expose
private Param param;
@SerializedName("message")
@Expose
private String message;
private final static long serialVersionUID = -2072236712186550607L;

public Boolean getStatus() {
return status;
}

public void setStatus(Boolean status) {
this.status = status;
}

public Param getParam() {
return param;
}

public void setParam(Param param) {
this.param = param;
}

public String getMessage() {
return message;
}

public void setMessage(String message) {
this.message = message;
}

}