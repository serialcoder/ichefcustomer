package com.ichef.android.responsemodel.dispute;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class DisputeAgainst implements Serializable
{

@SerializedName("firstname")
@Expose
private String firstname;
@SerializedName("user_type")
@Expose
private String userType;
@SerializedName("business_name")
@Expose
private String businessName;
@SerializedName("_id")
@Expose
private String id;
private final static long serialVersionUID = -8976403431156754498L;

public String getFirstname() {
return firstname;
}

public void setFirstname(String firstname) {
this.firstname = firstname;
}

public String getUserType() {
return userType;
}

public void setUserType(String userType) {
this.userType = userType;
}

public String getBusinessName() {
return businessName;
}

public void setBusinessName(String businessName) {
this.businessName = businessName;
}

public String getId() {
return id;
}

public void setId(String id) {
this.id = id;
}

}