
package com.ichef.android.responsemodel.cartmodel;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("jsonschema2pojo")
public class FoodItem {

    @SerializedName("photos")
    @Expose
    private List<Photo> photos = null;
    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("foodItem_name")
    @Expose
    private String foodItemName;

    public List<Photo> getPhotos() {
        return photos;
    }

    public void setPhotos(List<Photo> photos) {
        this.photos = photos;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFoodItemName() {
        return foodItemName;
    }

    public void setFoodItemName(String foodItemName) {
        this.foodItemName = foodItemName;
    }

}
