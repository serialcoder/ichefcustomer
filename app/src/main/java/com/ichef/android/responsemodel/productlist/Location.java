package com.ichef.android.responsemodel.productlist;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class Location implements Serializable
{

@SerializedName("coordinates")
@Expose
private List<Double> coordinates = null;
private final static long serialVersionUID = -3936736904044659391L;

public List<Double> getCoordinates() {
return coordinates;
}

public void setCoordinates(List<Double> coordinates) {
this.coordinates = coordinates;
}

}