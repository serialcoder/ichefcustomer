package com.ichef.android.responsemodel.productlist;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class HoursAvailability implements Serializable
{

@SerializedName("opening_time")
@Expose
private String openingTime;
@SerializedName("closing_time")
@Expose
private String closingTime;
private final static long serialVersionUID = -4294909048134701799L;

public String getOpeningTime() {
return openingTime;
}

public void setOpeningTime(String openingTime) {
this.openingTime = openingTime;
}

public String getClosingTime() {
return closingTime;
}

public void setClosingTime(String closingTime) {
this.closingTime = closingTime;
}

}