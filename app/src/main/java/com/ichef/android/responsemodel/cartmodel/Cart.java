
package com.ichef.android.responsemodel.cartmodel;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


@Generated("jsonschema2pojo")
public class Cart {

    @SerializedName("quantity")
    @Expose
    private Integer quantity;
    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("foodItem")
    @Expose
    private FoodItem foodItem;
    @SerializedName("price")
    @Expose
    private Integer price;
    @SerializedName("unit")
    @Expose
    private Unit unit;

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public FoodItem getFoodItem() {
        return foodItem;
    }

    public void setFoodItem(FoodItem foodItem) {
        this.foodItem = foodItem;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Unit getUnit() {
        return unit;
    }

    public void setUnit(Unit unit) {
        this.unit = unit;
    }
}
