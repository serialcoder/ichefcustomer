package com.ichef.android.responsemodel.profile;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Param implements Serializable
{

@SerializedName("firstname")
@Expose
private String firstname;
@SerializedName("lastname")
@Expose
private Object lastname;
@SerializedName("business_name")
@Expose
private String businessName;
@SerializedName("display_picture")
@Expose
private String displayPicture;
@SerializedName("business_email")
@Expose
private String businessEmail;
@SerializedName("email")
@Expose
private String email;
@SerializedName("mobile_number")
@Expose
private String mobileNumber;
@SerializedName("business_address")
@Expose
private String businessAddress;
@SerializedName("passport")
@Expose
private Object passport;
@SerializedName("_id")
@Expose
private String id;
private final static long serialVersionUID = -7068496653033656154L;

public String getFirstname() {
return firstname;
}

public void setFirstname(String firstname) {
this.firstname = firstname;
}

public Object getLastname() {
return lastname;
}

public void setLastname(Object lastname) {
this.lastname = lastname;
}

public String getBusinessName() {
return businessName;
}

public void setBusinessName(String businessName) {
this.businessName = businessName;
}

public String getDisplayPicture() {
return displayPicture;
}

public void setDisplayPicture(String displayPicture) {
this.displayPicture = displayPicture;
}

public String getBusinessEmail() {
return businessEmail;
}

public void setBusinessEmail(String businessEmail) {
this.businessEmail = businessEmail;
}

public String getEmail() {
return email;
}

public void setEmail(String email) {
this.email = email;
}

public String getMobileNumber() {
return mobileNumber;
}

public void setMobileNumber(String mobileNumber) {
this.mobileNumber = mobileNumber;
}

public String getBusinessAddress() {
return businessAddress;
}

public void setBusinessAddress(String businessAddress) {
this.businessAddress = businessAddress;
}

public Object getPassport() {
return passport;
}

public void setPassport(Object passport) {
this.passport = passport;
}

public String getId() {
return id;
}

public void setId(String id) {
this.id = id;
}

}