
package com.ichef.android.responsemodel.orders;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("jsonschema2pojo")
public class Vendor {

    @SerializedName("firstname")
    @Expose
    private String firstname;
    @SerializedName("lastname")
    @Expose
    private Object lastname;
    @SerializedName("email")
    @Expose
    private Object email;
    @SerializedName("mobile_number")
    @Expose
    private String mobileNumber;
    @SerializedName("averageRating")
    @Expose
    private Double averageRating;
    @SerializedName("totalRatingScore")
    @Expose
    private Float totalRatingScore;
    @SerializedName("_id")
    @Expose
    private String id;

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public Object getLastname() {
        return lastname;
    }

    public void setLastname(Object lastname) {
        this.lastname = lastname;
    }

    public Object getEmail() {
        return email;
    }

    public void setEmail(Object email) {
        this.email = email;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public Double getAverageRating() {
        return averageRating;
    }

    public void setAverageRating(Double averageRating) {
        this.averageRating = averageRating;
    }

    public Float getTotalRatingScore() {
        return totalRatingScore;
    }

    public void setTotalRatingScore(Float totalRatingScore) {
        this.totalRatingScore = totalRatingScore;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

}
