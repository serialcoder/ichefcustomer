package com.ichef.android.responsemodel.Address.City;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class State implements Serializable
{

@SerializedName("_id")
@Expose
private String id;
@SerializedName("statename")
@Expose
private String statename;
private final static long serialVersionUID = -8460591380180261969L;

public String getId() {
return id;
}

public void setId(String id) {
this.id = id;
}

public String getStatename() {
return statename;
}

public void setStatename(String statename) {
this.statename = statename;
}

}