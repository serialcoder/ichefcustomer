package com.ichef.android.responsemodel.payment;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Param implements Serializable
{

@SerializedName("response")
@Expose
private Response response;
private final static long serialVersionUID = 5625727065711441851L;

public Response getResponse() {
return response;
}

public void setResponse(Response response) {
this.response = response;
}

}