package com.ichef.android.responsemodel.bookmarklist;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Vendor {
    @SerializedName("business_name")
    @Expose
    private String businessName;
    @SerializedName("display_picture")
    @Expose
    private String displayPicture;
    @SerializedName("averageRating")
    @Expose
    private Float averageRating;
    @SerializedName("_id")
    @Expose
    private String id;

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public String getDisplayPicture() {
        return displayPicture;
    }

    public void setDisplayPicture(String displayPicture) {
        this.displayPicture = displayPicture;
    }

    public Float getAverageRating() {
        return averageRating;
    }

    public void setAverageRating(Float averageRating) {
        this.averageRating = averageRating;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
