package com.ichef.android.responsemodel.search;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Vendor__1 implements Serializable
{

@SerializedName("_id")
@Expose
private String id;
@SerializedName("averageRating")
@Expose
private Double averageRating;
@SerializedName("business_name")
@Expose
private String businessName;
@SerializedName("display_picture")
@Expose
private String displayPicture;
@SerializedName("hours_availability")
@Expose
private String hoursAvailability;
@SerializedName("weekdays_availability")
@Expose
private String weekdaysAvailability;
private final static long serialVersionUID = 5756884790834068560L;

public String getId() {
return id;
}

public void setId(String id) {
this.id = id;
}

public Double getAverageRating() {
return averageRating;
}

public void setAverageRating(Double averageRating) {
this.averageRating = averageRating;
}

public String getBusinessName() {
return businessName;
}

public void setBusinessName(String businessName) {
this.businessName = businessName;
}

public String getDisplayPicture() {
return displayPicture;
}

public void setDisplayPicture(String displayPicture) {
this.displayPicture = displayPicture;
}

public String getHoursAvailability() {
return hoursAvailability;
}

public void setHoursAvailability(String hoursAvailability) {
this.hoursAvailability = hoursAvailability;
}

public String getWeekdaysAvailability() {
return weekdaysAvailability;
}

public void setWeekdaysAvailability(String weekdaysAvailability) {
this.weekdaysAvailability = weekdaysAvailability;
}

}