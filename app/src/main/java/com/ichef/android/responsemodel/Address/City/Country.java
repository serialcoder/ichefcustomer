package com.ichef.android.responsemodel.Address.City;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Country implements Serializable
{

@SerializedName("_id")
@Expose
private String id;
@SerializedName("countryname")
@Expose
private String countryname;
private final static long serialVersionUID = 8535597643172392497L;

public String getId() {
return id;
}

public void setId(String id) {
this.id = id;
}

public String getCountryname() {
return countryname;
}

public void setCountryname(String countryname) {
this.countryname = countryname;
}

}