package com.ichef.android.responsemodel.areyoufeeling;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Result implements Serializable
{

@SerializedName("image")
@Expose
private String image;
@SerializedName("_id")
@Expose
private String id;
@SerializedName("name")
@Expose
private String name;
private final static long serialVersionUID = 9153063682894700218L;

public String getImage() {
return image;
}

public void setImage(String image) {
this.image = image;
}

public String getId() {
return id;
}

public void setId(String id) {
this.id = id;
}

public String getName() {
return name;
}

public void setName(String name) {
this.name = name;
}

}
