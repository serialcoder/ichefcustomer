
package com.ichef.android.responsemodel.orders;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("jsonschema2pojo")
public class Result {

    @SerializedName("address")
    @Expose
    private Address address;
    @SerializedName("cart")
    @Expose
    private List<Cart> cart = null;
    @SerializedName("transaction_id")
    @Expose
    private String transactionId;
    @SerializedName("promo_slash")
    @Expose
    private Double promoSlash;
    @SerializedName("discount_slash")
    @Expose
    private Double discountSlash;
    @SerializedName("dispute")
    @Expose
    private String dispute;
    @SerializedName("review")
    @Expose
    private Review review;
    @SerializedName("order_id")
    @Expose
    private String order_id;
    @SerializedName("reviewed")
    @Expose
    private Boolean reviewed;
    @SerializedName("disputed")
    @Expose
    private Boolean disputed;
    @SerializedName("coupon")
    @Expose
    private String coupon;
    @SerializedName("deleted")
    @Expose
    private Boolean deleted;
    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("vendor")
    @Expose
    private Vendor vendor;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("delivery_type")
    @Expose
    private String deliveryType;
    @SerializedName("delivery_amount")
    @Expose
    private Double deliveryAmount;
    @SerializedName("taxable_amount")
    @Expose
    private Double taxableAmount;
    @SerializedName("total_cart_amount")
    @Expose
    private Double totalCartAmount;
    @SerializedName("cart_amount")
    @Expose
    private Double cartAmount;
    @SerializedName("amount_charged")
    @Expose
    private Double amountCharged;
    @SerializedName("tip")
    @Expose
    private Double tip;
    @SerializedName("customer")
    @Expose
    private Customer customer;
    @SerializedName("orderedAt")
    @Expose
    private String orderedAt;
    @SerializedName("datecreated")
    @Expose
    private String datecreated;
    @SerializedName("lastmodified")
    @Expose
    private String lastmodified;
    @SerializedName("__v")
    @Expose
    private Integer v;

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public List<Cart> getCart() {
        return cart;
    }

    public void setCart(List<Cart> cart) {
        this.cart = cart;
    }

    public Object getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String  transactionId) {
        this.transactionId = transactionId;
    }

    public Double getPromoSlash() {
        return promoSlash;
    }

    public void setPromoSlash(Double promoSlash) {
        this.promoSlash = promoSlash;
    }

    public Double getDiscountSlash() {
        return discountSlash;
    }

    public void setDiscountSlash(Double discountSlash) {
        this.discountSlash = discountSlash;
    }

    public String getDispute() {
        return dispute;
    }

    public void setDispute(String dispute) {
        this.dispute = dispute;
    }

    public Review getReview() {
        return review;
    }

    public void setReview(Review review) {
        this.review = review;
    }

    public Boolean getReviewed() {
        return reviewed;
    }

    public void setReviewed(Boolean reviewed) {
        this.reviewed = reviewed;
    }

    public Boolean getDisputed() {
        return disputed;
    }

    public void setDisputed(Boolean disputed) {
        this.disputed = disputed;
    }

    public String getCoupon() {
        return coupon;
    }

    public void setCoupon(String coupon) {
        this.coupon = coupon;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Vendor getVendor() {
        return vendor;
    }

    public void setVendor(Vendor vendor) {
        this.vendor = vendor;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDeliveryType() {
        return deliveryType;
    }

    public void setDeliveryType(String deliveryType) {
        this.deliveryType = deliveryType;
    }

    public Double getDeliveryAmount() {
        return deliveryAmount;
    }

    public void setDeliveryAmount(Double deliveryAmount) {
        this.deliveryAmount = deliveryAmount;
    }

    public Double getTaxableAmount() {
        return taxableAmount;
    }

    public void setTaxableAmount(Double taxableAmount) {
        this.taxableAmount = taxableAmount;
    }

    public Double getTotalCartAmount() {
        return totalCartAmount;
    }

    public void setTotalCartAmount(Double totalCartAmount) {
        this.totalCartAmount = totalCartAmount;
    }

    public Double getCartAmount() {
        return cartAmount;
    }

    public void setCartAmount(Double cartAmount) {
        this.cartAmount = cartAmount;
    }

    public Double getAmountCharged() {
        return amountCharged;
    }

    public void setAmountCharged(Double amountCharged) {
        this.amountCharged = amountCharged;
    }

    public Double getTip() {
        return tip;
    }

    public void setTip(Double tip) {
        this.tip = tip;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public String getOrderedAt() {
        return orderedAt;
    }

    public void setOrderedAt(String orderedAt) {
        this.orderedAt = orderedAt;
    }

    public String getDatecreated() {
        return datecreated;
    }

    public void setDatecreated(String datecreated) {
        this.datecreated = datecreated;
    }

    public String getLastmodified() {
        return lastmodified;
    }

    public void setLastmodified(String lastmodified) {
        this.lastmodified = lastmodified;
    }

    public Integer getV() {
        return v;
    }

    public void setV(Integer v) {
        this.v = v;
    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }
}
