package com.ichef.android.responsemodel.productlist;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Category implements Serializable
{

@SerializedName("category_name")
@Expose
private String categoryName;
private final static long serialVersionUID = 5972207227513105883L;

public String getCategoryName() {
return categoryName;
}

public void setCategoryName(String categoryName) {
this.categoryName = categoryName;
}

}