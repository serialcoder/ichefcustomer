
package com.ichef.android.responsemodel.otprequest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class Param {

    @SerializedName("token")
    @Expose
    private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
