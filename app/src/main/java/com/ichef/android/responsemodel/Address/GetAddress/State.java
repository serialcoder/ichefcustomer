package com.ichef.android.responsemodel.Address.GetAddress;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import javax.annotation.Generated;

@Generated("jsonschema2pojo")
public class State implements Serializable {

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("statename")
    @Expose
    private String statename;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStatename() {
        return statename;
    }

    public void setStatename(String statename) {
        this.statename = statename;
    }

}
