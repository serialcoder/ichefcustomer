package com.ichef.android.responsemodel.productlist;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class Vendor implements Serializable
{

@SerializedName("location")
@Expose
private Location location;
@SerializedName("_id")
@Expose
private String id;
@SerializedName("averageRating")
@Expose
private Float averageRating;
@SerializedName("business_name")
@Expose
private String businessName;
@SerializedName("business_type")
@Expose
private String businessType;
@SerializedName("display_picture")
@Expose
private String displayPicture;
@SerializedName("available")
@Expose
private Boolean available = true;
/*@SerializedName("hours_availability")
@Expose
private HoursAvailability hoursAvailability;
@SerializedName("weekdays_availability")
@Expose
private WeekdaysAvailability weekdaysAvailability;*/
@SerializedName("categories")
@Expose
private List<Category> categories = null;
private final static long serialVersionUID = 3189430140471501082L;

public Location getLocation() {
return location;
}

public void setLocation(Location location) {
this.location = location;
}

public String getId() {
return id;
}

public void setId(String id) {
this.id = id;
}

public Float getAverageRating() {
return averageRating;
}

public void setAverageRating(Float averageRating) {
this.averageRating = averageRating;
}

public String getBusinessName() {
return businessName;
}

public void setBusinessName(String businessName) {
this.businessName = businessName;
}

public String getBusinessType() {
return businessType;
}

public void setBusinessType(String businessType) {
this.businessType = businessType;
}

public String getDisplayPicture() {
return displayPicture;
}

public void setDisplayPicture(String displayPicture) {
this.displayPicture = displayPicture;
}

/*public HoursAvailability getHoursAvailability() {
return hoursAvailability;
}

public void setHoursAvailability(HoursAvailability hoursAvailability) {
this.hoursAvailability = hoursAvailability;
}

public WeekdaysAvailability getWeekdaysAvailability() {
return weekdaysAvailability;
}

public void setWeekdaysAvailability(WeekdaysAvailability weekdaysAvailability) {
this.weekdaysAvailability = weekdaysAvailability;
}*/

public List<Category> getCategories() {
return categories;
}

public void setCategories(List<Category> categories) {
this.categories = categories;
}

    public Boolean getAvailable() {
        return available;
    }

    public void setAvailable(Boolean available) {
        this.available = available;
    }
}