package com.ichef.android.fragment;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.viewpager.widget.ViewPager;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.messaging.FirebaseMessaging;
import com.ichef.android.R;
import com.ichef.android.activity.HomePageActivity;
import com.ichef.android.activity.SearchActivity;
import com.ichef.android.adapter.FelingAdapter;
import com.ichef.android.adapter.HomeFoodAdapter;
import com.ichef.android.adapter.MoodAdapter;
import com.ichef.android.adapter.SlidingImage_Adapter;
import com.ichef.android.requestmodel.user.DeviceTokenRequest;
import com.ichef.android.responsemodel.areyoufeeling.FeelingListModel;
import com.ichef.android.responsemodel.banner.BannerListModel;
import com.ichef.android.responsemodel.banner.Result;
import com.ichef.android.responsemodel.login.DeviceTokenResponse;
import com.ichef.android.responsemodel.productlist.HomePageListModel;
import com.ichef.android.responsemodel.productlist.Vendor;
import com.ichef.android.responsemodel.regionData.RegionListModel;
import com.ichef.android.retrofit.APIInterface;
import com.ichef.android.retrofit.ApiClient;
import com.ichef.android.utils.Prefrence;
import com.ichef.android.utils.TransparentProgressDialog;
import com.viewpagerindicator.CirclePageIndicator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class HomeFragment extends Fragment implements AdapterView.OnItemSelectedListener {
    RecyclerView rv_MyProjectList,rvMoodlist,rvFeelinglist;
    HomeFoodAdapter rv_MyProjectAdapter;
    MoodAdapter moodADapter;
    FelingAdapter feelingADapter;
    RecyclerView.LayoutManager rv_MyProjectLayoutManager;
    RecyclerView.LayoutManager rvMoodLayoutManager;
    List<Vendor> mListData = new ArrayList<>();
    List<Result> mBannerListData = new ArrayList<>();
    List<com.ichef.android.responsemodel.regionData.Result> mRegionListData = new ArrayList<>();
    List<com.ichef.android.responsemodel.areyoufeeling.Result> mFeelingListData = new ArrayList<>();
    Context mContext;
    LocationManager locationManager;
    RelativeLayout lottiNoFood;
    TextView tvSearch,tvLablMood,tvLablFeel;
    private  ViewPager mPager;
    private  int currentPage = 0;
    private  int NUM_PAGES = 0;
    RelativeLayout relativeViewPager;
    CirclePageIndicator indicator;
    RelativeLayout relPopular,relRating;
    ImageView imgPopular,imgRating;
    TextView tvPopular,tvRating;
    String sortBy = "popular";
    ShimmerFrameLayout shimmerFrameLayout;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        mContext = getActivity();
        locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        init(view);
        returnMeFCMtoken();
        // fetchCurrentLatLong();
        return view;
    }

    public void returnMeFCMtoken() {
        final String[] token = {""};
        FirebaseMessaging.getInstance().getToken().addOnCompleteListener(new OnCompleteListener<String>() {
            @Override
            public void onComplete(@NonNull Task<String> task) {
                if(task.isComplete()){
                    token[0] = task.getResult();
                    Log.e("AppConstants", "onComplete: new Token got: "+token[0] );
                    Prefrence.save(mContext,Prefrence.KEY_DEVICE_TOKEN,token[0]);
                    updateDeviceToken();
                }
            }
        });

    }

    private void updateDeviceToken() {
        DeviceTokenRequest request = new DeviceTokenRequest();
        request.setDevice_token(Prefrence.get(mContext, Prefrence.KEY_DEVICE_TOKEN));
        APIInterface apiInterface = ApiClient.getClient().create(APIInterface.class);
        Call<DeviceTokenResponse> call = apiInterface.UpdateDeviceToken("Bearer " + Prefrence.get(mContext, Prefrence.KEY_TOKEN), request);
        call.enqueue(new Callback<DeviceTokenResponse>() {
            @Override
            public void onResponse(Call<DeviceTokenResponse> call, Response<DeviceTokenResponse> response) {
            }

            @Override
            public void onFailure(Call<DeviceTokenResponse> call, Throwable t) {
                call.cancel();
            }
        });
    }

    private void init(View view) {
        mPager = view.findViewById(R.id.pager);
        relativeViewPager = view.findViewById(R.id.relativeViewPager);
        indicator = view.findViewById(R.id.indicator);
        shimmerFrameLayout = view.findViewById(R.id.shimmerFrameLayout);

        relPopular = view.findViewById(R.id.relPopular);
        relRating = view.findViewById(R.id.relRating);
        imgPopular = view.findViewById(R.id.imgPopular);
        imgRating = view.findViewById(R.id.imgRating);
        tvPopular = view.findViewById(R.id.tvPopular);
        tvRating = view.findViewById(R.id.tvRating);

        tvSearch = view.findViewById(R.id.tvSearch);
        lottiNoFood = view.findViewById(R.id.lottiNoFood);
        rv_MyProjectList = view.findViewById(R.id.rvlist);
        rv_MyProjectList.setHasFixedSize(true);
        rv_MyProjectLayoutManager = new LinearLayoutManager(mContext);
        rv_MyProjectList.setLayoutManager(rv_MyProjectLayoutManager);

        tvLablFeel = view.findViewById(R.id.tvLablFeel);
        tvLablMood = view.findViewById(R.id.tvLablMood);
        rvMoodlist = view.findViewById(R.id.rvMoodlist);
        rvMoodlist.setHasFixedSize(true);
        rvMoodLayoutManager = new LinearLayoutManager(mContext,RecyclerView.HORIZONTAL,false);
        rvMoodlist.setLayoutManager(rvMoodLayoutManager);

        rvFeelinglist = view.findViewById(R.id.rvFeelinglist);
        rvFeelinglist.setHasFixedSize(true);
        rvMoodLayoutManager = new LinearLayoutManager(mContext,RecyclerView.HORIZONTAL,false);
        rvFeelinglist.setLayoutManager(rvMoodLayoutManager);

        tvSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(mContext, SearchActivity.class);
                startActivity(i);
            }
        });

        relPopular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imgPopular.setImageResource(R.drawable.descendant_white);
                tvPopular.setTextColor(mContext.getResources().getColor(R.color.white));
                relPopular.setBackground(mContext.getResources().getDrawable(R.drawable.border_search_red));

                imgRating.setImageResource(R.drawable.descendant);
                tvRating.setTextColor(mContext.getResources().getColor(R.color.black));
                relRating.setBackground(mContext.getResources().getDrawable(R.drawable.border_search));

                sortBy = "popular";
                getNearByVendorList();
            }
        });

        relRating.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imgPopular.setImageResource(R.drawable.descendant);
                tvPopular.setTextColor(mContext.getResources().getColor(R.color.black));
                relPopular.setBackground(mContext.getResources().getDrawable(R.drawable.border_search));

                imgRating.setImageResource(R.drawable.descendant_white);
                tvRating.setTextColor(mContext.getResources().getColor(R.color.white));
                relRating.setBackground(mContext.getResources().getDrawable(R.drawable.border_search_red));
                sortBy = "rating";
                getNearByVendorList();
            }
        });

        getBannerList();
    }



    @Override
    public void onResume() {
        super.onResume();

        shimmerFrameLayout.setVisibility(View.VISIBLE);
        shimmerFrameLayout.startShimmerAnimation();

        getNearByVendorList();
    }

    public void fetchCurrentLatLong() {
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            displayLocationSettingsRequest(mContext);
        }

        if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            // execute when network_provider is enable
            if (locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
                // set requestLocationUpdates with adding time to refresh your location status and distance
                locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 10000, 0, new LocationListener() {
                    @Override
                    public void onLocationChanged(@NonNull Location location) {
                        if (location != null) {
                            Prefrence.save(mContext, Prefrence.KEY_LATITUDE, String.valueOf(location.getLatitude()));
                            Prefrence.save(mContext, Prefrence.KEY_LONGITUDE, String.valueOf(location.getLongitude()));
                        }
                    }
                });
            }
            // execute when Gps_provider is enable
            else if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                // set requestLocationUpdates with adding time to refresh your location status and distance
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 10000, 0, new LocationListener() {
                    @Override
                    public void onLocationChanged(@NonNull Location location) {
                        if (location != null) {
                            Prefrence.save(mContext, Prefrence.KEY_LATITUDE, String.valueOf(location.getLatitude()));
                            Prefrence.save(mContext, Prefrence.KEY_LONGITUDE, String.valueOf(location.getLongitude()));
                        }
                    }
                });
            } else {
            }
        }
    }

    private void getBannerList() {

        APIInterface apiInterface = ApiClient.getClient().create(APIInterface.class);
        Call<BannerListModel> resultCall = apiInterface.CallBannerList();
        //Call<HomePageListModel> resultCall = apiInterface.CallProductList(token,"7.448166400000001","9.0570752");

        resultCall.enqueue(new Callback<BannerListModel>() {
            @Override
            public void onResponse(Call<BannerListModel> call, Response<BannerListModel> response) {
                getMoodList();
                if (response.body().getStatus()) {

                    mBannerListData = response.body().getResult();
                    if (mBannerListData.size() == 0) {
                        relativeViewPager.setVisibility(View.GONE);
                    } else {
                        relativeViewPager.setVisibility(View.VISIBLE);
                        ArrayList<String> image_list = new ArrayList<>();
                        if(mBannerListData != null && mBannerListData.size()>0){
                            for (int i=0;i<mBannerListData.size();i++){
                                image_list.add(mBannerListData.get(i).getImage());
                            }
                            mPager.setAdapter(new SlidingImage_Adapter(mContext, image_list));
                            indicator.setViewPager(mPager);
                            final float density = mContext.getResources().getDisplayMetrics().density;
                            indicator.setRadius(5 * density);

                            NUM_PAGES = image_list.size();

                            // Auto start of viewpager
                            if (image_list.size() > 0) {
                                final Handler handler = new Handler();
                                final Runnable Update = new Runnable() {
                                    public void run() {
                                        if (currentPage == NUM_PAGES) {
                                            currentPage = 0;
                                        }
                                        mPager.setCurrentItem(currentPage++, true);
                                    }
                                };

                                Timer swipeTimer = new Timer();
                                swipeTimer.schedule(new TimerTask() {
                                    @Override
                                    public void run() {
                                        handler.post(Update);
                                    }
                                }, 5000, 4000);

                                indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                                    @Override
                                    public void onPageSelected(int position) {
                                        currentPage = position;
                                    }

                                    @Override
                                    public void onPageScrolled(int pos, float arg1, int arg2) {
                                    }

                                    @Override
                                    public void onPageScrollStateChanged(int pos) {
                                    }
                                });
                            }
                        }
                    }
                } else {
                    lottiNoFood.setVisibility(View.VISIBLE);
                    rv_MyProjectList.setVisibility(View.GONE);
                    Toast.makeText(mContext, "No vendor found at this location!", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<BannerListModel> call, Throwable t) {
                getMoodList();

                lottiNoFood.setVisibility(View.VISIBLE);
                rv_MyProjectList.setVisibility(View.GONE);
                call.cancel();
            }
        });
    }
    private void getNearByVendorList() {

        String token = "Bearer " + Prefrence.get(mContext, Prefrence.KEY_TOKEN);

        APIInterface apiInterface = ApiClient.getClient().create(APIInterface.class);
        Call<HomePageListModel> resultCall = apiInterface.CallProductList(token,sortBy,Prefrence.get(mContext, Prefrence.KEY_LATITUDE), Prefrence.get(mContext, Prefrence.KEY_LONGITUDE));
        //Call<HomePageListModel> resultCall = apiInterface.CallProductList(token,"7.448166400000001","9.0570752");

        resultCall.enqueue(new Callback<HomePageListModel>() {
            @Override
            public void onResponse(Call<HomePageListModel> call, Response<HomePageListModel> response) {

                if (response != null && response.body() != null && response.body().getStatus()) {
                    shimmerFrameLayout.stopShimmerAnimation();
                    shimmerFrameLayout.setVisibility(View.GONE);
                    mListData = response.body().getResult().getVendors();
                    if (mListData.size() == 0) {
                        lottiNoFood.setVisibility(View.VISIBLE);
                        rv_MyProjectList.setVisibility(View.GONE);
                    } else {
                        lottiNoFood.setVisibility(View.GONE);
                        rv_MyProjectList.setVisibility(View.VISIBLE);
                    }
                    setProduct();
                } else {
                    lottiNoFood.setVisibility(View.VISIBLE);
                    rv_MyProjectList.setVisibility(View.GONE);
                    Toast.makeText(mContext, "No vendor found at this location!", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<HomePageListModel> call, Throwable t) {

                lottiNoFood.setVisibility(View.VISIBLE);
                rv_MyProjectList.setVisibility(View.GONE);
                shimmerFrameLayout.stopShimmerAnimation();
                shimmerFrameLayout.setVisibility(View.GONE);
                call.cancel();
            }
        });
    }

    private void getMoodList() {
        APIInterface apiInterface = ApiClient.getClient().create(APIInterface.class);
        Call<RegionListModel> resultCall = apiInterface.CallRegionList();
        //Call<HomePageListModel> resultCall = apiInterface.CallProductList(token,"7.448166400000001","9.0570752");

        resultCall.enqueue(new Callback<RegionListModel>() {
            @Override
            public void onResponse(Call<RegionListModel> call, Response<RegionListModel> response) {
                getFeelingList();

                if (response.body().getStatus()) {
                    mRegionListData = response.body().getResult();
                    if (mRegionListData.size() == 0) {
                        tvLablMood.setVisibility(View.GONE);
                        rvMoodlist.setVisibility(View.GONE);
                    } else {
                        tvLablMood.setVisibility(View.VISIBLE);
                        rvMoodlist.setVisibility(View.VISIBLE);
                        if (mRegionListData != null && mRegionListData.size() > 0) {
                            moodADapter = new MoodAdapter(getContext(), (ArrayList<com.ichef.android.responsemodel.regionData.Result>) mRegionListData);
                            rvMoodlist.setAdapter(moodADapter);
                        }
                    }

                } else {
                    tvLablMood.setVisibility(View.GONE);
                    rvMoodlist.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<RegionListModel> call, Throwable t) {
                getFeelingList();
                tvLablMood.setVisibility(View.GONE);
                rvMoodlist.setVisibility(View.GONE);
                call.cancel();
            }
        });
    }


    private void getFeelingList() {
        String token = "Bearer " + Prefrence.get(mContext, Prefrence.KEY_TOKEN);

        APIInterface apiInterface = ApiClient.getClient().create(APIInterface.class);
        Call<FeelingListModel> resultCall = apiInterface.CallAreyouFeelingList(token);
        //Call<HomePageListModel> resultCall = apiInterface.CallProductList(token,"7.448166400000001","9.0570752");

        resultCall.enqueue(new Callback<FeelingListModel>() {
            @Override
            public void onResponse(Call<FeelingListModel> call, Response<FeelingListModel> response) {

                if (response.body().getStatus()) {
                    mFeelingListData = response.body().getResult();
                    if (mFeelingListData.size() == 0) {
                        tvLablFeel.setVisibility(View.GONE);
                        rvFeelinglist.setVisibility(View.GONE);
                    } else {
                        tvLablFeel.setVisibility(View.VISIBLE);
                        rvFeelinglist.setVisibility(View.VISIBLE);
                        if (mFeelingListData != null && mFeelingListData.size() > 0) {
                            feelingADapter = new FelingAdapter(mContext, (ArrayList<com.ichef.android.responsemodel.areyoufeeling.Result>) mFeelingListData);
                            rvFeelinglist.setAdapter(feelingADapter);
                        }
                    }

                } else {
                    tvLablFeel.setVisibility(View.GONE);
                    rvFeelinglist.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<FeelingListModel> call, Throwable t) {
                tvLablFeel.setVisibility(View.GONE);
                rvFeelinglist.setVisibility(View.GONE);
                call.cancel();
            }
        });
    }

    private void setProduct() {
        if (mListData != null && mListData.size() > 0) {

            rv_MyProjectAdapter = new HomeFoodAdapter(mContext, (ArrayList<Vendor>) mListData);
            rv_MyProjectList.setAdapter(rv_MyProjectAdapter);
        }

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        ((TextView) parent.getChildAt(0)).setTextColor(Color.BLACK);
        ((TextView) parent.getChildAt(0)).setTextSize(12);
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    private void displayLocationSettingsRequest(Context context) {
        GoogleApiClient googleApiClient = new GoogleApiClient.Builder(context)
                .addApi(LocationServices.API).build();
        googleApiClient.connect();

        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(10000 / 2);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);
        builder.setAlwaysShow(true);

        PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        Log.i("SHUBHAM", "All location settings are satisfied.");
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        Log.i("SHUBHAM", "Location settings are not satisfied. Show the user a dialog to upgrade location settings ");

                        try {
                            // Show the dialog by calling startResolutionForResult(), and check the result
                            // in onActivityResult().
                            status.startResolutionForResult(((HomePageActivity) getActivity()).getActivityObject(), 101);
                        } catch (IntentSender.SendIntentException e) {
                            Log.i("SHUBHAM", "PendingIntent unable to execute request.");
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        Log.i("SHUBHAM", "Location settings are inadequate, and cannot be fixed here. Dialog not created.");
                        break;
                }
            }
        });
    }

}