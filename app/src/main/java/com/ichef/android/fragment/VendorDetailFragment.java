package com.ichef.android.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ichef.android.R;
import com.ichef.android.adapter.NotificationProfileAdapter;
import com.ichef.android.responsemodel.vendordetail.Product;
import com.ichef.android.utils.TransparentProgressDialog;

import java.util.ArrayList;
import java.util.List;

public class VendorDetailFragment extends Fragment {


    String token, vendorId;
    List<Product> productlist = new ArrayList<>();
    NotificationProfileAdapter.VendorDetailAdapter adapter;
    TransparentProgressDialog dialog;
    RecyclerView recyclerView;

    public static VendorDetailFragment newInstance() {
        VendorDetailFragment fragment = new VendorDetailFragment();
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            productlist = getArguments().getParcelableArrayList("productList");
            vendorId = getArguments().getString("vendorId");
            int pos = getArguments().getInt("position");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_vendor_detail, container, false);
        init(view);
        setProduct();
        onclick();
        return view;
    }

    private void onclick() {

    }

    private void init(View view) {
        recyclerView = (RecyclerView) view.findViewById(R.id.rvlistdynamic);
    }


    private void setProduct() {
        if (productlist != null && productlist.size() > 0) {
            adapter = new NotificationProfileAdapter.VendorDetailAdapter(getContext(), (ArrayList<Product>) productlist, vendorId);
            recyclerView.setHasFixedSize(true);
            recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
            recyclerView.setAdapter(adapter);
        }

    }

}