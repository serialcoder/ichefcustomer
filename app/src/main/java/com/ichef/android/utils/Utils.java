package com.ichef.android.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.time.Instant;

import static java.lang.Math.acos;
import static java.lang.Math.cos;
import static java.lang.Math.sin;


public class Utils {

    public static double distance(double lat1, double long1, double lat2, double long2) {
        System.out.println("SHUBHAM--"+lat1+"--"+long1+"--"+lat2+"--"+long2);
        if (lat1== 0.0 || long1== 0.0 || lat2== 0.0 || long2== 0.0)
        {
            return 0.0;
        }

        double PI_RAD = Math.PI / 180.0;
        double phi1 = lat1 * PI_RAD;
        double phi2 = lat2 * PI_RAD;
        double lam1 = long1 * PI_RAD;
        double lam2 = long2 * PI_RAD;

        return 6371.01 * acos(sin(phi1) * sin(phi2) + cos(phi1) * cos(phi2) * cos(lam2 - lam1));
    }

    public static double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }

    public static double rad2deg(double rad) {
        return (rad * 180.0 / Math.PI);
    }


    public static String formatDate(String sourcedatevalue){
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.sss'Z'");
        Date sourceDate = null;
        try {
            sourceDate = dateFormat.parse(sourcedatevalue);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        SimpleDateFormat targetFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm");
        String targetdatevalue= targetFormat.format(sourceDate);
        return targetdatevalue;
    }
}
