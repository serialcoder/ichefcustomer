package com.ichef.android.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.ichef.android.R;
import com.ichef.android.activity.VendorListActivity;
import com.ichef.android.responsemodel.regionData.Result;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;


public class FelingAdapter extends RecyclerView.Adapter<FelingAdapter.ViewHolder> {
    private Context ctx;
    private List<com.ichef.android.responsemodel.areyoufeeling.Result> mlist;
    private ArrayList<com.ichef.android.responsemodel.areyoufeeling.Result> slist;

    public FelingAdapter(Context context, ArrayList<com.ichef.android.responsemodel.areyoufeeling.Result> list) {
        mlist = list;
        ctx = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.list_mood, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        holder.tvRegionName.setText(mlist.get(position).getName());
        if(mlist.get(position).getImage() != null && ! mlist.get(position).getImage().equalsIgnoreCase("")){
            Picasso.get().load(mlist.get(position).getImage())
                    .placeholder(R.drawable.ic__placeholder_new)
                    .into(holder.foodimg);
        }
        holder.llMaster.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(ctx, VendorListActivity.class);
                intent.putExtra("name",mlist.get(position).getName());
                intent.putExtra("type","eattime");
                intent.putExtra("id",mlist.get(position).getId());
                ctx.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return mlist.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tvRegionName;
        ImageView foodimg;
        LinearLayout llMaster;

        public ViewHolder(View itemView) {
            super(itemView);
            this.tvRegionName = itemView.findViewById(R.id.tvRegionName);
            this.foodimg = itemView.findViewById(R.id.imgFood);
            this.llMaster = itemView.findViewById(R.id.llMaster);
        }
    }
}
