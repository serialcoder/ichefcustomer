package com.ichef.android.adapter;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.ichef.android.R;
import com.ichef.android.activity.EditAddress;
import com.ichef.android.responsemodel.Address.GetAddress.Address;
import com.ichef.android.utils.Prefrence;

import java.util.ArrayList;
import java.util.List;


public class SelectAddressAdapter extends RecyclerView.Adapter<SelectAddressAdapter.ViewHolder> {
    String id;
    private Activity ctx;
    private List<Address> mlist;
    private ArrayList<Address> slist;

    public SelectAddressAdapter(Activity context, ArrayList<Address> list) {
        mlist = list;
        ctx = context;
        this.slist = new ArrayList<Address>();
        this.slist.addAll(mlist);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.list_select_address, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        // holder.drivername.setText(mlist.get(position).getFirstName()+" "+mlist.get(position).getLastName());
        holder.name.setText(mlist.get(position).getName());
        holder.locationaddress.setText(mlist.get(position).toString());
        holder.phone.setText(mlist.get(position).getName() + ", " + mlist.get(position).getMobile());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Intent intent = new Intent(ctx, EditAddress.class);
                //ctx.startActivity(intent);
                Prefrence.save(ctx, Prefrence.KEY_DELIVERY_LATITUDE, String.valueOf(mlist.get(position).getLatitude()));
                Prefrence.save(ctx, Prefrence.KEY_DELIVERY_LONGITUDE, String.valueOf(mlist.get(position).getLongitude()));
                Prefrence.save(ctx, Prefrence.KEY_DELIVERY_CITYNAME, String.valueOf(mlist.get(position).toString()));
                Prefrence.save(ctx, Prefrence.KEY_DELIVERY_NAME, String.valueOf(mlist.get(position).getName()));
                Prefrence.save(ctx, Prefrence.KEY_DELIVERY_MOBILE_NO, String.valueOf(mlist.get(position).getMobile()));
                ctx.finish();

            }
        });
        holder.edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ctx, EditAddress.class);
                intent.putExtra("Address", mlist.get(position));
                ctx.startActivity(intent);
            }
        });


    }

    @Override
    public int getItemCount() {
        return mlist.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView name, locationaddress, phone;
        ImageView edit;

        public ViewHolder(View itemView) {
            super(itemView);
            this.name = (TextView) itemView.findViewById(R.id.nameaddress);
            this.locationaddress = (TextView) itemView.findViewById(R.id.locationaddress);
            this.phone = (TextView) itemView.findViewById(R.id.phoneno);
            this.edit = (ImageView) itemView.findViewById(R.id.edit);
        }
    }
}
