package com.ichef.android.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.ichef.android.R;
import com.ichef.android.activity.FoodDetail;
import com.ichef.android.activity.MyCart;
import com.ichef.android.activity.ProductDetailActivity;
import com.ichef.android.requestmodel.Cart.AddToCartRequest;
import com.ichef.android.requestmodel.Cart.ReduceCartRequest;
import com.ichef.android.responsemodel.Address.AddAddressResponse;
import com.ichef.android.responsemodel.cartmodel.Cart;
import com.ichef.android.responsemodel.cartmodel.FetchCartResponse;
import com.ichef.android.responsemodel.cartmodel.Result;
import com.ichef.android.retrofit.APIInterface;
import com.ichef.android.retrofit.ApiClient;
import com.ichef.android.utils.CommonUtility;
import com.ichef.android.utils.Prefrence;
import com.ichef.android.utils.TransparentProgressDialog;


import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;


public class MyCartAdapter extends RecyclerView.Adapter<MyCartAdapter.ViewHolder> {
    private Context ctx;
    private List<Cart> mlist;
    private ArrayList<Cart> slist;

    String id;
    private String vendorId;

    public MyCartAdapter(Context context, ArrayList<Cart> list,String vendorId) {
        mlist = list;
        ctx = context;
        this.slist = new ArrayList<Cart>();
        this.slist.addAll(mlist);
        this.vendorId=vendorId;
    }

    @Override
     public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem= layoutInflater.inflate(R.layout.list_cart, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
        }

@Override
public void onBindViewHolder(ViewHolder holder, int position) {

    // holder.drivername.setText(mlist.get(position).getFirstName()+" "+mlist.get(position).getLastName());
    if(mlist.get(position).getFoodItem() != null){
        holder.name.setText(mlist.get(position).getFoodItem().getFoodItemName());
    }else{
        holder.name.setText("NA");
    }
     holder.tvUnit.setText(mlist.get(position).getUnit().getName()+"");
     holder.tvQuantity.setText(mlist.get(position).getQuantity()+"");
     holder.amount.setText(String.valueOf(mlist.get(position).getPrice()));
     id =mlist.get(position).getId();

    holder.itemView.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent=new Intent(ctx, ProductDetailActivity.class);
            intent.putExtra("ProductId",mlist.get(position).getFoodItem().getId());
            intent.putExtra("VendorId",vendorId);
            ctx.startActivity(intent);
        }
    });

    holder.minus.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            removeCartApiCall(position,holder.tvQuantity,holder.add,holder.minus);
        }
    });
    
    holder.add.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            addCartApiCall(position,holder.tvQuantity,holder.add,holder.minus);
        }
    });


    }

    @Override
    public int getItemCount() {
        return mlist.size();
    }



    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView name,amount,tvQuantity,tvUnit;
        ImageView minus,add;
        public ViewHolder(View itemView) {
            super(itemView);

            this.name = (TextView) itemView.findViewById(R.id.namecartitem);
            this.tvQuantity = (TextView) itemView.findViewById(R.id.tvQuantity);
            this.amount = (TextView) itemView.findViewById(R.id.amountcart);
            this.minus = itemView.findViewById(R.id.minus);
            this.add = itemView.findViewById(R.id.add);
            this.tvUnit = itemView.findViewById(R.id.tvUnit);
        }
    }


    private void removeCartApiCall(int pos,TextView txt, ImageView imgAdd, ImageView imgMinus) {
        String token = Prefrence.get(ctx, Prefrence.KEY_TOKEN);
        ReduceCartRequest request = new ReduceCartRequest();
        request.setUserID(Prefrence.get(ctx, Prefrence.KEY_USER_ID));
        request.setFoodItem(mlist.get(pos).getFoodItem().getId());
        request.setUnit(mlist.get(pos).getUnit().getId());

        APIInterface apiInterface = ApiClient.getClient().create(APIInterface.class);
        Call<FetchCartResponse> resultCall = apiInterface.CallReduceCart("Bearer " + token, request);
        resultCall.enqueue(new Callback<FetchCartResponse>() {

            @Override
            public void onResponse(Call<FetchCartResponse> call, retrofit2.Response<FetchCartResponse> response) {
                if (response.body().getStatus()) {
                    mlist = response.body().getResult().getCart();
                    Toast.makeText(ctx, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    //CommonUtility.saveCartDetails(ctx, response.body().getResult());
                    ((MyCart)ctx).getlist();
                } else {
                    //Toast.makeText(ctx, "Please try again", Toast.LENGTH_SHORT).show();
                    Prefrence.saveInt(ctx, Prefrence.KEY_ITEM_COUNT, 0);
                    Prefrence.save(ctx, Prefrence.KEY_TOTAL, "0");
                    mlist = new ArrayList<>();
                    notifyDataSetChanged();
                }
                ((MyCart) ctx).updateCartItem(response.body().getResult(),response.body().getStatus());
            }

            @Override
            public void onFailure(Call<FetchCartResponse> call, Throwable t) {
                Toast.makeText(ctx, "Please check your Internet Connection", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void addCartApiCall(int pos, TextView txt, ImageView imgAdd, ImageView imgMinus) {
        String token = Prefrence.get(ctx, Prefrence.KEY_TOKEN);
        AddToCartRequest request = new AddToCartRequest();
        request.setUserID(Prefrence.get(ctx, Prefrence.KEY_USER_ID));
        request.setFoodItem(mlist.get(pos).getFoodItem().getId());
        request.setRestaurantId(vendorId);
        request.setPrice(mlist.get(pos).getPrice().toString());
        request.setUnit(mlist.get(pos).getUnit().getId());
        request.setFoodItemName(mlist.get(pos).getFoodItem().getFoodItemName());
        APIInterface apiInterface = ApiClient.getClient().create(APIInterface.class);
        Call<FetchCartResponse> resultCall = apiInterface.CallAddToCart("Bearer " + token, request);
        resultCall.enqueue(new Callback<FetchCartResponse>() {

            @Override
            public void onResponse(Call<FetchCartResponse> call, retrofit2.Response<FetchCartResponse> response) {
                //  Toast.makeText(FoodDetail.this, "Hello"+response, Toast.LENGTH_SHORT).show();

                if (response.body().getStatus()) {
                    mlist = response.body().getResult().getCart();
                    Toast.makeText(ctx, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    ((MyCart)ctx).getlist();
                    //CommonUtility.saveCartDetails(ctx, response.body().getResult());
                } else {
                    //Toast.makeText(ctx, "Cart is empty, Please add item first", Toast.LENGTH_LONG).show();
                    Prefrence.saveInt(ctx, Prefrence.KEY_ITEM_COUNT, 0);
                    Prefrence.save(ctx, Prefrence.KEY_TOTAL, "0");
                    mlist = new ArrayList<>();
                    notifyDataSetChanged();
                }
                ((MyCart) ctx).updateCartItem(response.body().getResult(),response.body().getStatus());
            }

            @Override
            public void onFailure(Call<FetchCartResponse> call, Throwable t) {
                Toast.makeText(ctx, "Please check your Internet Connection", Toast.LENGTH_SHORT).show();
            }
        });

    }


}
