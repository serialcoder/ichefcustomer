package com.ichef.android.adapter;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.ichef.android.activity.VendorDetailActivity;
import com.ichef.android.fragment.VendorDetailFragment;
import com.ichef.android.responsemodel.vendordetail.Menu;
import com.ichef.android.responsemodel.vendordetail.Product;

import java.util.ArrayList;
import java.util.List;


public class VendorDetailFragmentAdapter extends FragmentStatePagerAdapter {
    List<Menu> menuList = new ArrayList<>();
    VendorDetailActivity dynamicActivity;
    String vendorId;
    private int mNumOfTabs;

    public VendorDetailFragmentAdapter(@NonNull FragmentManager fm, int mNumOfTabs, List<Menu> menuList, VendorDetailActivity dynamicActivity, String vendorId) {
        super(fm);
        this.mNumOfTabs = mNumOfTabs;
        this.menuList = menuList;
        this.dynamicActivity = dynamicActivity;
        this.vendorId = vendorId;
    }

    // get the current item with position number
    @Override
    public Fragment getItem(int position) {
        Bundle b = new Bundle();
        b.putInt("position", position);
        b.putString("vendorId", vendorId);
        b.putParcelableArrayList("productList", (ArrayList<Product>) menuList.get(position).getProducts());
        Fragment frag = VendorDetailFragment.newInstance();
        frag.setArguments(b);
        return frag;
    }

    // get total number of tabs
    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}
