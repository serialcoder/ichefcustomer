package com.ichef.android.adapter;

import android.content.Context;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.viewpager.widget.PagerAdapter;

import com.ichef.android.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class SlidingImage_Adapter extends PagerAdapter {

    List<String> image;
    private LayoutInflater inflater;
    private Context context;
    public SlidingImage_Adapter(Context context, List<String> IMAGES) {
        this.context = context;
        this.image=IMAGES;
        inflater = LayoutInflater.from(context);
    }
    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }
    @Override
    public int getCount() {
        return image.size();
    }
    @Override
    public Object instantiateItem(ViewGroup view, int position) {
        View imageLayout = inflater.inflate(R.layout.slidingimages_layout, view, false);
        assert imageLayout != null;
        final ImageView imageView = (ImageView) imageLayout
                .findViewById(R.id.image);
        if (image.get(position).isEmpty()) {
            imageView.setImageResource(R.drawable.ic__placeholder_new);
        } else{
            Picasso.get().load(image.get(position))
                    .placeholder(R.drawable.ic__placeholder_new)
                    .into(imageView);
        }

        // imageView.setImageResource(image.get(position));
        view.addView(imageLayout, 0);
        return imageLayout;
    }
    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }
    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {
    }
    @Override
    public Parcelable saveState() {
        return null;
    }
}
