package com.ichef.android.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.ichef.android.R;
import com.ichef.android.activity.ProductDetailActivity;
import com.ichef.android.responsemodel.search.Product;

import java.util.ArrayList;
import java.util.List;


public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ViewHolder> {
    private Context ctx;
    private List<Product> mlist;
    private ArrayList<Product> slist;

    public ProductAdapter(Context context, ArrayList<Product> list) {
        mlist = list;
        ctx = context;
        this.slist = new ArrayList<Product>();
        this.slist.addAll(mlist);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.list_product, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        holder.tvFoodname.setText(mlist.get(position).getFoodItemName());
        holder.tvVendorName.setText(mlist.get(position).getVendor().getBusinessName());
        if (mlist.get(position).getUnitPrice() != null && mlist.get(position).getUnitPrice().size() > 0) {
            holder.tvPrice.setText(ctx.getResources().getString(R.string.nigiriacurrency) + "" + mlist.get(position).getUnitPrice().get(0).getPrice()+"/"+mlist.get(position).getUnitPrice().get(0).getUnitName());
        } else {
            holder.tvPrice.setText("NA");
        }
        holder.cardMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(ctx, ProductDetailActivity.class);
                intent.putExtra("ProductId",mlist.get(position).getId());
                intent.putExtra("VendorId",mlist.get(position).getVendor().getId());
                intent.putExtra("VendorName",mlist.get(position).getVendor().getBusinessName());
                ctx.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return mlist.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tvFoodname, tvVendorName, tvPrice;
        ImageView foodimg;
        CardView cardMain;

        public ViewHolder(View itemView) {
            super(itemView);
            this.tvFoodname = itemView.findViewById(R.id.tvFoodname);
            this.tvVendorName = itemView.findViewById(R.id.tvVendorName);
            this.tvPrice = itemView.findViewById(R.id.tvPrice);
            this.foodimg = itemView.findViewById(R.id.imgFood);
            this.cardMain = itemView.findViewById(R.id.cardMain);
        }
    }
}
