package com.ichef.android.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.ichef.android.R;
import com.ichef.android.activity.ProductDetailActivity;
import com.ichef.android.responsemodel.notifications.Result;
import com.ichef.android.responsemodel.vendordetail.Product;
import com.ichef.android.responsemodel.vendordetail.UnitPrice;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;


public class NotificationProfileAdapter extends RecyclerView.Adapter<NotificationProfileAdapter.ViewHolder> {
    private Context ctx;
    private List<Result> mlist;
    private ArrayList<Result> slist;

    String id;

    public NotificationProfileAdapter(Context context, ArrayList<Result> list) {
        mlist = list;
        ctx = context;
        this.slist = new ArrayList<Result>();
        this.slist.addAll(mlist);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.list_notificationprofile, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Result result=mlist.get(position);
        holder.tvTitle.setText(result.getTitle());
        holder.tvDescription.setText(result.getDescription());
        try {
            String strCurrentDate = result.getDatecreated();
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
            format.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date newDate = format.parse(strCurrentDate);
            format = new SimpleDateFormat("MMM dd,yyyy hh:mm a");
            String date = format.format(newDate);
            holder.tvTime.setText(date);//2021-06-16T12:50:51.895Z
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return mlist.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tvTitle, tvDescription, tvTime;


        public ViewHolder(View itemView) {
            super(itemView);
            this.tvTitle = (TextView) itemView.findViewById(R.id.tvTitle);
            this.tvDescription = (TextView) itemView.findViewById(R.id.tvDescription);
            this.tvTime = (TextView) itemView.findViewById(R.id.tvTime);
        }
    }

    public static class VendorDetailAdapter extends RecyclerView.Adapter<VendorDetailAdapter.ViewHolder>{

        private Context ctx;
        List<Product> productlist = new ArrayList<>();
        List<UnitPrice> unitPrices = new ArrayList<>();
        String vendorId="";

        public VendorDetailAdapter(Context ctx, List<Product> productlist,String vendorId) {
            this.ctx = ctx;
            this.productlist = productlist;
            this.vendorId=vendorId;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View listItem= layoutInflater.inflate(R.layout.list_vendor, parent, false);
            ViewHolder viewHolder = new ViewHolder(listItem);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position)
        {
            String id= productlist.get(position).getId();
            holder.heading.setText(productlist.get(position).getFoodItemName());
            holder.subheading.setText(productlist.get(position).getDescription());
            unitPrices=productlist.get(position).getUnitPrice();
            if(unitPrices.size()>0) {
                String price = "₦ " + String.valueOf(unitPrices.get(0).getPrice())+"/"+unitPrices.get(0).getUnitName();
                holder.price.setText(price);
            }else{
                holder.price.setText(ctx.getString(R.string.na));
            }
            holder.llMain.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent=new Intent(ctx, ProductDetailActivity.class);
                    intent.putExtra("ProductId",productlist.get(position).getId());
                    intent.putExtra("VendorId",vendorId);
                    ctx.startActivity(intent);
                }
            });

            if(productlist.get(position).getPhotos() != null && productlist.get(position).getPhotos().size()>0){
                Picasso.get().load(productlist.get(position).getPhotos().get(0).getUrl())
                        .placeholder(R.drawable.ic__placeholder_new)
                        .into(holder.imgFood);
            }
        }


        @Override
        public int getItemCount() {
            return productlist.size();
        }

        public static class ViewHolder extends RecyclerView.ViewHolder {
            public TextView heading,subheading,price;
            LinearLayout llMain;
            ImageView imgFood;


            public ViewHolder(View itemView) {
                super(itemView);
                this.imgFood = itemView.findViewById(R.id.imgFood);
                this.heading = itemView.findViewById(R.id.tv_heading);
                this.subheading= itemView.findViewById(R.id.tv_subheading);
                this.price = itemView.findViewById(R.id.tv_price);
                this.llMain = itemView.findViewById(R.id.llMain);

            }
        }
    }
}
