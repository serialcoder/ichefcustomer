package com.ichef.android.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.ichef.android.R;
import com.ichef.android.activity.EditAddress;
import com.ichef.android.responsemodel.Address.AddAddressResponse;
import com.ichef.android.responsemodel.Address.GetAddress.Address;
import com.ichef.android.retrofit.APIInterface;
import com.ichef.android.retrofit.ApiClient;
import com.ichef.android.utils.Prefrence;
import com.ichef.android.utils.TransparentProgressDialog;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;


public class MyAddressAdapter extends RecyclerView.Adapter<MyAddressAdapter.ViewHolder> {
    String id;
    private Context ctx;
    private List<Address> mlist;
    private ArrayList<Address> slist;

    public MyAddressAdapter(Context context, ArrayList<Address> list) {
        mlist = list;
        ctx = context;
        this.slist = new ArrayList<Address>();
        this.slist.addAll(mlist);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.list_address, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        holder.name.setText(mlist.get(position).getName());
        holder.locationaddress.setText(mlist.get(position).toString());
        holder.phone.setText(mlist.get(position).getName() + ", " + mlist.get(position).getMobile());
        holder.edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ctx, EditAddress.class);
                intent.putExtra("Address", mlist.get(position));
                ctx.startActivity(intent);
            }
        });
        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(ctx);
                alertDialog.setMessage("Are you sure you want to remove this?");
                alertDialog.setIcon(android.R.drawable.ic_menu_close_clear_cancel);
                alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        removeAddressApiCall(position);
                    }
                });

                // Setting Negative "NO" Button
                alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // Write your code here to invoke NO event
                        dialog.dismiss();
                        dialog.cancel();
                    }
                });
                // Showing Alert Message
                alertDialog.show();
            }
        });
    }

    private void removeAddressApiCall(int position) {
        TransparentProgressDialog dialog = new TransparentProgressDialog(ctx);
        dialog.show();
        String token = Prefrence.get(ctx, Prefrence.KEY_TOKEN);
        APIInterface apiInterface = ApiClient.getClient().create(APIInterface.class);
        Call<AddAddressResponse> resultCall = apiInterface.CallDeleteAddress("Bearer " + token, mlist.get(position).getId());
        resultCall.enqueue(new Callback<AddAddressResponse>() {

            @Override
            public void onResponse(Call<AddAddressResponse> call, retrofit2.Response<AddAddressResponse> response) {
                if (response.body().getStatus()) {
                    Toast.makeText(ctx, "Address deleted successfully", Toast.LENGTH_SHORT).show();
                    mlist.remove(position);
                    notifyItemRemoved(position);
                } else {
                    Toast.makeText(ctx, "Something went wrong please try again", Toast.LENGTH_SHORT).show();
                }
                dialog.dismiss();
            }

            @Override
            public void onFailure(Call<AddAddressResponse> call, Throwable t) {
                Toast.makeText(ctx, "Please check your Internet Connection", Toast.LENGTH_SHORT).show();
                dialog.dismiss();
            }
        });
    }


    @Override
    public int getItemCount() {
        return mlist.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView name, locationaddress, phone;
        ImageView edit, delete;

        public ViewHolder(View itemView) {
            super(itemView);

            this.name = (TextView) itemView.findViewById(R.id.nameaddress);
            this.locationaddress = (TextView) itemView.findViewById(R.id.locationaddress);
            this.phone = (TextView) itemView.findViewById(R.id.phoneno);
            this.edit = (ImageView) itemView.findViewById(R.id.edit);
            this.delete = (ImageView) itemView.findViewById(R.id.delete);
        }
    }
}
