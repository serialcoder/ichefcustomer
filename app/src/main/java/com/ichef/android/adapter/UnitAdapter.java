package com.ichef.android.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.ichef.android.R;
import com.ichef.android.activity.MyCart;
import com.ichef.android.activity.ProductDetailActivity;
import com.ichef.android.requestmodel.Cart.AddToCartRequest;
import com.ichef.android.requestmodel.Cart.ReduceCartRequest;
import com.ichef.android.responsemodel.cartmodel.Cart;
import com.ichef.android.responsemodel.cartmodel.FetchCartResponse;
import com.ichef.android.responsemodel.cartmodel.Result;
import com.ichef.android.responsemodel.productdetail.UnitPrice;
import com.ichef.android.responsemodel.productdetail.Vendor;
import com.ichef.android.retrofit.APIInterface;
import com.ichef.android.retrofit.ApiClient;
import com.ichef.android.utils.CommonUtility;
import com.ichef.android.utils.Prefrence;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;


public class UnitAdapter extends RecyclerView.Adapter<UnitAdapter.ViewHolder> {
    private Context ctx;
    private List<UnitPrice> mlist;
    List<Cart> mCartList;
    String foodId;
    String foodName;
    private Vendor vendorId;
    private Vendor cartVendorId;

    public UnitAdapter(Context context, ArrayList<UnitPrice> list, Vendor vendorId, List<Cart> mCartList,
                       String foodId, String foodName, Vendor cartVendorId) {
        mlist = list;
        this.mCartList = mCartList;
        ctx = context;
        this.vendorId = vendorId;
        this.foodId = foodId;
        this.foodName = foodName;
        this.cartVendorId = cartVendorId;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.list_cart, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        if (mlist.get(position).getUnitName() != null) {
            holder.name.setText(mlist.get(position).getUnitName());
        } else {
            holder.name.setText("NA");
        }

        Boolean isQtyFound = false;
        if (mCartList != null && mCartList.size() > 0) {
            for (int i = 0; i < mCartList.size(); i++) {
                if (mCartList.get(i).getUnit().getId().equalsIgnoreCase(mlist.get(position).getUnitId()) &&
                        mCartList.get(i).getFoodItem().getId().equalsIgnoreCase(foodId)) {
                    holder.tvQuantity.setText(mCartList.get(i).getQuantity() + "");
                    isQtyFound = true;
                    break;
                }
            }
        } else {
            holder.tvQuantity.setText("0");
        }


        if (isQtyFound == false) {
            holder.tvQuantity.setText("0");
        }

        holder.minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                removeCartApiCall(position, holder.tvQuantity, holder.add, holder.minus, holder.progressBarMinus, holder.progressBarAdd);
            }
        });

        holder.add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mCartList != null && mCartList.size() > 0) {
                    if(cartVendorId != null && cartVendorId.getId().equalsIgnoreCase(vendorId.getId())){
                        addCartApiCall(position, holder.tvQuantity, holder.add, holder.minus, holder.progressBarMinus, holder.progressBarAdd);
                    }else{
                            AlertDialog.Builder alertDialog = new AlertDialog.Builder(ctx);
                            alertDialog.setTitle("Replace cart item?");
                            alertDialog.setMessage("Your cart contains dishes from "+cartVendorId.getBusinessName()+". Do you want to discard the selection and add dishes from "+vendorId.getBusinessName()+"?");
                            alertDialog.setPositiveButton("Clear Cart", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    ((ProductDetailActivity)ctx).clearCart();
                                }
                            });

                            // Setting Negative "NO" Button
                            alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // Write your code here to invoke NO event
                                    dialog.dismiss();
                                    dialog.cancel();
                                }
                            });
                            // Showing Alert Message
                            alertDialog.show();
                            return;

                    }
                }else{
                    addCartApiCall(position, holder.tvQuantity, holder.add, holder.minus, holder.progressBarMinus, holder.progressBarAdd);
                }
            }
        });
        holder.amount.setText(String.valueOf(mlist.get(position).getPrice()));
    }

    @Override
    public int getItemCount() {
        return mlist.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView name, amount, tvQuantity;
        ImageView minus, add;
        ProgressBar progressBarMinus, progressBarAdd;

        public ViewHolder(View itemView) {
            super(itemView);

            this.name = (TextView) itemView.findViewById(R.id.namecartitem);
            this.tvQuantity = (TextView) itemView.findViewById(R.id.tvQuantity);
            this.amount = (TextView) itemView.findViewById(R.id.amountcart);
            this.minus = itemView.findViewById(R.id.minus);
            this.add = itemView.findViewById(R.id.add);
            this.progressBarMinus = itemView.findViewById(R.id.progressBarMinus);
            this.progressBarAdd = itemView.findViewById(R.id.progressBarAdd);
        }
    }


    private void removeCartApiCall(int pos, TextView txt, ImageView imgAdd, ImageView imgMinus, ProgressBar mProgressMinus, ProgressBar mProgressAddress) {
        mProgressMinus.setVisibility(View.VISIBLE);
        mProgressAddress.setVisibility(View.INVISIBLE);
        imgMinus.setVisibility(View.INVISIBLE);
        String token = Prefrence.get(ctx, Prefrence.KEY_TOKEN);
        ReduceCartRequest request = new ReduceCartRequest();
        request.setUserID(Prefrence.get(ctx, Prefrence.KEY_USER_ID));
        request.setFoodItem(foodId);
        request.setUnit(mlist.get(pos).getUnitId());

        APIInterface apiInterface = ApiClient.getClient().create(APIInterface.class);
        Call<FetchCartResponse> resultCall = apiInterface.CallReduceCart("Bearer " + token, request);
        resultCall.enqueue(new Callback<FetchCartResponse>() {

            @Override
            public void onResponse(Call<FetchCartResponse> call, retrofit2.Response<FetchCartResponse> response) {
                mProgressMinus.setVisibility(View.INVISIBLE);
                mProgressAddress.setVisibility(View.INVISIBLE);
                imgMinus.setVisibility(View.VISIBLE);
                if (response.body().getStatus()) {
                    Toast.makeText(ctx, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    CommonUtility.saveCartDetails(ctx, response.body().getResult());
                    mCartList = response.body().getResult().getCart();
                    cartVendorId = response.body().getResult().getVendor();
                    notifyDataSetChanged();
                } else {
                    //Toast.makeText(ctx, "Please try again", Toast.LENGTH_SHORT).show();
                    Prefrence.saveInt(ctx, Prefrence.KEY_ITEM_COUNT, 0);
                    Prefrence.save(ctx, Prefrence.KEY_TOTAL, "0");
                    mCartList = new ArrayList<>();
                    notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(Call<FetchCartResponse> call, Throwable t) {
                mProgressMinus.setVisibility(View.INVISIBLE);
                mProgressAddress.setVisibility(View.INVISIBLE);
                imgMinus.setVisibility(View.VISIBLE);
                Toast.makeText(ctx, "Please check your Internet Connection", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void addCartApiCall(int pos, TextView txt, ImageView imgAdd, ImageView imgMinus, ProgressBar mProgressMinus, ProgressBar mProgressAdd) {
        mProgressMinus.setVisibility(View.INVISIBLE);
        mProgressAdd.setVisibility(View.VISIBLE);
        imgAdd.setVisibility(View.INVISIBLE);
        String token = Prefrence.get(ctx, Prefrence.KEY_TOKEN);
        AddToCartRequest request = new AddToCartRequest();
        request.setFoodItem(foodId);
        request.setRestaurantId(vendorId.getId());
        request.setFoodItemName(foodName);
        request.setPrice(mlist.get(pos).getPrice().toString());
        request.setUnit(mlist.get(pos).getUnitId());


        APIInterface apiInterface = ApiClient.getClient().create(APIInterface.class);
        Call<FetchCartResponse> resultCall = apiInterface.CallAddToCart("Bearer " + token, request);
        resultCall.enqueue(new Callback<FetchCartResponse>() {

            @Override
            public void onResponse(Call<FetchCartResponse> call, retrofit2.Response<FetchCartResponse> response) {
                mProgressMinus.setVisibility(View.INVISIBLE);
                mProgressAdd.setVisibility(View.INVISIBLE);
                imgAdd.setVisibility(View.VISIBLE);
                if (response.body().getStatus()) {
                    cartVendorId = response.body().getResult().getVendor();
                    Toast.makeText(ctx, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    CommonUtility.saveCartDetails(ctx, response.body().getResult());
                    mCartList = response.body().getResult().getCart();
                    notifyDataSetChanged();
                } else {
                    //Toast.makeText(ctx, "Cart is empty, Please add item first", Toast.LENGTH_LONG).show();
                    Prefrence.saveInt(ctx, Prefrence.KEY_ITEM_COUNT, 0);
                    Prefrence.save(ctx, Prefrence.KEY_TOTAL, "0");
                    mCartList = new ArrayList<>();
                    notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(Call<FetchCartResponse> call, Throwable t) {
                mProgressMinus.setVisibility(View.INVISIBLE);
                mProgressAdd.setVisibility(View.INVISIBLE);
                imgAdd.setVisibility(View.VISIBLE);
                Toast.makeText(ctx, "Please check your Internet Connection", Toast.LENGTH_SHORT).show();
            }
        });

    }

}
