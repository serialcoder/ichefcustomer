package com.ichef.android.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.ichef.android.R;
import com.ichef.android.activity.Feedback;
import com.ichef.android.activity.VendorDetailActivity;
import com.ichef.android.requestmodel.markbookmark.MarkBookmarkRequest;
import com.ichef.android.responsemodel.Address.AddAddressResponse;
import com.ichef.android.responsemodel.bookmarklist.Result;
import com.ichef.android.retrofit.APIInterface;
import com.ichef.android.retrofit.ApiClient;
import com.ichef.android.utils.Prefrence;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;


public class BookmarkAdapter extends RecyclerView.Adapter<BookmarkAdapter.ViewHolder> {
    private Context ctx;
    private List<Result> mlist;
    private ArrayList<Result> slist;

    String id;

    public BookmarkAdapter(Context context, ArrayList<Result> list) {
        mlist = list;
        ctx = context;
        this.slist = new ArrayList<Result>();
        this.slist.addAll(mlist);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.list_bookmark, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        holder.vendorName.setText(mlist.get(position).getVendor().getBusinessName());
   /* String categroylist = "";
    if (mlist.get(position).getVendor().getCategories() != null) {
        for (int i = 0; i < mlist.get(position).getVendor().getCategories().size(); i++) {
            categroylist = categroylist + " \u2022 " + mlist.get(position).getVendor().getCategories().get(i).getCategoryName();
            if (i == 3) {
                break;
            }
        }
    }
    holder.tvCategory.setText(categroylist);
*/

        if (mlist.get(position).getVendor().getAverageRating() != null) {
            holder.ratingStars.setRating(mlist.get(position).getVendor().getAverageRating()); // to set rating value
            holder.ratingStars.setStepSize(mlist.get(position).getVendor().getAverageRating());
        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ctx, VendorDetailActivity.class);
                intent.putExtra("VendorId", mlist.get(position).getVendor().getId());
                ctx.startActivity(intent);
            }
        });

        holder.ratingStars.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ctx, Feedback.class);
                ctx.startActivity(intent);
            }
        });
        holder.bookmarkred.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removeAsBookmark(mlist.get(position).getId(),position);
                holder.bookmarkred.setVisibility(View.GONE);
                holder.bookmarkwhite.setVisibility(View.VISIBLE);
            }
        });
        holder.bookmarkwhite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                markAsBookmark(mlist.get(position).getVendor().getId());
                holder.bookmarkred.setVisibility(View.VISIBLE);
                holder.bookmarkwhite.setVisibility(View.GONE);
            }
        });
        holder.share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent share = new Intent(Intent.ACTION_SEND);
                share.setType("text/plain");
                share.putExtra(Intent.EXTRA_TEXT, "Your text");
                ctx.startActivity(Intent.createChooser(share, "Share using"));
            }
        });

        if(mlist.get(position).getVendor().getDisplayPicture() != null && ! mlist.get(position).getVendor().getDisplayPicture().equalsIgnoreCase("")){
            Picasso.get().load(mlist.get(position).getVendor().getDisplayPicture())
                    .placeholder(R.drawable.ic__placeholder_new)
                    .into(holder.imgFood);
        }

    /*Double mDistance = Utils.distance(Double.parseDouble(Prefrence.get(ctx, Prefrence.KEY_LATITUDE)),
            Double.parseDouble(Prefrence.get(ctx, Prefrence.KEY_LONGITUDE)),
            mlist.get(position).getVendor().getLocation().getCoordinates().get(0), mlist.get(position).getVendor().getLocation().getCoordinates().get(1));
    holder.tvDistance.setText(String.format("%.2f", mDistance) + " Miles");
*/

    }

    private void markAsBookmark(String id) {
        String token = Prefrence.get(ctx, Prefrence.KEY_TOKEN);
        MarkBookmarkRequest request = new MarkBookmarkRequest();
        request.setUserID(Prefrence.get(ctx, Prefrence.KEY_USER_ID));
        request.setVendor(id);


        APIInterface apiInterface = ApiClient.getClient().create(APIInterface.class);
        Call<AddAddressResponse> resultCall = apiInterface.CallAddBookMark("Bearer " + token, request);
        resultCall.enqueue(new Callback<AddAddressResponse>() {

            @Override
            public void onResponse(Call<AddAddressResponse> call, retrofit2.Response<AddAddressResponse> response) {
                if (response.body().getStatus().equals(true)) {
                    Toast.makeText(ctx, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(ctx, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<AddAddressResponse> call, Throwable t) {
                Toast.makeText(ctx, "Please check your Internet Connection", Toast.LENGTH_SHORT).show();
            }
        });

    }
    private void removeAsBookmark(String id,int position) {
        String token = Prefrence.get(ctx, Prefrence.KEY_TOKEN);
        APIInterface apiInterface = ApiClient.getClient().create(APIInterface.class);
        Call<AddAddressResponse> resultCall = apiInterface.CallRemoveBookMark("Bearer " + token, id);
        resultCall.enqueue(new Callback<AddAddressResponse>() {

            @Override
            public void onResponse(Call<AddAddressResponse> call, retrofit2.Response<AddAddressResponse> response) {
                if (response.body().getStatus().equals(true)) {
                    Toast.makeText(ctx, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    mlist.remove(position);
                    notifyItemRemoved(position);
                } else {
                    Toast.makeText(ctx, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<AddAddressResponse> call, Throwable t) {
                Toast.makeText(ctx, "Please check your Internet Connection", Toast.LENGTH_SHORT).show();
            }
        });

    }


    @Override
    public int getItemCount() {
        return mlist.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView vendorName, tvCategory, tvDistance;
        RatingBar ratingStars;
        ImageView bookmarkred, bookmarkwhite, share;
        ImageView imgFood;

        public ViewHolder(View itemView) {
            super(itemView);

            this.vendorName = (TextView) itemView.findViewById(R.id.name);
            this.tvCategory = (TextView) itemView.findViewById(R.id.tvCategory);
            this.tvDistance = itemView.findViewById(R.id.tvDistance);
            this.ratingStars = itemView.findViewById(R.id.ratingStars);
            this.bookmarkwhite = itemView.findViewById(R.id.bookmarkwhite);
            this.bookmarkred = itemView.findViewById(R.id.bookmarkred);
            this.share = itemView.findViewById(R.id.share);
            this.imgFood = itemView.findViewById(R.id.imgFood);

        }
    }
}
