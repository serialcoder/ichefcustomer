package com.ichef.android.adapter;

import android.content.Context;
import android.content.Intent;
import android.media.Image;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.cloudinary.Transformation;
import com.cloudinary.android.MediaManager;
import com.ichef.android.R;
import com.ichef.android.activity.Feedback;
import com.ichef.android.activity.HomePageActivity;
import com.ichef.android.activity.VendorDetailActivity;
import com.ichef.android.requestmodel.markbookmark.MarkBookmarkRequest;
import com.ichef.android.responsemodel.Address.AddAddressResponse;
import com.ichef.android.responsemodel.markbookmark.MarkBookmarkResponse;
import com.ichef.android.responsemodel.productlist.Vendor;
import com.ichef.android.retrofit.APIInterface;
import com.ichef.android.retrofit.ApiClient;
import com.ichef.android.utils.Prefrence;
import com.ichef.android.utils.TransparentProgressDialog;
import com.ichef.android.utils.Utils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;


public class HomeFoodAdapter extends RecyclerView.Adapter<HomeFoodAdapter.ViewHolder> {
    private Context ctx;
    private List<Vendor> mlist;
    private ArrayList<Vendor> slist;

    String token;

    public HomeFoodAdapter(Context context, ArrayList<Vendor> list) {
        mlist = list;
        ctx = context;
        this.slist = new ArrayList<Vendor>();
        this.slist.addAll(mlist);
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.list_food, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        holder.vendorName.setText(mlist.get(position).getBusinessName());
        holder.tvCurrentLat.setText("Current Lat/Long- "+Prefrence.get(ctx, Prefrence.KEY_LATITUDE)+"--"+Prefrence.get(ctx, Prefrence.KEY_LONGITUDE));
        holder.tvVendorLatlong.setText("Vendor Lat/Long - "+mlist.get(position).getLocation().getCoordinates().get(1)+"--"+mlist.get(position).getLocation().getCoordinates().get(0));
        if(mlist.get(position).getCategories() != null && mlist.get(position).getCategories().size()>0){
            String categroylist = "";
            if (mlist.get(position).getCategories() != null) {
                for (int i = 0; i < mlist.get(position).getCategories().size(); i++) {
                    categroylist = categroylist + " \u2022 " + mlist.get(position).getCategories().get(i).getCategoryName();
                    if (i == 3) {
                        break;
                    }
                }
            }

            holder.tvCategory.setText(categroylist);
        }else{
            holder.tvCategory.setText(mlist.get(position).getBusinessType());
        }

        if (mlist.get(position).getAverageRating() != null) {
            holder.ratingStars.setRating(mlist.get(position).getAverageRating()); // to set rating value
            holder.ratingStars.setStepSize(mlist.get(position).getAverageRating());
        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mlist.get(position).getAvailable()) {
                    Intent intent = new Intent(ctx, VendorDetailActivity.class);
                    intent.putExtra("VendorId", mlist.get(position).getId());
                    ctx.startActivity(intent);
                }else{
                    Toast.makeText(ctx, ctx.getResources().getString(R.string.vendor_closed), Toast.LENGTH_SHORT).show();
                }
            }
        });

        holder.ratingStars.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ctx, Feedback.class);
                ctx.startActivity(intent);
            }
        });
        holder.bookmarkred.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.bookmarkred.setVisibility(View.GONE);
                holder.bookmarkwhite.setVisibility(View.VISIBLE);
            }
        });
        holder.bookmarkwhite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                markAsBookmark(mlist.get(position).getId(),position,holder.bookmarkwhite,holder.bookmarkred);
               /* holder.bookmarkred.setVisibility(View.VISIBLE);
                holder.bookmarkwhite.setVisibility(View.GONE);*/
            }
        });
        holder.share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent share = new Intent(Intent.ACTION_SEND);
                share.setType("text/plain");
                share.putExtra(Intent.EXTRA_TEXT, "Your text");
                ctx.startActivity(Intent.createChooser(share, "Share using"));
            }
        });

        if(mlist.get(position).getLocation() != null && mlist.get(position).getLocation().getCoordinates()!= null){
            Double mDistance = Utils.distance(Double.parseDouble(Prefrence.get(ctx, Prefrence.KEY_LATITUDE)),
                    Double.parseDouble(Prefrence.get(ctx, Prefrence.KEY_LONGITUDE)),
                    mlist.get(position).getLocation().getCoordinates().get(1), mlist.get(position).getLocation().getCoordinates().get(0));
            holder.tvDistance.setText(String.format("%.2f", mDistance) + " KM");
        }else{
            holder.tvDistance.setText("NA KM");
        }

        if(mlist.get(position).getAvailable()){
            holder.tvVendorStatus.setText("OPEN");
            holder.tvVendorStatus.setTextColor(ctx.getResources().getColor(R.color.colorPrimary));
        }else{
            holder.tvVendorStatus.setText("CLOSED");
            holder.tvVendorStatus.setTextColor(ctx.getResources().getColor(R.color.themered));
        }


        if(mlist.get(position).getDisplayPicture() != null && ! mlist.get(position).getDisplayPicture().equalsIgnoreCase("")){


            Transformation tr = new Transformation();
            tr.crop("fit").width(HomePageActivity.widthDevice);      // "c_fit,w_100"
            String url = MediaManager.get().url().transformation(tr).generate(mlist.get(position).getDisplayPicture());

            Picasso.get().load(url)
                    .placeholder(R.drawable.placeholder_new)
                    .into(holder.imgFood);
        }


    }

    private void markAsBookmark(String id, int pos, ImageView favWhite, ImageView favRed) {
        TransparentProgressDialog dialog = new TransparentProgressDialog(ctx);
        dialog.show();
        token = Prefrence.get(ctx, Prefrence.KEY_TOKEN);
        MarkBookmarkRequest request = new MarkBookmarkRequest();
        request.setUserID(Prefrence.get(ctx,Prefrence.KEY_USER_ID));
        request.setVendor(id);


        APIInterface apiInterface = ApiClient.getClient().create(APIInterface.class);
        Call<AddAddressResponse> resultCall = apiInterface.CallAddBookMark("Bearer " + token, request);
        resultCall.enqueue(new Callback<AddAddressResponse>() {

            @Override
            public void onResponse(Call<AddAddressResponse> call, retrofit2.Response<AddAddressResponse> response) {
                if (response.body().getStatus().equals(true)) {
                    favWhite.setVisibility(View.GONE);
                    favRed.setVisibility(View.VISIBLE);
                    Toast.makeText(ctx, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(ctx, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }
                dialog.dismiss();
            }

            @Override
            public void onFailure(Call<AddAddressResponse> call, Throwable t) {
                dialog.dismiss();
                Toast.makeText(ctx, "Please check your Internet Connection", Toast.LENGTH_SHORT).show();
            }
        });

    }


    @Override
    public int getItemCount() {
        return mlist.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView vendorName, tvCategory, tvDistance,tvCurrentLat,tvVendorLatlong,tvVendorStatus;
        RatingBar ratingStars;
        ImageView bookmarkred, bookmarkwhite, share,imgFood;

        public ViewHolder(View itemView) {
            super(itemView);

            this.tvVendorLatlong = (TextView) itemView.findViewById(R.id.tvVendorLatlong);
            this.tvCurrentLat = (TextView) itemView.findViewById(R.id.tvCurrentLat);
            this.vendorName = (TextView) itemView.findViewById(R.id.name);
            this.tvCategory = (TextView) itemView.findViewById(R.id.tvCategory);
            this.tvDistance = itemView.findViewById(R.id.tvDistance);
            this.ratingStars = itemView.findViewById(R.id.ratingStars);
            this.bookmarkwhite = itemView.findViewById(R.id.bookmarkwhite);
            this.bookmarkred = itemView.findViewById(R.id.bookmarkred);
            this.share = itemView.findViewById(R.id.share);
            this.imgFood = itemView.findViewById(R.id.imgFood);
            this.tvVendorStatus = itemView.findViewById(R.id.tvVendorStatus);

        }
    }
}
