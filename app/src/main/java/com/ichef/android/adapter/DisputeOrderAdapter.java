package com.ichef.android.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import com.ichef.android.R;
import com.ichef.android.activity.DisputeMessageActivity;
import com.ichef.android.activity.MyCart;
import com.ichef.android.activity.OrderDetail;
import com.ichef.android.activity.ReviewRating;
import com.ichef.android.requestmodel.CreateOrder.CancelOrderRequest;
import com.ichef.android.requestmodel.CreateOrder.DisputeOrderRequest;
import com.ichef.android.responsemodel.Address.AddAddressResponse;
import com.ichef.android.responsemodel.orders.Cart;
import com.ichef.android.responsemodel.orders.Reorder.ReorderResponse;
import com.ichef.android.responsemodel.orders.Result;
import com.ichef.android.retrofit.APIInterface;
import com.ichef.android.retrofit.ApiClient;
import com.ichef.android.utils.Prefrence;
import com.ichef.android.utils.TransparentProgressDialog;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class DisputeOrderAdapter extends RecyclerView.Adapter<DisputeOrderAdapter.ViewHolder> {
    private Context ctx;
    private List<com.ichef.android.responsemodel.dispute.Result> mlist;
    private ArrayList<com.ichef.android.responsemodel.dispute.Result> slist;

    String id;

    public DisputeOrderAdapter(Context context, ArrayList<com.ichef.android.responsemodel.dispute.Result> list) {
        mlist = list;
        ctx = context;
        this.slist = new ArrayList<com.ichef.android.responsemodel.dispute.Result>();
        this.slist.addAll(mlist);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.list_dispute, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        com.ichef.android.responsemodel.dispute.Result result = mlist.get(position);
        holder.tvVendorName.setText(result.getDisputeAgainst().getFirstname());
        holder.tvStatus.setText(result.getStatus());
        holder.tvDIsputedID.setText(result.getId());
        holder.tvOrderID.setText(result.getOrder());
        holder.tvReason.setText(result.getReason());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ctx, DisputeMessageActivity.class);
                intent.putExtra("disputeID",result.getId());
                intent.putExtra("status",result.getStatus());
                ctx.startActivity(intent);
            }
        });
    }


    @Override
    public int getItemCount() {
        return mlist.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tvVendorName , tvStatus, tvDIsputedID,tvOrderID,tvReason;

        public ViewHolder(View itemView) {
            super(itemView);
            this.tvStatus = (TextView) itemView.findViewById(R.id.tvStatus);
            this.tvVendorName = (TextView) itemView.findViewById(R.id.tvVendorName);
            this.tvDIsputedID = (TextView) itemView.findViewById(R.id.tvDIsputedID);
            this.tvOrderID = (TextView) itemView.findViewById(R.id.tvOrderID);
            this.tvReason = (TextView) itemView.findViewById(R.id.tvReason);
        }
    }
}
