package com.ichef.android.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.ichef.android.R;
import com.ichef.android.activity.VendorDetailActivity;
import com.ichef.android.responsemodel.SearchResult;
import com.ichef.android.responsemodel.search.Vendor__1;


import java.util.ArrayList;
import java.util.List;


public class VendorAdapter extends RecyclerView.Adapter<VendorAdapter.ViewHolder> {
    private Context ctx;
    private List<Vendor__1> mlist;
    private ArrayList<Vendor__1> slist;
    LayoutInflater inflater;

    String id;

    public VendorAdapter(Context context, ArrayList<Vendor__1> list) {
        mlist = list;
        ctx = context;
        inflater = LayoutInflater.from(context);
        this.slist = new ArrayList<Vendor__1>();
        this.slist.addAll(mlist);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.list_search, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        holder.tvVendorName.setText(mlist.get(position).getBusinessName()); //mlist.get(position).getFoodItemID()
        holder.tvRating.setText(mlist.get(position).getAverageRating() + ""); //mlist.get(position).getFoodItemID()
        holder.cardMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ctx, VendorDetailActivity.class);
                intent.putExtra("VendorId", mlist.get(position).getId());
                ctx.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return mlist.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tvRating, tvVendorName;
        CardView cardMain;

        public ViewHolder(View itemView) {
            super(itemView);
            this.cardMain=itemView.findViewById(R.id.cardMain);
            this.tvRating = (TextView) itemView.findViewById(R.id.tvRating);
            this.tvVendorName = (TextView) itemView.findViewById(R.id.tvVendorName);


        }
    }
}
