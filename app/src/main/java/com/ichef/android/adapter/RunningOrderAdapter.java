package com.ichef.android.adapter;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.ichef.android.R;
import com.ichef.android.activity.MyCart;
import com.ichef.android.activity.ReviewRating;
import com.ichef.android.activity.RunningOrder;
import com.ichef.android.requestmodel.CreateOrder.CancelOrderRequest;
import com.ichef.android.requestmodel.CreateOrder.DisputeOrderRequest;
import com.ichef.android.responsemodel.Address.AddAddressResponse;
import com.ichef.android.responsemodel.orders.Cart;
import com.ichef.android.responsemodel.orders.Reorder.ReorderResponse;
import com.ichef.android.responsemodel.orders.Result;
import com.ichef.android.retrofit.APIInterface;
import com.ichef.android.retrofit.ApiClient;
import com.ichef.android.utils.Prefrence;
import com.ichef.android.utils.TransparentProgressDialog;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class RunningOrderAdapter extends RecyclerView.Adapter<RunningOrderAdapter.ViewHolder> {
    private Context ctx;
    private List<Result> mlist;
    private ArrayList<Result> slist;

    String id;

    public RunningOrderAdapter(Context context, ArrayList<Result> list) {
        mlist = list;
        ctx = context;
        this.slist = new ArrayList<Result>();
        this.slist.addAll(mlist);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.list_runningorder, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        Result result = mlist.get(position);
        if(result.getVendor().getFirstname() != null){
            holder.tvVendorName.setText(result.getVendor().getFirstname());
        }else{
            holder.tvVendorName.setText("NA");
        }
        holder.tvStatus.setText(result.getStatus());
        holder.tvLocation.setText(result.getAddress().getAddress());
        holder.imgCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int permissionCheck = ContextCompat.checkSelfPermission(ctx, Manifest.permission.CALL_PHONE);

                if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
                    ((RunningOrder)ctx).onCall();
                } else {
                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + result.getVendor().getMobileNumber()));
                    ctx.startActivity(intent);
                }
            }
        });

        String cartItem = "";
        for (Cart cart : result.getCart()) {
            if (cart.getFoodItemName() != null) {
                cartItem = cartItem + cart.getQuantity() + " X " + cart.getFoodItemName() + ", ";
            }
        }
        if (cartItem.length() > 0) {
            holder.tvCartItem.setText(cartItem.substring(0, cartItem.length() - 2));
        }
        try {
            String strCurrentDate = result.getDatecreated();
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
            format.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date newDate = format.parse(strCurrentDate);
            format = new SimpleDateFormat("MMM dd,yyyy hh:mm a");
            String date = format.format(newDate);
            holder.tvTime.setText(date);//2021-06-16T12:50:51.895Z
            if (System.currentTimeMillis() < newDate.getTime() + (5 * 60 * 1000) && result.getStatus().equalsIgnoreCase("pending")) {
                holder.tvCancel.setVisibility(View.VISIBLE);
            } else {
                holder.tvCancel.setVisibility(View.GONE);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        holder.tvOrderId.setText("Order ID: "+mlist.get(position).getOrder_id());//2021-06-16T12:50:51.895Z

        if (result.getStatus().equalsIgnoreCase("completed") && !result.getDisputed()) {
            holder.tvDispute.setVisibility(View.VISIBLE);
            holder.rlRatingView.setVisibility(View.VISIBLE);
        } else {
            holder.tvDispute.setVisibility(View.GONE);
            holder.rlRatingView.setVisibility(View.GONE);

        }
        if (result.getReviewed()) {
            holder.llRated.setVisibility(View.VISIBLE);
            holder.tvRated.setText("You Rated " + result.getReview().getRating());
            holder.rlRatingView.setVisibility(View.GONE);
        } else {
            holder.llRated.setVisibility(View.GONE);
        }
        if (result.getDisputed()) {
            holder.tvDisputed.setVisibility(View.VISIBLE);
            holder.tvReorder.setVisibility(View.GONE);
        } else {
            holder.tvDisputed.setVisibility(View.GONE);
            if (result.getStatus().equalsIgnoreCase("completed") || result.getStatus().equalsIgnoreCase("rejected") || result.getStatus().equalsIgnoreCase("cancelled")) {
                holder.tvReorder.setVisibility(View.VISIBLE);
            } else {
                holder.tvReorder.setVisibility(View.GONE);
            }
        }
        holder.tvTotalAmount.setText(ctx.getString(R.string.nigiriacurrency) + " " + result.getAmountCharged());

        holder.rlRatingView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ctx.startActivity(new Intent(ctx, ReviewRating.class).putExtra("orderId", result.getId()).putExtra("vendorName", result.getVendor().getFirstname()).putExtra("vendorId", result.getVendor().getId()));
            }
        });
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*Intent intent = new Intent(ctx, OrderDetail.class);
                ctx.startActivity(intent);*/
            }
        });
        holder.tvDispute.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alertbox = new AlertDialog.Builder(ctx);
                LinearLayout ll_alert_layout = new LinearLayout(ctx);
                ll_alert_layout.setOrientation(LinearLayout.VERTICAL);
                final EditText ed_input = new EditText(ctx);
                ll_alert_layout.addView(ed_input);
                alertbox.setTitle("Raise a dispute");
                alertbox.setMessage("Please add a detailed not about dispute");
                //setting linear layout to alert dialog
                alertbox.setView(ll_alert_layout);

                alertbox.setNegativeButton("CANCEL",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface arg0, int arg1) {
                                // will automatically dismiss the dialog and will do nothing
                            }
                        });


                alertbox.setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface arg0, int arg1) {
                                String input_text = ed_input.getText().toString();
                                // do your action with input string
                                if (input_text.isEmpty()) {
                                    ed_input.setError("Please add detailed reason for dispute");
                                } else {
                                    DisputeOrderRequest request = new DisputeOrderRequest();
                                    request.setOrder(result.getId());
                                    request.setDisputeAgainst(result.getVendor().getId());
                                    request.setReason(input_text);
                                    request.setDisputeFor(Prefrence.get(ctx, Prefrence.KEY_USER_ID));
                                    callDisputeAPI(request, position);
                                }
                            }
                        });
                alertbox.show();
            }
        });
        holder.tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(ctx);
                alertDialog.setMessage("Are you sure, you want to cancel this order?");
                alertDialog.setIcon(android.R.drawable.ic_menu_close_clear_cancel);
                alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        cancelOrder(position);
                    }
                });

                // Setting Negative "NO" Button
                alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // Write your code here to invoke NO event
                        dialog.dismiss();
                        dialog.cancel();
                    }
                });
                // Showing Alert Message
                alertDialog.show();
            }
        });
        holder.tvReorder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Prefrence.getInt(ctx, Prefrence.KEY_ITEM_COUNT) > 0) {
                    android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(ctx);
                    alertDialog.setMessage("Your cart is already having items added, do you want to clear cart and reorder?");
                    alertDialog.setIcon(android.R.drawable.ic_menu_close_clear_cancel);
                    alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            clearCart(result.getId());
                        }
                    });

                    // Setting Negative "NO" Button
                    alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // Write your code here to invoke NO event
                            dialog.dismiss();
                            dialog.cancel();
                        }
                    });
                    // Showing Alert Message
                    alertDialog.show();
                } else {
                    reorder(result.getId());
                }
            }
        });


    }

    private void callDisputeAPI(DisputeOrderRequest request, int position) {
        TransparentProgressDialog dialog = new TransparentProgressDialog(ctx);
        dialog.show();
        String token = Prefrence.get(ctx, Prefrence.KEY_TOKEN);
        APIInterface apiInterface = ApiClient.getClient().create(APIInterface.class);
        Call<AddAddressResponse> resultCall = apiInterface.CallDisputeOrder("Bearer " + token, request);
        resultCall.enqueue(new Callback<AddAddressResponse>() {

            @Override
            public void onResponse(Call<AddAddressResponse> call, retrofit2.Response<AddAddressResponse> response) {
                if (response.body().getStatus()) {
                    Toast.makeText(ctx, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    mlist.get(position).setDisputed(true);
                    notifyItemChanged(position);
                } else {
                    Toast.makeText(ctx, " " + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }
                dialog.dismiss();
            }

            @Override
            public void onFailure(Call<AddAddressResponse> call, Throwable t) {
                Toast.makeText(ctx, "Please check your Internet Connection", Toast.LENGTH_SHORT).show();
                dialog.dismiss();
            }
        });
    }

    private void cancelOrder(int position) {
        TransparentProgressDialog dialog = new TransparentProgressDialog(ctx);
        dialog.show();
        String token = Prefrence.get(ctx, Prefrence.KEY_TOKEN);
        CancelOrderRequest request = new CancelOrderRequest();
        request.setStatus("cancelled");
        APIInterface apiInterface = ApiClient.getClient().create(APIInterface.class);
        Call<AddAddressResponse> resultCall = apiInterface.CallCancelOrder("Bearer " + token, mlist.get(position).getId(), request);
        resultCall.enqueue(new Callback<AddAddressResponse>() {

            @Override
            public void onResponse(Call<AddAddressResponse> call, retrofit2.Response<AddAddressResponse> response) {
                if (response.body().getStatus()) {
                    Toast.makeText(ctx, "Order Canceled Successfully", Toast.LENGTH_SHORT).show();
                    mlist.get(position).setStatus("cancelled");
                    notifyItemChanged(position);
                } else {
                    Toast.makeText(ctx, " " + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }
                dialog.dismiss();
            }

            @Override
            public void onFailure(Call<AddAddressResponse> call, Throwable t) {
                Toast.makeText(ctx, "Please check your Internet Connection", Toast.LENGTH_SHORT).show();
                dialog.dismiss();
            }
        });
    }

    private void clearCart(String orderId) {
        TransparentProgressDialog dialog = new TransparentProgressDialog(ctx);
        dialog.show();
        String token = Prefrence.get(ctx, Prefrence.KEY_TOKEN);
        APIInterface apiInterface = ApiClient.getClient().create(APIInterface.class);
        Call<AddAddressResponse> call = apiInterface.ClearCart("Bearer " + token);
        call.enqueue(new Callback<AddAddressResponse>() {
            @Override
            public void onResponse(Call<AddAddressResponse> call, Response<AddAddressResponse> response) {
                dialog.dismiss();
                Prefrence.saveInt(ctx, Prefrence.KEY_ITEM_COUNT, 0);
                Prefrence.save(ctx, Prefrence.KEY_TOTAL, "0");
                reorder(orderId);
            }

            @Override
            public void onFailure(Call<AddAddressResponse> call, Throwable t) {

                dialog.dismiss();
                call.cancel();
            }
        });
    }

    private void reorder(String orderId) {
        TransparentProgressDialog dialog = new TransparentProgressDialog(ctx);
        dialog.show();
        String token = Prefrence.get(ctx, Prefrence.KEY_TOKEN);
        APIInterface apiInterface = ApiClient.getClient().create(APIInterface.class);
        Call<ReorderResponse> call = apiInterface.CallReorder("Bearer " + token, orderId);
        call.enqueue(new Callback<ReorderResponse>() {
            @Override
            public void onResponse(Call<ReorderResponse> call, Response<ReorderResponse> response) {
                dialog.dismiss();
                Prefrence.saveInt(ctx, Prefrence.KEY_ITEM_COUNT, response.body().getResult().getCartItemsCount());
                Prefrence.save(ctx, Prefrence.KEY_TOTAL, "" + response.body().getResult().getTotalCartAmount());
                ctx.startActivity(new Intent(ctx, MyCart.class));
            }

            @Override
            public void onFailure(Call<ReorderResponse> call, Throwable t) {

                dialog.dismiss();
                call.cancel();
            }
        });
    }

    @Override
    public int getItemCount() {
        return mlist.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tvVendorName, tvDisputed, tvReorder, tvRated,
                tvTotalAmount, tvStatus, tvDispute, tvLocation, tvTime, tvCartItem, tvCancel,tvOrderId;
        RelativeLayout rlRatingView;
        RelativeLayout llRated;
        ImageView imgCall;

        public ViewHolder(View itemView) {
            super(itemView);
            this.tvStatus = (TextView) itemView.findViewById(R.id.tvStatus);
            this.tvVendorName = (TextView) itemView.findViewById(R.id.tvVendorName);
            this.tvTotalAmount = (TextView) itemView.findViewById(R.id.tvTotalAmount);
            this.tvLocation = (TextView) itemView.findViewById(R.id.tvLocation);
            this.tvTime = (TextView) itemView.findViewById(R.id.tvTime);
            this.tvCartItem = (TextView) itemView.findViewById(R.id.tvCartItem);
            this.tvCancel = (TextView) itemView.findViewById(R.id.tvCancel);
            this.tvDispute = (TextView) itemView.findViewById(R.id.tvDispute);
            this.tvDisputed = (TextView) itemView.findViewById(R.id.tvDisputed);
            this.tvReorder = (TextView) itemView.findViewById(R.id.tvReorder);
            this.rlRatingView = itemView.findViewById(R.id.rlRatingView);
            this.llRated = itemView.findViewById(R.id.llRated);
            this.tvRated = itemView.findViewById(R.id.tvRated);
            this.imgCall = itemView.findViewById(R.id.imgCall);
            this.tvOrderId = itemView.findViewById(R.id.tvOrderId);
        }
    }
}
