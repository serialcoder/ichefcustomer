package com.ichef.android.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.ichef.android.R;
import com.ichef.android.responsemodel.Rating.Result;

import java.util.ArrayList;
import java.util.List;


public class ReviewNewAdapter extends RecyclerView.Adapter<ReviewNewAdapter.ViewHolder> {
    private Context ctx;
    private List<Result> mlist;
    private ArrayList<Result> slist;

    String id;

    public ReviewNewAdapter(Context context, ArrayList<Result> list) {
        mlist = list;
        ctx = context;
        this.slist = new ArrayList<Result>();
        this.slist.addAll(mlist);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.listreviewnew, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        String username = mlist.get(position).getUser().getFirstname();
        int rating = mlist.get(position).getRating();
        holder.ratingBar.setRating(rating);
        holder.name.setText(username);
        holder.more.setText(mlist.get(position).getComment());
        holder.dateofreview.setText("");
        id = mlist.get(position).getId();

    }

    @Override
    public int getItemCount() {
        return mlist.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView name, more, dateofreview;
        RatingBar ratingBar;


        public ViewHolder(View itemView) {
            super(itemView);

            this.name = itemView.findViewById(R.id.username);
            this.more = itemView.findViewById(R.id.reviewtext);
            this.dateofreview = itemView.findViewById(R.id.dateofreview);
            this.ratingBar = itemView.findViewById(R.id.ratingbyuser);

        }
    }
}
