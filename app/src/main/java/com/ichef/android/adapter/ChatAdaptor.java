package com.ichef.android.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;


import com.ichef.android.R;
import com.ichef.android.responsemodel.disputeMessage.DisputedMessageModel;
import com.ichef.android.utils.Prefrence;
import com.ichef.android.utils.Utils;

import java.util.ArrayList;


/**
 * A custom adapter to use with the RecyclerView widget.
 */
public class ChatAdaptor extends RecyclerView.Adapter<RecyclerView.ViewHolder>  {

    private Context mContext;
    ArrayList<com.ichef.android.responsemodel.disputeMessage.Result> chatModelList;
    String  disputeID;

    public ChatAdaptor(Context context, ArrayList<com.ichef.android.responsemodel.disputeMessage.Result> chatModelList, String disputeID) {
        this.mContext = context;
        this.chatModelList=chatModelList;
        this.disputeID=disputeID;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_chat, viewGroup, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        //Here you can fill your row view
        if (holder instanceof ViewHolder) {
            final ViewHolder genericViewHolder = (ViewHolder) holder;
            com.ichef.android.responsemodel.disputeMessage.Result chatModel=chatModelList.get(position);
            if(chatModel.getUser().getId().equalsIgnoreCase(Prefrence.get(mContext,Prefrence.KEY_USER_ID))){
                genericViewHolder.rlAdmin.setVisibility(View.VISIBLE);
                genericViewHolder.rlUser.setVisibility(View.GONE);
                genericViewHolder.txtAdminMessage.setText(chatModel.getMessage()==null?"":chatModel.getMessage());
                genericViewHolder.txtAdminDate.setText(Utils.formatDate(chatModel.getDatecreated()));
            }else{
                genericViewHolder.rlUser.setVisibility(View.VISIBLE);
                genericViewHolder.rlAdmin.setVisibility(View.GONE);
                genericViewHolder.txtUserMessage.setText(chatModel.getMessage()==null?"":chatModel.getMessage());
                genericViewHolder.txtUserDate.setText(Utils.formatDate(chatModel.getDatecreated()));
            }
        }
    }



    @Override
    public int getItemCount() {
        return  chatModelList==null?0:chatModelList.size();
    }

   

    public interface OnItemClickListener {
        void onItemClick(View view, int position, DisputedMessageModel model);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {


        private TextView txtUserMessage,txtAdminMessage;
        private LinearLayout rlAdmin;
        private LinearLayout rlUser;
        private TextView txtUserDate,txtAdminDate;
        
         ViewHolder(final View itemView) {
            super(itemView);
            // ButterKnife.bind(this, itemView);
            this.txtUserMessage =  itemView.findViewById(R.id.txtUserMessage);
            this.txtAdminMessage =  itemView.findViewById(R.id.txtAdminMessage);
            this.rlUser =  itemView.findViewById(R.id.rlUser);
            this.rlAdmin =  itemView.findViewById(R.id.rlAdmin);
            this.txtAdminDate =  itemView.findViewById(R.id.txtAdminDate);
            this.txtUserDate =  itemView.findViewById(R.id.txtUserDate);
        }
    }
}

