package com.ichef.android.retrofit;

import android.app.Application;

import com.cloudinary.android.LogLevel;
import com.cloudinary.android.MediaManager;
import com.cloudinary.android.download.glide.GlideDownloadRequestBuilderFactory;
import com.cloudinary.android.policy.GlobalUploadPolicy;
import com.cloudinary.android.policy.UploadPolicy;
import com.google.android.gms.common.api.Api;
import com.splunk.mint.Mint;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClient  extends Application {

    private static Retrofit retrofit = null;

    @Override
    public void onCreate() {
        super.onCreate();
        Mint.enableDebugLog();
        Mint.initAndStartSession(this, "0349fe22");
        // This can be called any time regardless of initialization.
        MediaManager.setLogLevel(LogLevel.DEBUG);
        // Mandatory - call a flavor of init. Config can be null if cloudinary_url is provided in the manifest.
        MediaManager.init(this);
        MediaManager.get().setDownloadRequestBuilderFactory(new GlideDownloadRequestBuilderFactory());

        // Optional - configure global policy.
        MediaManager.get().setGlobalUploadPolicy(
                new GlobalUploadPolicy.Builder()
                        .maxConcurrentRequests(4)
                        .networkPolicy(UploadPolicy.NetworkType.ANY)
                        .build());
    }

    public static Retrofit getClient() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder()
               // .addInterceptor(OAuthInterceptor("Bearer", "---ACCESS---TOKEN---"))
                .addInterceptor(interceptor)
                .readTimeout(2, TimeUnit.MINUTES)
                .connectTimeout(2, TimeUnit.MINUTES)
                .addInterceptor(new Interceptor() {
                    @Override
                    public Response intercept(Chain chain) throws IOException {
                        Request original = chain.request();
                        original = original.newBuilder()
                                .method(original.method(), original.body())
                                .build();
                        // try the request
                        Response response = chain.proceed(original);
                        return response;
                    }
                })
                .build();

        retrofit = new Retrofit.Builder()
               // .baseUrl("http://ichefproj.herokuapp.com/")
                .baseUrl("https://api.ichefmobile.com/")
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        return retrofit;
    }

    public static Retrofit getPaymentClient() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder()
               // .addInterceptor(OAuthInterceptor("Bearer", "---ACCESS---TOKEN---"))
                .addInterceptor(interceptor)
                .readTimeout(2, TimeUnit.MINUTES)
                .connectTimeout(2, TimeUnit.MINUTES)
                .addInterceptor(new Interceptor() {
                    @Override
                    public Response intercept(Chain chain) throws IOException {
                        Request original = chain.request();
                        original = original.newBuilder()
                                .method(original.method(), original.body())
                                .build();
                        // try the request
                        Response response = chain.proceed(original);
                        return response;
                    }
                })
                .build();

        retrofit = new Retrofit.Builder()
               // .baseUrl("http://139.59.86.31/time-it/index.php/mobileapi/")
                .baseUrl("https://api.paystack.co/")
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        return retrofit;
    }

    private static Interceptor OAuthInterceptor(String bearer, String s) {
        return null;
    }
}
