package com.ichef.android.retrofit;


import com.ichef.android.requestmodel.Address.AddAddressRequest;
import com.ichef.android.requestmodel.Cart.AddToCartRequest;
import com.ichef.android.requestmodel.Cart.ReduceCartRequest;
import com.ichef.android.requestmodel.Cart.UpdateCartRequest;
import com.ichef.android.requestmodel.CreateOrder.CancelOrderRequest;
import com.ichef.android.requestmodel.CreateOrder.CreateOrderRequest;
import com.ichef.android.requestmodel.CreateOrder.DisputeOrderRequest;
import com.ichef.android.requestmodel.addtocartrequest.AddtoCartRequest;
import com.ichef.android.requestmodel.dispute.SendMessageModel;
import com.ichef.android.requestmodel.markbookmark.MarkBookmarkRequest;
import com.ichef.android.requestmodel.payment.PaymentItem;
import com.ichef.android.requestmodel.rate.RateOrderRequest;
import com.ichef.android.requestmodel.user.ChangePasswordRequest;
import com.ichef.android.requestmodel.user.DeleteUserRequest;
import com.ichef.android.requestmodel.user.DeviceTokenRequest;
import com.ichef.android.requestmodel.user.EditProfileRequest;
import com.ichef.android.requestmodel.user.LoginRequest;
import com.ichef.android.requestmodel.user.OTPRequest;
import com.ichef.android.requestmodel.user.SignupRequest;
import com.ichef.android.requestmodel.user.SocialLoginRequest;
import com.ichef.android.requestmodel.user.UpdateNotificationRequest;
import com.ichef.android.responsemodel.Address.AddAddressResponse;
import com.ichef.android.responsemodel.Address.City.CityResponse;
import com.ichef.android.responsemodel.Address.GetAddress.GetAddressesResponse;
import com.ichef.android.responsemodel.PromoCode.ApplyPromoCodeResponse;
import com.ichef.android.responsemodel.Rating.ReviewsListResponse;
import com.ichef.android.responsemodel.addtocartresponse.AddtoCartResponse;
import com.ichef.android.responsemodel.areyoufeeling.FeelingListModel;
import com.ichef.android.responsemodel.banner.BannerListModel;
import com.ichef.android.responsemodel.bookmarklist.GetBookmarkListResponse;
import com.ichef.android.responsemodel.checkMobile.MobileExistenceModel;
import com.ichef.android.responsemodel.dispute.DisputedModel;
import com.ichef.android.responsemodel.disputeMessage.DisputedMessageModel;
import com.ichef.android.responsemodel.fetchcart.FetchCartResponse;
import com.ichef.android.responsemodel.homefood.DriverListResponse;
import com.ichef.android.responsemodel.login.DeviceTokenResponse;
import com.ichef.android.responsemodel.login.LoginResponse;
import com.ichef.android.responsemodel.notifications.GetNotificationResponse;
import com.ichef.android.responsemodel.orders.GetOrderResponse;
import com.ichef.android.responsemodel.orders.Reorder.ReorderResponse;
import com.ichef.android.responsemodel.otprequest.ForgotResponse;
import com.ichef.android.responsemodel.otprequest.OTPResponse;
import com.ichef.android.responsemodel.payment.PaymentResponseModel;
import com.ichef.android.responsemodel.productdetail.ProductDetailResponse;
import com.ichef.android.responsemodel.productlist.HomePageListModel;
import com.ichef.android.responsemodel.profile.ProfileModel;
import com.ichef.android.responsemodel.regionData.RegionListModel;
import com.ichef.android.responsemodel.search.SearchResponse;
import com.ichef.android.responsemodel.signup.SignupResponse;
import com.ichef.android.responsemodel.vendordetail.GetDynamicVendorResponse;
import com.ichef.android.responsemodel.verify.VerifyResponseModel;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface APIInterface {

    @POST("getdriverlist")
    @FormUrlEncoded
    Call<DriverListResponse> getdriverlist(@FieldMap Map<String, String> params);

    @Headers("Content-Type: application/json")
    @POST("user/checkMobileExist")
    Call<MobileExistenceModel> CallCheckMobile(@Body LoginRequest loginRequest);

    @Headers("Content-Type: application/json")
    @POST("user/personal/login")
    Call<LoginResponse> CallLogin(@Body LoginRequest loginRequest);

    @Headers("Content-Type: application/json")
    @POST("/user/personal/signup")
    Call<SignupResponse> CallSignup(@Body SignupRequest signupRequest);

    @Headers("Content-Type: application/json")
    @POST("/user/verify")
    Call<OTPResponse> CallOtp(@Body OTPRequest otpRequest);

    @Headers("Content-Type: application/json")
    @POST("/user/resend")
    Call<OTPResponse> CallResendOTP(@Body OTPRequest otpRequest);

    @Headers("Content-Type: application/json")
    @POST("/user/social")
    Call<LoginResponse> CallSocialLogin(@Body SocialLoginRequest otpRequest);

    @Headers("Content-Type: application/json")
    @POST("/user/forgotPassword")
    Call<ForgotResponse> CallForgotPassword(@Body LoginRequest otpRequest);

    @Headers("Content-Type: application/json")
    @POST("/user/updatePassword")
    Call<SignupResponse> CallUpdatePassword(@Body ChangePasswordRequest otpRequest);

    @Headers("Content-Type: application/json")
    @PUT("/user/updateUser")
    Call<AddAddressResponse> CallUpdateProfile(@Header("Authorization") String token, @Body EditProfileRequest editProfileRequest);

    @Headers("Content-Type: application/json")
    @GET("/product/getFilteredVendors")
    Call<HomePageListModel> CallProductList(@Header("Authorization") String token,@Query("sort") String sort,@Query("latt") String latitude, @Query("long") String longitude );

    @Headers("Content-Type: application/json")
    @GET("/product/getNearbyVendors")
    Call<HomePageListModel> CallNearByVendorType(@Header("Authorization") String token, @Query("type") String type,@Query("id") String id,@Query("latt") String latitude, @Query("long") String longitude );


   @Headers("Content-Type: application/json")
    @GET("/misc/getSliders")
    Call<BannerListModel> CallBannerList();

    @Headers("Content-Type: application/json")
    @GET("misc/regions?sort=id&direction=asc")
    Call<RegionListModel> CallRegionList();

    @Headers("Content-Type: application/json")
    @GET("product/getEattimes")
    Call<FeelingListModel> CallAreyouFeelingList(@Header("Authorization") String token);


    @Headers("Content-Type: application/json")
    @POST("/product/addToCart")
    Call<AddtoCartResponse> Calladdtocart(@Header("Authorization") String token, @Body AddtoCartRequest addtoCartRequest);

    @GET("/product/fetchCart")
    Call<FetchCartResponse> Getcart(@Header("Authorization") String token);

    @GET("/product/bookmarks")
    Call<GetBookmarkListResponse> Getbookmark(@Header("Authorization") String token);

    @GET("/product/search")
    Call<SearchResponse> GetSearch(@Header("Authorization") String token, @Query("searchtext") String searchtext, @Query("page") String page
            , @Query("limit") String limit,@Query("latt") String latitude, @Query("long") String longitude);

    @GET("/product/getVendor/{VendorId}")
    Call<GetDynamicVendorResponse> GetVendorDetails(@Header("Authorization") String token, @Path("VendorId") String VendorId,@Query("latt") String latitude, @Query("long") String longitude);

    @GET("/product/{ProductId}")
    Call<ProductDetailResponse> GetProductsDetails(@Header("Authorization") String token, @Path("ProductId") String ProductId);

    @GET("/product/fetchCart")
    Call<com.ichef.android.responsemodel.cartmodel.FetchCartResponse> GetFetchCart(@Header("Authorization") String token);

    @POST("/product/updateShippingCost")
    Call<com.ichef.android.responsemodel.cartmodel.FetchCartResponse> GetUpdatedCart(@Header("Authorization") String token, @Body UpdateCartRequest updateCartRequest);

    @DELETE("product/clearCart")
    Call<com.ichef.android.responsemodel.Address.AddAddressResponse> ClearCart(@Header("Authorization") String token);

    @Headers("Content-Type: application/json")
    @POST("product/clearCartItem")
    Call<com.ichef.android.responsemodel.Address.AddAddressResponse> RemoveCart(@Header("Authorization") String token,@Body AddToCartRequest addtoCartRequest);

    @Headers("Content-Type: application/json")
    @POST("/product/addToCart")
    Call<com.ichef.android.responsemodel.cartmodel.FetchCartResponse> CallAddToCart(@Header("Authorization") String token, @Body AddToCartRequest addtoCartRequest);

    @Headers("Content-Type: application/json")
    @POST("/product/reduceCart")
    Call<com.ichef.android.responsemodel.cartmodel.FetchCartResponse> CallReduceCart(@Header("Authorization") String token, @Body ReduceCartRequest reduceCartRequest);

    @Headers("Content-Type: application/json")
    @POST("/address/addAddress")
    Call<com.ichef.android.responsemodel.Address.AddAddressResponse> CallAddAddress(@Header("Authorization") String token, @Body AddAddressRequest addAddressRequest);


    @GET("/misc/getallcities")
    Call<CityResponse> GetCity(@Header("Authorization") String token);

    @GET("address/getAddressList")
    Call<GetAddressesResponse> GetAddresses(@Header("Authorization") String token);

    @GET("notification/getnotificationsbyuserid")
    Call<GetNotificationResponse> GetNotifications(@Header("Authorization") String token);

    @GET("payment/verify")
    Call<VerifyResponseModel> VarifyPayment(@Header("Authorization") String token, @Query("id") String id);

    @DELETE("/address/deleteAddress/{AddressId}")
    Call<com.ichef.android.responsemodel.Address.AddAddressResponse> CallDeleteAddress(@Header("Authorization") String token, @Path("AddressId") String AddressId);

    @Headers("Content-Type: application/json")
    @POST("/order")
    Call<com.ichef.android.responsemodel.Address.AddAddressResponse> CallCreateOrder(@Header("Authorization") String token, @Body CreateOrderRequest createOrderRequest);

    @Headers("Content-Type: application/json")
    @POST("/product/bookmark")
    Call<com.ichef.android.responsemodel.Address.AddAddressResponse> CallAddBookMark(@Header("Authorization") String token, @Body MarkBookmarkRequest markBookmarkRequest);

    @Headers("Content-Type: application/json")
    @POST("/product/review")
    Call<com.ichef.android.responsemodel.Address.AddAddressResponse> CallRateOrder(@Header("Authorization") String token, @Body RateOrderRequest rateOrderRequest);

    @DELETE("/product/deletebookmark/{vendorId}")
    Call<com.ichef.android.responsemodel.Address.AddAddressResponse> CallRemoveBookMark(@Header("Authorization") String token, @Path("vendorId") String vendorId);

    @DELETE("/user/deleteUser")
    Call<com.ichef.android.responsemodel.Address.AddAddressResponse> CallDeleteUSer(@Header("Authorization") String token, @Body DeleteUserRequest deleteUserRequest);

    @PATCH("/order/changestatus/{orderId}")
    Call<com.ichef.android.responsemodel.Address.AddAddressResponse> CallCancelOrder(@Header("Authorization") String token, @Path("orderId") String orderId, @Body CancelOrderRequest cancelOrderRequest);

    @POST("/dispute")
    Call<com.ichef.android.responsemodel.Address.AddAddressResponse> CallDisputeOrder(@Header("Authorization") String token, @Body DisputeOrderRequest disputeOrderRequest);

    @PATCH("user/updateNotificationStatus")
    Call<com.ichef.android.responsemodel.Address.AddAddressResponse> CallUpdateNotification(@Header("Authorization") String token, @Body UpdateNotificationRequest updateNotificationRequest);

    @GET("/promo/getCouponByCode/{PromoCode}")
    Call<ApplyPromoCodeResponse> ApplyPromoCode(@Header("Authorization") String token, @Path("PromoCode") String PromoCode);

    @GET("/order/reorder/{orderId}")
    Call<ReorderResponse> CallReorder(@Header("Authorization") String token, @Path("orderId") String orderId);

    @GET("/order/orders/{customerId}/")
    Call<GetOrderResponse> GetOrders(@Header("Authorization") String token,@Path("customerId") String customerId, @Query("page") Integer PageNumber, @Query("limit") Integer Limit, @Query("sort") String sort, @Query("direction") String direction);

    @Headers("Content-Type: application/json")
    @GET("product/review/vendorsreviews/{vendorId}")
    Call<ReviewsListResponse> CallReview(@Header("Authorization") String token, @Path("vendorId") String vendorId, @Query("page") String page, @Query("limit") String limit
            , @Query("sort") String sort, @Query("direction") String direction);

    @Headers("Content-Type: application/json")
    @PUT("/user/updateUser")
    Call<SignupResponse> CallUpdateUser(@Header("Authorization") String token,@Body EditProfileRequest signupRequest);

    @Headers("Content-Type: application/json")
    @POST("user/addPushToken")
    Call<DeviceTokenResponse> UpdateDeviceToken(@Header("Authorization") String token, @Body DeviceTokenRequest bankDetailRequest);

    @Headers("Content-Type: application/json")
    @POST("/dispute/message")
    Call<DeviceTokenResponse> sendMessage(@Header("Authorization") String token, @Body SendMessageModel bankDetailRequest);

    @GET("/user/getProfile")
    Call<ProfileModel> GetProfile(@Header("Authorization")String token);

    @GET("/dispute/getdisputes")
    Call<DisputedModel> GetDisputes(@Header("Authorization")String token);

    @GET("dispute/getdisputemessages/{disputeID}?page=0&limit=50&sort=datecreated&direction=desc")
    Call<DisputedMessageModel> GetDisputesMessage(@Header("Authorization")String token, @Path("disputeID") String disputeID);

    @Headers("Content-Type: application/json")
    @POST("/payment/initiate")
    Call<PaymentResponseModel> CallPaymentInitiate(@Header("Authorization") String token, @Body PaymentItem addtoCartRequest);

}
